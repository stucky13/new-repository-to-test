<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 27.07.2018
 * Time: 10:06
 */

use yii\helpers\Html;

?>
<div class="text-color">
    <h2><?= Yii::t('app', 'imprint') ?></h2>

    Organisation GmbH<br>
    Straße Hausnummer<br>
    PLZ Ort<br>
    Telefon: <br>
    E-Mail: <br>
    <div class="link-color"><?= Html::a('Link', '#/', ['target' => '_blank', 'class' => 'link-color']) ?></div>
    <br>

    <div><?= Html::img('@web/images/logo.png', ['class' => 'imprint-brand-logo']) ?></div>
    <br>

    <?= Yii::t('app', 'manager')?>: <br>

    <?= Yii::t('app', 'chairman of the board')?>: <br>

    Amtsgericht Ort HRB Nr<br>

</div>


