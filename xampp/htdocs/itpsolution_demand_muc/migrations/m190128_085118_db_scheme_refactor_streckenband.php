<?php

use yii\db\Migration;

/**
 * Class m190128_085118_db_scheme_refactor_streckenband
 */
class m190128_085118_db_scheme_refactor_streckenband extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // drop old columns
        $this->dropPrimaryKey('', 'streckenband');
        $this->dropColumn('streckenband', 'hst_sort');
        $this->addPrimaryKey('','streckenband', 'strecke_id');
        $this->dropForeignKey('fk_streckenband_stationsangabe_ibnr', 'streckenband');
        $this->dropColumn('streckenband', 'ibnr');
        $this->dropColumn('streckenband', 'name');

        // create table for all stations of a route
        $this->execute('
            CREATE TABLE IF NOT EXISTS streckenband_stationsangabe (
              `strecke_id` INT(11) NOT NULL,
              `hst_sort` INT(11) NOT NULL,
              `ibnr` INT(11) NOT NULL,
              PRIMARY KEY (`strecke_id`, `hst_sort`, `ibnr`),
              INDEX `fk_routes_stations_ibnr_idx` (`ibnr` ASC),
              CONSTRAINT `fk_streckenband_stationsangabe_stationsangabe_ibnr`
                FOREIGN KEY (`ibnr`)
                REFERENCES `stationsangabe` (`ibnr`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_streckenband_stationsangabe_streckenband_id`
                FOREIGN KEY (`strecke_id`)
                REFERENCES `streckenband` (`strecke_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drop new table
        $this->dropTable('streckenband_stationsangabe');

        // add old columns
        $this->dropPrimaryKey('', 'streckenband');
        $this->addColumn('streckenband', 'hst_sort', 'INT(11)');
        $this->addPrimaryKey('','streckenband', ['strecke_id', 'hst_sort']);
        $this->addColumn('streckenband', 'ibnr', 'INT(11)');
        $this->addForeignKey('fk_streckenband_stationsangabe_ibnr', 'streckenband', 'ibnr', 'stationsangabe', 'ibnr');
        $this->addColumn('streckenband', 'name', 'VARCHAR(255)');
    }
}
