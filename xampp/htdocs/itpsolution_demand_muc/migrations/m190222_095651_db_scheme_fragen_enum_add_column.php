<?php

use yii\db\Migration;

/**
 * Class m190222_095651_db_scheme_fragen_enum_add_column
 */
class m190222_095651_db_scheme_fragen_enum_add_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('fragen_enum', 'dt_created', 'datetime');
        $this->addColumn('fragen_enum', 'dt_updated', 'datetime');
        $this->addColumn('fragen_enum', 'user_created', 'int(11)');
        $this->addColumn('fragen_enum', 'user_updated', 'int(11)');
        $this->addForeignKey("fk_fragen_enum_user_created_user_user_id", "fragen_enum", "user_created", "user", "user_id");
        $this->addForeignKey("fk_fragen_enum_user_updated_user_user_id", "fragen_enum", "user_updated", "user", "user_id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('fragen_enum', 'dt_created');
        $this->dropColumn('fragen_enum', 'dt_updated');
        $this->dropColumn('fragen_enum', 'user_created');
        $this->dropColumn('fragen_enum', 'user_updated');
        $this->dropForeignKey("fk_fragen_enum_user_created_user_user_id", "fragen_enum");
        $this->dropForeignKey("fk_fragen_enum_user_updated_user_user_id", "fragen_enum");
    }

}
