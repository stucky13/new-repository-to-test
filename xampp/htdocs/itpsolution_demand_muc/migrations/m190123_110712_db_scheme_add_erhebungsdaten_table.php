<?php

use yii\db\Migration;

/**
 * Class m190123_110712_db_scheme_add_erhebungsdaten_table
 */
class m190123_110712_db_scheme_add_erhebungsdaten_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("CREATE TABLE IF NOT EXISTS `erhebungsdaten` (
              `erhebungsdaten_id` INT(11) NOT NULL AUTO_INCREMENT,
              `questionnaire_id` INT(11) NOT NULL,
              `survey_id` INT(11) NOT NULL,
              `count_data_id` INT(11) NOT NULL,
              `year` INT(4) NOT NULL,
              `period` VARCHAR(45) NOT NULL,
              `description` TEXT NOT NULL,
              `note` VARCHAR(255) NOT NULL,
              PRIMARY KEY (`erhebungsdaten_id`))
            ENGINE = InnoDB;");


        $this->execute("CREATE TABLE IF NOT EXISTS `zaehldaten` (
              `zaehldaten_id` INT NOT NULL AUTO_INCREMENT,
              `strecke` INT(11) NULL,
              `linie` VARCHAR(45) NULL,
              `zugnummer` INT(11) NULL,
              `zeitschicht` INT(11) NULL,
              `richtung` INT(11) NULL,
              `hst_sort` INT(11) NULL,
              `bhf_id` INT(7) NULL,
              `bhf_name` VARCHAR(255) NULL,
              `uhrzeit` TIME NULL,
              `zaehlung` INT(11) NULL,
              `ein` INT(11) NULL,
              `aus` INT(11) NULL,
              `bes` INT(11) NULL,
              `bes1` INT(11) NULL,
              `bes2` INT(11) NULL,
              `ein_f` INT(11) NULL,
              `aus_f` INT(11) NULL,
              `c_bes_f` INT(11) NULL,
              `abschnitt_km` decimal(30,15) NULL,
              `f_tag` decimal(30,15) NULL,
              `f_jahr` INT(11) NULL,
              `p_tag` INT(11) NULL,
              `r_tag` INT(11) NULL,
              `pkm_tag` decimal(30,15) NULL,
              `rkm_tag` decimal(30,15) NULL,
              `p_jahr` INT(11) NULL,
              `r_jahr` INT(11) NULL,
              `pkm_jahr` decimal(30,15) NULL,
              `rkm_jahr` decimal(30,15) NULL,
              `zaehldaten_file_id` int(11) NOT NULL,
              PRIMARY KEY (`zaehldaten_id`))
            ENGINE = InnoDB;");


        $this->execute("
            CREATE TABLE IF NOT EXISTS `zaehldaten_file` (
              `zaehldaten_file_id` INT(11) NOT NULL AUTO_INCREMENT,
              `organisation_id` INT(11) NOT NULL,
              `version` INT(11) NOT NULL,
              `has_errors` TINYINT(1) NULL DEFAULT '0',
              `path` TEXT NULL DEFAULT NULL,
              `path_errors` TEXT NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `upload_ip` VARCHAR(255) NULL DEFAULT NULL,
              `dt_created` DATETIME NOT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `user_created` INT(11) NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NULL DEFAULT '0',
              `is_valid` TINYINT(1) NULL DEFAULT '0',
              `is_processed` TINYINT(1) NULL DEFAULT '0',
              `is_imported` TINYINT(1) NULL DEFAULT '0',
              PRIMARY KEY (`zaehldaten_file_id`),
              INDEX `fk_zaehldaten_file_organisation_id_idx` (`organisation_id` ASC),
              INDEX `fk_zaehldaten_file_user_created_user_id_idx` (`user_created` ASC),
              INDEX `fk_zaehldaten_file_user_updated_idx` (`user_updated` ASC),
              CONSTRAINT `fk_zaehldaten_file_organisation_id`
                FOREIGN KEY (`organisation_id`)
                REFERENCES `organisation` (`organisation_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `fk_zaehldaten_file_user_created_user_id`
                FOREIGN KEY (`user_created`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_zaehldaten_file_user_updated_user_id`
                FOREIGN KEY (`user_updated`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `zaehldaten_file_error` (
              `zaehldaten_file_id` INT(11) NOT NULL AUTO_INCREMENT,
              `error_type` INT(11) NOT NULL,
              `user_created` INT(11) NOT NULL,
              `dt_created` DATETIME NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NULL DEFAULT NULL,
              `detail` TEXT NOT NULL,
              INDEX `fk_zaehldaten_file_error_zaehldaten_file_id_idx` (`zaehldaten_file_id` ASC),
              INDEX `fk_zaehldaten_file_error_user_created_user_id_idx` (`user_created` ASC),
              INDEX `fk_zaehldaten_file_error_user_updated_user_id_idx` (`user_updated` ASC),
              CONSTRAINT `fk_zaehldaten_file_error_zaehldaten_file_id`
                FOREIGN KEY (`zaehldaten_file_id`)
                REFERENCES `zaehldaten_file` (`zaehldaten_file_id`)
                ON DELETE CASCADE
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_zaehldaten_file_error_user_created_user_id`
                FOREIGN KEY (`user_created`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_zaehldaten_file_error_user_updated_user_id`
                FOREIGN KEY (`user_updated`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("erhebungsdaten");
        $this->dropTable("zaehldaten_file_error");
        $this->dropTable("zaehldaten_file");
        $this->dropTable("zaehldaten");
    }
}
