<?php

use yii\db\Migration;

/**
 * Class m190327_092109_db_scheme_add_table_verkehrsmittel
 */
class m190327_092109_db_scheme_add_table_verkehrsmittel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute('
            CREATE TABLE IF NOT EXISTS `verkehrsmittel` (
              `nr` INT(11) NOT NULL,
              `name` VARCHAR(255) NOT NULL,
              PRIMARY KEY (`nr`))
            ENGINE = InnoDB;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{verkehrsmittel}}');
    }
}
