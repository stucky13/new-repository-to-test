<?php

use yii\db\Migration;

/**
 * Class m190227_135028_db_scheme_add_columns_for_reference_table
 */
class m190227_135028_db_scheme_add_columns_for_reference_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('fragen', 'reference', 'VARCHAR(255)');
        $this->addColumn('fragen', 'field', 'VARCHAR(255)');
        $this->addColumn('fragen', 'category_id', 'INT(11)');
        $this->addForeignKey('fk_fragen_category_category_id', 'fragen', 'category_id', 'category', 'category_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_fragen_category_category_id', 'fragen');
        $this->dropColumn('fragen', 'category_id');
        $this->dropColumn('fragen', 'field');
        $this->dropColumn('fragen', 'reference');
    }
}
