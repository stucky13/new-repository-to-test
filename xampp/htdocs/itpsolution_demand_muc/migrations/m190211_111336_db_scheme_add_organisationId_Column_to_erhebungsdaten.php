<?php

use yii\db\Migration;

/**
 * Class m190211_111336_db_scheme_add_organisationId_Column_to_erhebungsdaten
 */
class m190211_111336_db_scheme_add_organisationId_Column_to_erhebungsdaten extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('erhebungsdaten', 'organisation_id', 'INT(11) NOT NULL');
        $this->addForeignKey('fk_erhebungsdaten_organisation_id', 'erhebungsdaten', 'organisation_id', 'organisation', 'organisation_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk_erhebungsdaten_organisation_id_idx', 'erhebungsdaten');
        $this->dropColumn('erhebungsdaten', 'organisation_id');
    }

}
