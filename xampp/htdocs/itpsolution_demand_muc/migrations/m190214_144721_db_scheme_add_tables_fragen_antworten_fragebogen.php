<?php

use yii\db\Migration;

/**
 * Class m190214_144721_db_scheme_add_tables_fragen_antworten_fragebogen
 */
class m190214_144721_db_scheme_add_tables_fragen_antworten_fragebogen extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable("fragen", ["frage_id" => "int(11) NOT NULL", "fragestellung" => "varchar(45) NOT NULL", "kurztext" => "varchar(45) NOT NULL", "antworttyp" => "varchar(45) NOT NULL", "wertebereich_min" => "varchar(45)", "wertebereich_max" => "varchar(45)", "nachkommastellen" => "varchar(45)", "dt_created" => "datetime NOT NULL", "user_created" => "int(11) NOT NULL", "dt_updated" => "datetime NULL DEFAULT NULL", "user_updated" => "int(11) NULL DEFAULT NULL"]);
        $this->createTable("fragebogen", ["fragebogen_id" => "int(11) NOT NULL", "user_created" => "int(11) NOT NULL", "dt_created" => "datetime NOT NULL", "user_updated" => "int(11) NULL DEFAULT NULL", "dt_updated" => "datetime NULL DEFAULT NULL"]);
        $this->createTable("antworten", ["antwort_id" => "int(11) NOT NULL", "interview_id" => "int(11) NOT NULL", "frage_id" => "int(11) NOT NULL", "fragebogen_id" => "int(11) NOT NULL","value" => "varchar(45)"]);
        $this->createTable("frage_fragebogen", ["fragebogen_id" => "int(11) NOT NULL", "frage_id" => "int(11) NOT NULL"]);
        $this->createTable("fragen_enum", ["frage_id" => "int(11) NOT NULL", "enum_id" => "int(11) NOT NULL", "value" => "varchar(45) NOT NULL"]);
        $this->addPrimaryKey("pk_fragen_frage_id", "fragen", "frage_id");
        $this->addPrimaryKey("pk_fragebogen_fragebogen_id", "fragebogen", "fragebogen_id");
        $this->addPrimaryKey("pk_antworten_antwort_id_interview_id_frage_id_fragebogen_id", "antworten", ["antwort_id", "interview_id", "frage_id", "fragebogen_id"]);
        $this->addPrimaryKey("pk_fragen_enum_enum_id_frage_id", "fragen_enum", ["enum_id", "frage_id"]);
        $this->addPrimaryKey("pk_frage_fragebogen_frage_id_fragebogen_id", "frage_fragebogen", ["frage_id", "fragebogen_id"]);
        $this->addForeignKey("fk_antworten_frage_id_fragen_frage_id", "antworten", "frage_id", "fragen", "frage_id");
        $this->addForeignKey("fk_antworten_fragebogen_id_fragebogen_fragebogen_id", "antworten", "fragebogen_id", "fragebogen", "fragebogen_id");
        $this->addForeignKey("fk_frage_fragebogen_frage_id_fragen_frage_id", "frage_fragebogen", "frage_id", "fragen", "frage_id");
        $this->addForeignKey("fk_frage_fragebogen_fragebogen_id_fragebogen_fragebogen_id", "frage_fragebogen", "fragebogen_id", "fragebogen", "fragebogen_id");
        $this->addForeignKey("fk_fragen_enum_frage_id_fragen_frage_id", "fragen_enum", "frage_id", "fragen", "frage_id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_antworten_frage_id_fragen_frage_id", "antworten");
        $this->dropForeignKey("fk_antworten_fragebogen_id_fragebogen_fragebogen_id", "antworten");
        $this->dropForeignKey("fk_frage_fragebogen_frage_id_fragen_frage_id", "frage_fragebogen");
        $this->dropForeignKey("fk_frage_fragebogen_fragebogen_id_fragebogen_fragebogen_id", "frage_fragebogen");
        $this->dropForeignKey("fk_fragen_enum_frage_id_fragen_frage_id", "fragen_enum");
        $this->dropPrimaryKey("pk_frage_fragebogen_frage_id_fragebogen_id", "frage_fragebogen");
        $this->dropPrimaryKey("pk_fragen_enum_enum_id_frage_id", "fragen_enum");
        $this->dropPrimaryKey("pk_antworten_antwort_id_frage_id_fragebogen_id", "antworten");
        $this->dropPrimaryKey("pk_fragebogen_fragebogen_id", "fragebogen");
        $this->dropPrimaryKey("pk_fragen_frage_id", "fragen");
        $this->dropTable("fragen_enum");
        $this->dropTable("frage_fragebogen");
        $this->dropTable("antworten");
        $this->dropTable("fragebogen");
        $this->dropTable("fragen");
    }


}
