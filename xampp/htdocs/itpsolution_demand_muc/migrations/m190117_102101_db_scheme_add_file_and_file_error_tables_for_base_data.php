<?php

use yii\db\Migration;

/**
 * Class m190117_102101_db_scheme_add_file_and_file_error_tables_for_base_data
 */
class m190117_102101_db_scheme_add_file_and_file_error_tables_for_base_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        /** create new tables */
        $this->execute("
            CREATE TABLE IF NOT EXISTS `stationsangabe_file` (
              `stationsangabe_file_id` INT(11) NOT NULL AUTO_INCREMENT,
              `organisation_id` INT(11) NOT NULL,
              `version` INT(11) NOT NULL,
              `has_errors` TINYINT(1) NULL DEFAULT '0',
              `path` TEXT NULL DEFAULT NULL,
              `path_errors` TEXT NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `upload_ip` VARCHAR(255) NULL DEFAULT NULL,
              `dt_created` DATETIME NOT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `user_created` INT(11) NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NULL DEFAULT '0',
              `is_valid` TINYINT(1) NULL DEFAULT '0',
              `is_processed` TINYINT(1) NULL DEFAULT '0',
              `is_imported` TINYINT(1) NULL DEFAULT '0',
              PRIMARY KEY (`stationsangabe_file_id`),
              INDEX `fk_stationsangabe_file_organisation_id_idx` (`organisation_id` ASC),
              INDEX `fk_stationsangabe_file_user_created_user_id_idx` (`user_created` ASC),
              INDEX `fk_stationsangabet_file_user_updated_idx` (`user_updated` ASC),
              CONSTRAINT `fk_stationsangabe_file_organisation_id`
                FOREIGN KEY (`organisation_id`)
                REFERENCES `organisation` (`organisation_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `fk_stationsangabe_file_user_created_user_id`
                FOREIGN KEY (`user_created`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_stationsangabet_file_user_updated_user_id`
                FOREIGN KEY (`user_updated`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `stationsangabe_file_error` (
              `stationsangabe_file_id` INT(11) NOT NULL,
              `error_type` INT(11) NOT NULL,
              `user_created` INT(11) NOT NULL,
              `dt_created` DATETIME NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NULL DEFAULT NULL,
              `detail` TEXT NOT NULL,
              INDEX `fk_stationsangabe_file_error_stationsangabe_file_id_idx` (`stationsangabe_file_id` ASC),
              INDEX `fk_stationsangabe_file_error_user_created_user_id_idx` (`user_created` ASC),
              INDEX `fk_stationsangabe_file_error_user_updated_user_id_idx` (`user_updated` ASC),
              CONSTRAINT `fk_stationsangabe_file_error_stationsangabe_file_id`
                FOREIGN KEY (`stationsangabe_file_id`)
                REFERENCES `stationsangabe_file` (`stationsangabe_file_id`)
                ON DELETE CASCADE
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_stationsangabe_file_error_user_created_user_id`
                FOREIGN KEY (`user_created`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_stationsangabe_file_error_user_updated_user_id`
                FOREIGN KEY (`user_updated`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `streckenband_file` (
              `streckenband_file_id` INT(11) NOT NULL AUTO_INCREMENT,
              `organisation_id` INT(11) NOT NULL,
              `version` INT(11) NOT NULL,
              `has_errors` TINYINT(1) NULL DEFAULT '0',
              `path` TEXT NULL DEFAULT NULL,
              `path_errors` TEXT NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `upload_ip` VARCHAR(255) NULL DEFAULT NULL,
              `dt_created` DATETIME NOT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `user_created` INT(11) NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NULL DEFAULT '0',
              `is_valid` TINYINT(1) NULL DEFAULT '0',
              `is_processed` TINYINT(1) NULL DEFAULT '0',
              `is_imported` TINYINT(1) NULL DEFAULT '0',
              PRIMARY KEY (`streckenband_file_id`),
              INDEX `fk_stationsangabe_file_organisation_id_idx` (`organisation_id` ASC),
              INDEX `fk_stationsangabe_file_user_created_user_id_idx` (`user_created` ASC),
              INDEX `fk_stationsangabet_file_user_updated_idx` (`user_updated` ASC),
              CONSTRAINT `fk_streckenband_file_organisation_id`
                FOREIGN KEY (`organisation_id`)
                REFERENCES `organisation` (`organisation_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `fk_streckenband_file_user_created_user_id`
                FOREIGN KEY (`user_created`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_streckenband_file_user_updated_user_id`
                FOREIGN KEY (`user_updated`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `streckenband_file_error` (
              `streckenband_file_id` INT(11) NOT NULL,
              `error_type` INT(11) NOT NULL,
              `user_created` INT(11) NOT NULL,
              `dt_created` DATETIME NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NULL DEFAULT NULL,
              `detail` TEXT NOT NULL,
              INDEX `fk_stationsangabe_file_error_user_created_user_id_idx` (`user_created` ASC),
              INDEX `fk_stationsangabe_file_error_user_updated_user_id_idx` (`user_updated` ASC),
              INDEX `fk_streckenband_file_error_streckenband_file_id_idx` (`streckenband_file_id` ASC),
              CONSTRAINT `fk_streckenband_file_error_streckenband_file_id`
                FOREIGN KEY (`streckenband_file_id`)
                REFERENCES `streckenband_file` (`streckenband_file_id`)
                ON DELETE CASCADE
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_streckenband_file_error_user_created_user_id`
                FOREIGN KEY (`user_created`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_streckenband_file_error_user_updated_user_id`
                FOREIGN KEY (`user_updated`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `fahrschein_file` (
              `fahrschein_file_id` INT(11) NOT NULL AUTO_INCREMENT,
              `organisation_id` INT(11) NOT NULL,
              `version` INT(11) NOT NULL,
              `has_errors` TINYINT(1) NULL DEFAULT '0',
              `path` TEXT NULL DEFAULT NULL,
              `path_errors` TEXT NULL DEFAULT NULL,
              `name` TEXT NULL DEFAULT NULL,
              `upload_ip` VARCHAR(255) NULL DEFAULT NULL,
              `dt_created` DATETIME NOT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `user_created` INT(11) NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NULL DEFAULT '0',
              `is_valid` TINYINT(1) NULL DEFAULT '0',
              `is_processed` TINYINT(1) NULL DEFAULT '0',
              `is_imported` TINYINT(1) NULL DEFAULT '0',
              PRIMARY KEY (`fahrschein_file_id`),
              INDEX `fk_stationsangabe_file_organisation_id_idx` (`organisation_id` ASC),
              INDEX `fk_stationsangabe_file_user_created_user_id_idx` (`user_created` ASC),
              INDEX `fk_stationsangabet_file_user_updated_idx` (`user_updated` ASC),
              CONSTRAINT `fk_fahrschein_file_organisation_id`
                FOREIGN KEY (`organisation_id`)
                REFERENCES `organisation` (`organisation_id`)
                ON DELETE CASCADE
                ON UPDATE CASCADE,
              CONSTRAINT `fk_fahrschein_file_user_created_user_id`
                FOREIGN KEY (`user_created`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_fahrschein_file_user_updated_user_id`
                FOREIGN KEY (`user_updated`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");

        $this->execute("
            CREATE TABLE IF NOT EXISTS `fahrschein_file_error` (
              `fahrschein_file_id` INT(11) NOT NULL,
              `error_type` INT(11) NOT NULL,
              `user_created` INT(11) NOT NULL,
              `dt_created` DATETIME NOT NULL,
              `user_updated` INT(11) NULL DEFAULT NULL,
              `dt_updated` DATETIME NULL DEFAULT NULL,
              `is_deleted` TINYINT(1) NULL DEFAULT NULL,
              `detail` TEXT NOT NULL,
              INDEX `fk_stationsangabe_file_error_user_created_user_id_idx` (`user_created` ASC),
              INDEX `fk_stationsangabe_file_error_user_updated_user_id_idx` (`user_updated` ASC),
              INDEX `fk_streckenband_file_error_streckenband_file_id_idx` (`fahrschein_file_id` ASC),
              CONSTRAINT `fk_fahrschein_file_error_fahrschein_file_id`
                FOREIGN KEY (`fahrschein_file_id`)
                REFERENCES `fahrschein_file` (`fahrschein_file_id`)
                ON DELETE CASCADE
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_fahrschein_file_error_user_created_user_id`
                FOREIGN KEY (`user_created`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION,
              CONSTRAINT `fk_fahrschein_file_error_user_updated_user_id`
                FOREIGN KEY (`user_updated`)
                REFERENCES `user` (`user_id`)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION)
            ENGINE = InnoDB;
        ");
        /** add foreign key columns */
        $this->addColumn('stationsangabe', 'stationsangabe_file_id', 'INT(11) NOT NULL');
        $this->addForeignKey('fk_stationsangabe_stationsangabe_file_id', 'stationsangabe', 'stationsangabe_file_id', 'stationsangabe_file', 'stationsangabe_file_id');
        $this->addColumn('streckenband', 'streckenband_file_id', 'INT(11) NOT NULL');
        $this->addForeignKey('fk_streckenband_streckenband_file_id', 'streckenband', 'streckenband_file_id', 'streckenband_file', 'streckenband_file_id');
        $this->addColumn('fahrschein', 'fahrschein_file_id', 'INT(11) NOT NULL');
        $this->addForeignKey('fk_fahrschein_fahrschein_file_id', 'fahrschein', 'fahrschein_file_id', 'fahrschein_file', 'fahrschein_file_id');
       /** add missing column and change fields */
        $this->addColumn('fahrschein', 'name', 'VARCHAR(255)');
        $this->alterColumn('stationsangabe', 'laenge', "decimal(30,15) NULL");
        $this->alterColumn('stationsangabe', 'breite', "decimal(30,15) NULL");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('stationsangabe', 'laenge', "decimal(30,15) NOT NULL");
        $this->alterColumn('stationsangabe', 'breite', "decimal(30,15) NOT NULL");
        $this->dropColumn('fahrschein', 'name');
        $this->dropForeignKey('fk_stationsangabe_stationsangabe_file_id', 'stationsangabe');
        $this->dropColumn('stationsangabe', 'stationsangabe_file_id');
        $this->dropForeignKey('fk_streckenband_streckenband_file_id', 'streckenband');
        $this->dropColumn('streckenband', 'streckenband_file_id');
        $this->dropForeignKey('fk_fahrschein_fahrschein_file_id', 'fahrschein');
        $this->dropColumn('fahrschein', 'fahrschein_file_id');

        $this->dropTable('fahrschein_file_error');
        $this->dropTable('fahrschein_file');
        $this->dropTable('streckenband_file_error');
        $this->dropTable('streckenband_file');
        $this->dropTable('stationsangabe_file_error');
        $this->dropTable('stationsangabe_file');
    }
}
