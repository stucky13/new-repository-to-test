<?php

use yii\db\Migration;

/**
 * Class m190320_084741_db_scheme_adjust_data_structure_for_relation
 */
class m190320_084741_db_scheme_adjust_data_structure_for_relation extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('fragen', 'is_georef', 'TINYINT(1)');
        $this->addColumn('fragen', 'is_filter', 'TINYINT(1)');
        $this->addColumn('fragen', 'is_value', 'TINYINT(1)');
        $this->addColumn('fragen', 'value_type', 'INT(11)');

        $this->renameTable('streckenband', 'linien');
        $this->renameColumn('linien', 'strecke_id', 'linien_id');
        $this->renameColumn('linien', 'strecke_name', 'linien_name');
        $this->renameColumn('linien', 'streckenband_file_id', 'linien_file_id');
        $this->renameTable('streckenband_file', 'linien_file');
        $this->renameColumn('linien_file', 'streckenband_file_id','linien_file_id');
        $this->renameTable('streckenband_file_error', 'linien_file_error');
        $this->renameColumn('linien_file_error', 'streckenband_file_id','linien_file_id');

        $this->renameTable('stationsangabe', 'knoten');
        $this->renameColumn('knoten', 'ibnr', 'nr');
        $this->renameColumn('knoten', 'stationsangabe_file_id', 'knoten_file_id');
        $this->renameTable('stationsangabe_file', 'knoten_file');
        $this->renameColumn('knoten_file', 'stationsangabe_file_id','knoten_file_id');
        $this->renameTable('stationsangabe_file_error', 'knoten_file_error');
        $this->renameColumn('knoten_file_error', 'stationsangabe_file_id','knoten_file_id');

        $this->renameTable('streckenband_stationsangabe', 'linien_knoten');
        $this->renameColumn('linien_knoten', 'ibnr', 'nr');
        $this->renameColumn('linien_knoten', 'strecke_id', 'linien_id');

        $this->execute("
            CREATE TABLE IF NOT EXISTS `tarifpunkt` (
              `nr_version` int(11) NOT NULL,
              `nr` int(11) NOT NULL,
              `name_30` varchar(30) NOT NULL DEFAULT '',
              `name_24` varchar(24) NOT NULL DEFAULT '',
              `name_20` varchar(20) NOT NULL DEFAULT '',
              `name_12` char(12) NOT NULL DEFAULT '',
              `kuerzel` char(6) NOT NULL DEFAULT '',
              `grafikname` varchar(255) NOT NULL DEFAULT '',
              `nr_beschriftungsebene` int(11) NOT NULL,
              `nr_tarifpunkttyp` int(11) NOT NULL,
              `htp_ref` varchar(255) NOT NULL DEFAULT '',
              `nr_tarifgebiet` int(11) NOT NULL,
              `nr_tarifgebiet2` int(11) NOT NULL,
              `nr_ref_haltestelle` int(11) NOT NULL,
              `nr_ref_verbund` int(11) NOT NULL,
              `name_ref_verbund` varchar(255) NOT NULL DEFAULT '',
              `nr_at` int(11) NOT NULL,
              `nr_vugruppe` int(11) NOT NULL,
              `ex_hafas` varchar(255) NOT NULL DEFAULT '',
              `ex_ibnr` varchar(255) NOT NULL DEFAULT '',
              `bus` int(11) NOT NULL,
              `ersatzradius` decimal(30,15) NOT NULL,
              `siedlungsstruktur` int(11) NOT NULL,
              `flaeche` decimal(30,15) NOT NULL,
              `x_schwerp_f` decimal(30,15) NOT NULL,
              `y_schwerp_f` decimal(30,15) NOT NULL,
              `x_schwerp_v` decimal(30,15) NOT NULL,
              `y_schwerp_v` decimal(30,15) NOT NULL,
              `fix_schwerp_v` bit(1) NOT NULL,
              `lon` decimal(30,15) NOT NULL,
              `lat` decimal(30,15) NOT NULL,
              `x` decimal(30,15) NOT NULL,
              `y` decimal(30,15) NOT NULL,
              `schriftgroesse` decimal(16,1) NOT NULL,
              `relativegroesse` decimal(18,3) NOT NULL,
              `fuellfarbe` int(11) NOT NULL,
              `no_induz` bit(1) NOT NULL,
              `login` varchar(255) NOT NULL DEFAULT '',
              `modified` datetime DEFAULT NULL,
              `einwohner` int(11) NOT NULL,
              `uebersteiger` int(11) NOT NULL,
              `bemerkung` varchar(255) NOT NULL DEFAULT '',
              `vp1` decimal(30,15) NOT NULL,
              `vp2` decimal(30,15) NOT NULL,
              `einaussteiger` int(11) NOT NULL,
              `ueberstquote` decimal(18,3) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8;"
        );

        $this->execute("CREATE TABLE IF NOT EXISTS `value_type` (
                `value_type_id` INT(12) NOT NULL,
                `name` VARCHAR(255) NOT NULL,
                `shortname` VARCHAR(255) NOT NULL,
                `description` VARCHAR(255) NOT NULL,
                `unit` VARCHAR(255) NOT NULL DEFAULT '-',
                `aufteilungsverfahren` INT NOT NULL DEFAULT 1,
                PRIMARY KEY (`value_type_id`)
            )");

        $this->insert('value_type', ["value_type_id" => 0, "name" => "anzahl_tickets", "shortname" => "anzahl_tickets", "description" => "Anzahl der Ticketverkäufe", "unit" => "-", "aufteilungsverfahren" => 1]);
        $this->insert('value_type', ["value_type_id" => 1, "name" => "umsatz_zuschlag_pbv", "shortname" => "umsatz_zuschlag_pbv", "description" => "Umsatz des Zuschlags für personenbedienter Verkauf", "unit" => "€", "aufteilungsverfahren" => 1]);
        $this->insert('value_type', ["value_type_id" => 2, "name" => "umsatz_provision", "shortname" => "umsatz_provision", "description" => "Provision des Vertriebsdienstleisters", "unit" => "€", "aufteilungsverfahren" => 1]);
        $this->insert('value_type', ["value_type_id" => 3, "name" => "umsatz_am_soll", "shortname" => "umsatz_am_soll", "description" => "Soll-Umsatz der Anschlussmobilität", "unit" => "€", "aufteilungsverfahren" => 3]);
        $this->insert('value_type', ["value_type_id" => 4, "name" => "umsatz_am_ist", "shortname" => "umsatz_am_ist", "description" => "Ist-Umsatz der Anschlussmobilität", "unit" => "€", "aufteilungsverfahren" => 0]);
        $this->insert('value_type', ["value_type_id" => 5, "name" => "umsatz_am_bc_soll", "shortname" => "umsatz_am_bc_soll", "description" => "Soll-Anspruch BC-Ausgleichszahlungen der Anschlussmobilität", "unit" => "€", "aufteilungsverfahren" => 3]);
        $this->insert('value_type', ["value_type_id" => 6, "name" => "umsatz_am_bc_ist", "shortname" => "umsatz_am_bc_ist", "description" => "Ist-Anspruch BC-Ausgleichszahlungen der Anschlussmobilität", "unit" => "€", "aufteilungsverfahren" => 0]);
        $this->insert('value_type', ["value_type_id" => 7, "name" => "umsatz_vuv_soll", "shortname" => "umsatz_vuv_soll", "description" => "Soll-Umsatz für SPNV", "unit" => "€", "aufteilungsverfahren" => 2]);
        $this->insert('value_type', ["value_type_id" => 8, "name" => "umsatz_vuv_ist", "shortname" => "umsatz_vuv_ist", "description" => "Ist-Umsatz für SPNV", "unit" => "€", "aufteilungsverfahren" => 2]);
        $this->insert('value_type', ["value_type_id" => 9, "name" => "umsatz_vuv_bc_soll", "shortname" => "umsatz_vuv_bc_soll", "description" => "Soll-Anspruch BC-Ausgleichszahlungen für SPNV", "unit" => "€", "aufteilungsverfahren" => 2]);
        $this->insert('value_type', ["value_type_id" => 10, "name" => "umsatz_vuv_bc_ist", "shortname" => "umsatz_vuv_bc_ist", "description" => "Ist-Anspruch BC-Ausgleichszahlungen für SPNV", "unit" => "€", "aufteilungsverfahren" => 2]);
        $this->insert('value_type', ["value_type_id" => 11, "name" => "umsatz_vuv_av_direkt", "shortname" => "umsatz_vuv_av_direkt", "description" => "Direkter Ausgleichsanspruch aus Festlegung Höchstpreise", "unit" => "€", "aufteilungsverfahren" => 2]);
        $this->insert('value_type', ["value_type_id" => 12, "name" => "umsatz_vuv_av_am", "shortname" => "umsatz_vuv_av_am", "description" => "Direkter Ausgleichsanspruch aus externer Finanzierung der Anschlussmobilität", "unit" => "€", "aufteilungsverfahren" => 2]);
        $this->insert('value_type', ["value_type_id" => 13, "name" => "Aufkommen_VUV", "shortname" => "p_vuv", "unit" => "P"]);
        $this->insert('value_type', ["value_type_id" => 14, "name" => "Aufkommen_AM", "shortname" => "p_am", "unit" => "P"]);
        $this->insert('value_type', ["value_type_id" => 15, "name" => "Verkehrsleistung_VUV", "shortname" => "pkm_vuv", "unit" => "Pkm"]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('value_type');
        $this->dropTable('tarifpunkt');

        $this->renameTable('linien_knoten', 'streckenband_stationsangabe');
        $this->renameColumn('streckenband_stationsangabe', 'nr', 'ibnr');
        $this->renameColumn('streckenband_stationsangabe', 'linien_id', 'strecke_id');

        $this->renameTable('knoten', 'stationsangabe');
        $this->renameColumn('stationsangabe', 'nr', 'ibnr');
        $this->renameColumn('stationsangabe', 'knoten_file_id', 'stationsangabe_file_id');
        $this->renameTable('knoten_file', 'stationsangabe_file');
        $this->renameColumn('stationsangabe_file', 'knoten_file_id','stationsangabe_file_id');
        $this->renameTable('knoten_file_error', 'stationsangabe_file_error');
        $this->renameColumn('stationsangabe_file_error', 'knoten_file_id','stationsangabe_file_id');

        $this->renameTable('linien', 'streckenband');
        $this->renameColumn('streckenband', 'linien_id', 'strecke_id');
        $this->renameColumn('streckenband', 'linien_name', 'strecke_name');
        $this->renameColumn('streckenband', 'linien_file_id', 'streckenband_file_id');
        $this->renameTable('linien_file', 'streckenband_file');
        $this->renameColumn('streckenband_file', 'linien_file_id','streckenband_file_id');
        $this->renameTable('linien_file_error', 'streckenband_file_error');
        $this->renameColumn('streckenband_file_error', 'linien_file_id','streckenband_file_id');

        $this->dropColumn('fragen', 'value_type');
        $this->dropColumn('fragen', 'is_value');
        $this->dropColumn('fragen', 'is_georef');
        $this->dropColumn('fragen', 'is_filter');
    }
}
