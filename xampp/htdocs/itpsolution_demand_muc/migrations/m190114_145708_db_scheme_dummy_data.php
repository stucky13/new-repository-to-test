<?php

use yii\db\Migration;
use yii\db\Expression;

/**
 * Class m190114_145708_db_scheme_dummy_data
 */
class m190114_145708_db_scheme_dummy_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        /* Dummy-User with admin permission */
        $this->insert('{{user}}', [
            'dt_created' => new Expression('NOW()'),
            'dt_updated' => new Expression('NOW()'),
            'username' => 'Admin',
            'password' => Yii::$app->getSecurity()->generatePasswordHash('AdminPW123!.'),
            'first_name' => 'Peter',
            'last_name' => 'Müller',
            'is_active' => true,
            'is_new' => false,
            'email' => 'email@admin.de',
            'auth_key' => Yii::$app->getSecurity()->generateRandomString(),
            'access_token' => Yii::$app->getSecurity()->generateRandomString()
        ]);

        $this->insert('{{auth_assignment}}', [
            'item_name' => 'admin',
            'user_id' => 1,
            'created_at' => new Expression('NOW()')
        ]);

        /* Dummy-Org-Type */
        $this->insert('{{organisation_type}}', [
            'dt_created' => new Expression('NOW()'),
            'dt_updated' => new Expression('NOW()'),
            'title' => 'test',
            'short_title' => 'test',
        ]);

        /* Dummy-Org */
        $this->insert('{{organisation}}', [
            'dt_created' => new Expression('NOW()'),
            'dt_updated' => new Expression('NOW()'),
            'organisation_type_id' => 1,
            'title' => 'test',
            'short_title' => 'test'
        ]);

        /* Dummy-User-Org */
        $this->insert('{{user_organisation}}', [
            'dt_created' => new Expression('NOW()'),
            'user_id' => 1,
            'organisation_id' => 1
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('user_organisation', ['and', 'user_id' => 1, 'organiation_id' => 1]);
        $this->delete('organisation', ['organisation_id' => 1]);
        $this->delete('organisation_type', ['organisation_type_id' => 1]);
        $this->delete('auth_assignment', ['and', ['user_id' => 1], ['item_name' => 'admin']]);
        $this->delete('user', ['user_id' => 1]);
    }
}
