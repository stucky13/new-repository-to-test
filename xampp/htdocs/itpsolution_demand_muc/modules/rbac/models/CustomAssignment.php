<?php

namespace app\modules\rbac\models;

use yii\rbac\Assignment;

class CustomAssignment extends Assignment
{
    /**
     * @var int the data
     */
    public $data;
}