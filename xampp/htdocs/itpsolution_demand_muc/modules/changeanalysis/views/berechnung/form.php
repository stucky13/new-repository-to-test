<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 07.08.2019
 * Time: 09:47
 */
/**\components\bootswatch\sandstone\bootstrap.css hier kann man die css für Checkbox ändern.*/

use app\modules\changeanalysis\Module;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = Module::t("changeanalysis", "Landseitige Anbindung des Flughafens München - Veränderungsanalyse");
?>
<?php $form = ActiveForm::begin([
    'id' => 'changeanalysis-form',
    'options' => ['class' => 'form-vertical']
]); ?>
<div class="container" style="width:1200px">
        <div class="form-row">
            <div class="col-sm-5 form-color-dark"><h4>
                <?= $form->field($model, 'bezeichnung')->textInput(['class' => 'form-control', 'placeholder' => 'Bezeichnung']) ?>
                    </h4>
            </div>
            <div class="col-sm-7 form-color-dark"><h4>
                <?= $form->field($model, 'anmerkung')->textInput(['class' => 'form-control', 'placeholder' => 'Anmerkung'])?>
                    </h4>
            </div>
        </div>

        <table class="table form-control-color" >
            <thead>
            <tr  class="form-color-dark" >
                <th scope="col-1" class="text-center" ></th>
                <th scope="col-3">Veränderungen im ÖV Zugang </th>
                <th scope="col-2" class="text-center ">München Hbf</th>
                <th scope="col-2" class="text-center ">Nordost (Regensburg)</th>
                <th scope="col-2" class="text-center ">Südost (Salzburg)</th>
                <th scope="col-2" class="text-center ">West (Augsburg)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">1</th>
                <td>
                    <div><?= $form->field($model, 'muc_fahrzeit_funktion')->checkbox()->label("Fahrzeitänderung [Min]")?></div>
                </td>
                <td>
                    <div class="input-group"><?= $form->field($model,'muc_fahrzeit_hbf_ist')->textInput(['placeholder' => 'Ist'])->label("")?>
                        <?=$form->field($model, 'muc_fahrzeit_hbf_neu')->textInput(['placeholder' => 'Neu'])->label("")?></div>
                </td>
                <td>
                    <div class="input-group"><?= $form->field($model, 'muc_fahrzeit_no_ist')->textInput(['placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model, 'muc_fahrzeit_no_neu')->textInput(['placeholder' => 'Neu'])->label("")?></div>
                </td>
                <td>
                    <div class="input-group"><?= $form->field($model, 'muc_fahrzeit_so_ist')->textInput(['placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model,'muc_fahrzeit_so_neu')->textInput(['placeholder' => 'Neu'])->label("")?></div>
                </td>
                <td>
                    <div class="input-group"><?= $form->field($model,'muc_fahrzeit_w_ist')->textInput([ 'placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model, 'muc_fahrzeit_w_neu')->textInput(['placeholder' => 'Neu'])->label("")?></div>
                </td>
            </tr>
            <tr>
                <th scope="row">2</th>
                <td>
                    <div><?= $form->field($model, 'muc_takt_funktion')->checkbox()->label("Änderung der Bedienungshäufigkeit<br>[Takt in Minuten]")?></div>
                </td>
                <td>
                    <div class="input-group"><?= $form->field($model, 'muc_takt_hbf_ist')->textInput(['placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model, 'muc_takt_hbf_neu')->textInput([ 'placeholder' => 'Neu'])->label("")?></div>
                </td>
                <td>
                    <div class="input-group"><?= $form->field($model, 'muc_takt_no_ist')->textInput([ 'placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model, 'muc_takt_no_neu')->textInput(['placeholder' => 'Neu'])->label("")?></div>
                </td>
                <td>
                    <div class="input-group"><?= $form->field($model, 'muc_takt_so_ist')->textInput([ 'placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model, 'muc_takt_so_neu')->textInput([ 'placeholder' => 'Neu'])->label("")?></div>
                </td>
                <td>
                    <div class="input-group"><?= $form->field($model, 'muc_takt_w_ist')->textInput([ 'placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model, 'muc_takt_w_neu')->textInput([ 'placeholder' => 'Neu'])->label("")?></div>
                </td> <

            </tr>
            <tr>
                <th scope="row">3</th>
                <td>
                    <div><?= $form->field($model, 'muc_direktanbindung_funktion')->checkbox()->label("Direktanbindung<br>[Äquivalente: Oberzentrum = 10,<br> Mittelzentrum = 3, sonstige = 1]")?></div>
                </td>
                <td>
                    <div class="input-group sm-3"><?= $form->field($model, 'muc_direktanbindung_hbf_ist')->textInput([ 'placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model, 'muc_direktanbindung_hbf_neu')->textInput([ 'placeholder' => 'Neu'])->label("")?></div>
                </td>
                <td>
                    <div class="input-group sm-3"><?= $form->field($model, 'muc_direktanbindung_no_ist')->textInput([ 'placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model, 'muc_direktanbindung_no_neu')->textInput([ 'placeholder' => 'Neu'])->label("")?></div>
                </td>
                <td>
                    <div class="input-group sm-3"><div class="col-sm-6 col-sm-offset-3"></div>
                        <?= $form->field($model, 'muc_direktanbindung_so_neu')->textInput([ 'placeholder' => 'Neu'])->label("")?></div>
                </td>
                <td>
                    <div class="input-group sm-3"><div class="col-sm-6 col-sm-offset-3"></div>
                        <?= $form->field($model, 'muc_direktanbindung_w_neu')->textInput(['placeholder' => 'Neu'])->label("")?></div>
                </td>
            </tr>
            <tr>
                <th scope="row">4</th>
                <td rowspan="3">
                    <div><?= $form->field($model, 'muc_hbf_produkt_funktion')->checkbox()->label("Eigenes Produkt [MAX]")?>
                            <br>
                            <p>&emsp;&nbsp; Preisaufschlag [EUR]</p>
                    </div>
                </td>
                <td>
                    <div><?= $form->field($model, 'muc_hbf_produkt_janein')->checkbox()->label("Nein/Ja")?></div>
                    <div><?= $form->field($model, 'muc_hbf_produkt_preisaufschlag')->textInput(['placeholder' => '[EUR]'])->label("")?></div>
                </td>
            </tr>
            </tbody>
            <thead>
            <tr>
                <th scope="col-1" class="form-color-dark"></th>
                <th scope="col-3" class="form-color-dark">ÖV Fahrtzeitänderungen bei<br>Konkurrenzflughäfen</th>
                <th scope="col-2" class="text-center form-color-dark">Augsburg<br>(Flughafen Stuttgart)</th>
                <th scope="col-2" class="text-center form-color-dark">Achse Stuttgart<br>(Flughafen Frankfurt)</th>
                <th scope="col-2" class="text-center form-color-dark">Achse Nürnberg <br>(Flughafen Frankfurt)</th>
                <th scope="col-2" class="text-center form-color-dark">Salzburg<br>(Flughafen Wien)</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">5</th>
                <td>
                    <div><?= $form->field($model, 'muc_str_fahrzeit_funktion')->checkbox()->label("Flughafen Stuttgart [Min]")?></div>
                </td>
                <td>
                    <div class="input-group sm-3"><?= $form->field($model, 'muc_str_fahrzeit_a_ist')->textInput([ 'placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model, 'muc_str_fahrzeit_a_neu')->textInput(['placeholder' => 'Neu'])->label("")?></div>
                </td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th scope="row">6</th>
                <td>
                    <div><?= $form->field($model, 'muc_fra_fahrzeit_funktion')->checkbox()->label("Flughafen Frankfurt [Min]")?></div>
                </td>
                <td></td>
                <td>
                    <div class="input-group sm-3"><?= $form->field($model, 'muc_fra_fahrzeit_achse_s_ist')->textInput([ 'placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model, 'muc_fra_fahrzeit_achse_s_neu')->textInput([ 'placeholder' => 'Neu'])->label("")?></div>
                </td>
                <td>
                    <div class="input-group sm-3"><?= $form->field($model, 'muc_fra_fahrzeit_achse_n_ist')->textInput([ 'placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model, 'muc_fra_fahrzeit_achse_n_neu')->textInput([ 'placeholder' => 'Neu'])->label("")?></div>
                </td>
                <td></td>
            </tr>
            <tr>
                <th scope="row">7</th>
                <td>
                    <div><?= $form->field($model, 'muc_vie_fahrzeit_funktion')->checkbox()->label("Flughafen Wien [Min]")?></div>
                </td>
                <td></td>
                <td></td>
                <td></td>
                <td>
                    <div class="input-group sm-3"><?= $form->field($model, 'muc_vie_fahrzeit_sal_ist')->textInput(['placeholder' => 'Ist'])->label("")?>
                        <?= $form->field($model, 'muc_vie_fahrzeit_sal_neu')->textInput([ 'placeholder' => 'Neu'])->label("")?></div>
                </td>
            </tr>
            </tbody>
            <thead>
            <tr>
                <th scope="col-1" class="form-color-dark text-center"></th>
                <th scope="col-3" class="form-color-dark">Zugangssituation MIV</th>
                <th scope="col-2" class="form-color-dark text-center">A9 (Autobahnende)</th>
                <th scope="col-2" class="form-color-dark text-center">AD Holledau</th>
                <th scope="col-2" class="form-color-dark text-center">AK München-Süd</th>
                <th scope="col-2" class="form-color-dark text-center">AK München-West</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <th scope="row">8</th>
                    <td>
                        <div><?= $form->field($model, "muc_miv_fahrzeit_funktion")->checkbox()->label("Fahrzeitänderung [Min]")?></div>
                    </td>
                    <td>
                        <div class="input-group sm-3"><?= $form->field($model, 'muc_miv_fahrzeit_a9_ist')->textInput(['placeholder' => 'Ist'])->label("")?>
                            <?= $form->field($model, 'muc_miv_fahrzeit_a9_neu')->textInput(['placeholder' => 'Neu'])->label("")?></div>
                    </td>
                    <td>
                        <div class="input-group sm-3"><?= $form->field($model, "muc_miv_fahrzeit_ad_ist")->textInput(['placeholder' => 'Ist'])->label("")?>
                            <?= $form->field($model, 'muc_miv_fahrzeit_ad_neu')->textInput(['placeholder' => 'Neu'])->label("")?></div>
                    </td>
                    <td>
                        <div class="input-group sm-3"><?= $form->field($model, 'muc_miv_fahrzeit_ak_s_ist')->textInput(['placeholder' => 'Ist'])->label("")?>
                            <?= $form->field($model, 'muc_miv_fahrzeit_ak_s_neu')->textInput([ 'placeholder' => 'Neu'])->label("")?></div>
                    </td>
                    <td>
                        <div class="input-group sm-3"><?= $form->field($model, 'muc_miv_fahrzeit_ak_w_ist')->textInput([ 'placeholder' => 'Ist'])->label("")?>
                            <?= $form->field($model, 'muc_miv_fahrzeit_ak_w_neu')->textInput([ 'placeholder' => 'Neu'])->label("")?></div>
                    </td>
                </tr>
            </tbody>
        </table>
    <?= Html::submitButton(Module::t('changeanalysis', 'Start'), ['class' => 'btn mediumButton pull-right', 'name' => 'page', 'value' => $page])?>
</div>
<?php ActiveForm::end(); ?>
