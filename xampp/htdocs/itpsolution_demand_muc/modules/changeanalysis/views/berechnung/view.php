<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 07.08.2019
 * Time: 09:47
 */

use app\modules\admin\Module;
use app\modules\core\models\Organisation;
use app\modules\core\models\OrganisationGroup;
use app\modules\core\models\OrganisationType;
use app\modules\core\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * @var int $page
 * @var \app\modules\changeanalysis\models\Berechnung $model
 */
$this->title = $model->getId();
?>
<div class="col-md-12">

    <?= DetailView::widget([
        'model' => $model,
        'options' => ['class' => 'table table-bordered detail-view panel-color bg-color'],
        'attributes' => [
            'berechnung_id',
            'user_id',
            'bezeichnung',
            'status',
            'anmerkung',
            'startzeitpunkt',
            'details',
            'muc_fahrzeit_funktion',
            'muc_fahrzeit_hbf_ist',
            'muc_fahrzeit_hbf_neu',
            'muc_fahrzeit_no_ist',
            'muc_fahrzeit_no_neu',
            'muc_fahrzeit_so_ist',
            'muc_fahrzeit_so_neu',
            'muc_fahrzeit_w_ist',
            'muc_fahrzeit_w_neu',
            'muc_takt_funktion',
            'muc_takt_hbf_ist',
            'muc_takt_hbf_neu',
            'muc_takt_no_ist',
            'muc_takt_no_neu',
            'muc_takt_so_ist',
            'muc_takt_so_neu',
            'muc_takt_w_ist',
            'muc_takt_w_neu',
            'muc_direktanbindung_funktion',
            'muc_direktanbindung_hbf_ist',
            'muc_direktanbindung_hbf_neu',
            'muc_direktanbindung_no_ist',
            'muc_direktanbindung_no_neu',
            'muc_direktanbindung_so_ist',
            'muc_direktanbindung_so_neu',
            'muc_direktanbindung_w_ist',
            'muc_direktanbindung_w_neu',
            'muc_hbf_produkt_funktion',
            'muc_hbf_produkt_janein',
            'muc_hbf_produkt_preisaufschlag',
            'muc_str_fahrzeit_funktion',
            'muc_str_fahrzeit_a_ist',
            'muc_str_fahrzeit_a_neu',
            'muc_fra_fahrzeit_funktion',
            'muc_fra_fahrzeit_achse_s_ist',
            'muc_fra_fahrzeit_achse_s_neu',
            'muc_fra_fahrzeit_achse_n_ist',
            'muc_fra_fahrzeit_achse_n_neu',
            'muc_vie_fahrzeit_funktion',
            'muc_vie_fahrzeit_sal_ist',
            'muc_vie_fahrzeit_sal_neu',
            'muc_miv_fahrzeit_funktion',
            'muc_miv_fahrzeit_a9_ist',
            'muc_miv_fahrzeit_a9_neu',
            'muc_miv_fahrzeit_ad_neu',
            'muc_miv_fahrzeit_ad_ist',
            'muc_miv_fahrzeit_ak_s_ist',
            'muc_miv_fahrzeit_ak_s_neu',
            'muc_miv_fahrzeit_ak_w_ist',
            'muc_miv_fahrzeit_ak_w_neu',
        ],
    ]); ?>

</div>

<div class="clearfix">
    <?= Html::a(Yii::t('app', 'back'), Yii::$app->getHomeUrl(), ['class' => 'btn mediumButton pull-left']) ?>
</div>
