<?php
namespace app\modules\changeanalysis\assets;
use yii\web\AssetBundle;

/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 09.08.2019
 * Time: 12:45
 */

class ChangeanalysisAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/changeanalysis/assets';

    public $js = [
        'changeanalysis.js'
    ];

    public $depends = [
        'yii\web\JQueryAsset'
    ];
}