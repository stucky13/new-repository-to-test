<?php

namespace app\modules\changeanalysis\controllers;

use app\components\BaseController;
use app\components\Constants;
use app\components\FormModel;
use app\modules\changeanalysis\models\Berechnung;
use app\modules\changeanalysis\models\BerechnungForm;
use app\modules\changeanalysis\Module;
use app\modules\core\components\BerechnungQuery;
use app\widgets\Alert;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;

/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 06.08.2019
 * Time: 16:12
 */
class BerechnungController extends BaseController
{
    /**
     * @param int $page
     * @return string|\yii\web\Response
     * Maja:
     */
    public function actionIndex($page = 1)
    {

        return $this->render("index", [
            'dataProvider' => new ActiveDataProvider(['query' => Berechnung::find()]),
            'page' => $page
        ]);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionView($id)
    {

        $berechnung = Berechnung::findOne($id);
        return $this->render("view", [
            'model' => $berechnung
        ]);
    }

    /**
     * @param $page
     * @return string|\yii\web\Response
     * @throws \Exception
     */
    public function actionCreate($page)
    {

        $model = new BerechnungForm();
        $model->userId = Yii::$app->getUser()->getId();
        $model->startzeitpunkt = date('Y-m-d H:i:s');

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                if ($model->save()) {
                    $page = \Yii::$app->request->post('page');
                    Alert::addSuccess(Module::t('changeanalysis', 'changeanalysis successfully created'));
                    return $this->redirect(["index?page=$page"]);
                } else {
                    Alert::addError(Module::t('changeanalysis', 'could not save'));
                }
            } else {
                Alert::addError(Module::t('changeanalysis', 'could not validate'));
            }

        }
        return $this->render("form", [
            'model' => $model,
            'page' => $page
        ]);
    }

    /**
     * @param $id
     * @param int $page
     * @return string|\yii\web\Response
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public function actionUpdate($id, $page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::ADMIN) && !Yii::$app->getUser()->can(Constants::EDIT_CHANGE_ANALYSIS)) {
            return $this->goHome();
        }
        $berechnung = Berechnung::findOne($id);
        if ($berechnung == null) {
            throw new BadRequestHttpException(Module::t('changeanalysis', 'Failed to find organisation with id' . $id));
        }

        $model = new BerechnungForm(FormModel::SCENARIO_UPDATE, $id);
        if ($model->laod(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            $page = \Yii::$app->request->post('page');
            Alert::addSuccess(Module::t('changeanalysis', 'berechnung %s successfully updated'), $model->berechnungId);
            return $this->redirect(["index?page=$page"]);
        } else {
            $model->berechnungId = $berechnung->getId();
            $model->userId = $berechnung->getUserId();
            $model->bezeichnung = $berechnung->getBezeichnung();
            $model->status = $berechnung->getStatus();
            $model->anmerkung = $berechnung->getAnmerkung();
            $model->startzeitpunkt = $berechnung->getStartzeitpunkt();
            $model->mucFahrzeitFunktion = $berechnung->getMucFahrzeitFunktion();
            $model->mucFahrzeitHbfIst = $berechnung->getMucFahrzeitHbfIst();
            $model->mucFahrzeitHbfNeu = $berechnung->getMucFahrzeitHbfNeu();
            $model->mucFahrzeitNoIst = $berechnung->getMucFahrzeitNoIst();
            $model->mucFahrzeitNoNeu = $berechnung->getMucFahrzeitNoNeu();
            $model->mucFahrzeitSoIst = $berechnung->getMucFahrzeitSoIst();
            $model->mucFahrzeitSoNeu = $berechnung->getMucFahrzeitSoNeu();
            $model->mucFahrzeitWIst = $berechnung->getMucFahrzeitWIst();
            $model->mucFahrzeitWNeu = $berechnung->getMucFahrzeitWNeu();
            $model->mucTaktFunktion = $berechnung->getMucTaktFunktion();
            $model->mucTaktHbfIst = $berechnung->getMucTaktHbfIst();
            $model->mucTaktHbfNeu = $berechnung->getMucTaktHbfNeu();
            $model->mucTaktNoIst = $berechnung->getMucTaktNoIst();
            $model->mucTaktNoNeu = $berechnung->getMucTaktNoNeu();
            $model->mucTaktSoIst = $berechnung->getMucTaktSoIst();
            $model->mucTaktSoNeu = $berechnung->getMucTaktSoNeu();
            $model->mucTaktWIst = $berechnung->getMucTaktWIst();
            $model->mucTaktWNeu = $berechnung->getMucTaktWNeu();
            $model->mucDirektanbindungFunktion = $berechnung->getMucDirektanbindungFunktion();
            $model->mucDirektanbindungHbfIst = $berechnung->getMucDirektanbindungHbfIst();
            $model->mucDirektanbindungHbfNeu = $berechnung->getMucDirektanbindungHbfNeu();
            $model->mucDirektanbindungNoIst = $berechnung->getMucDirektanbindungNoIst();
            $model->mucDirektanbindungNoNeu = $berechnung->getMucDirektanbindungNoNeu();
            $model->mucDirektanbindungSoIst = $berechnung->getMucDirektanbindungSoIst();
            $model->mucDirektanbindungSoNeu = $berechnung->getMucDirektanbindungSoNeu();
            $model->mucDirektanbindungWIst = $berechnung->getMucDirektanbindungWIst();
            $model->mucDirektanbindungWNeu = $berechnung->getMucDirektanbindungWNeu();
            $model->mucHbfProduktFunktion = $berechnung->getMucHbfProduktFunktion();
            $model->mucHbfProduktJanein = $berechnung->getMucHbfProduktJanein();
            $model->mucHbfProduktPreisaufschlag = $berechnung->getMucHbfProduktPreisaufschlag();
            $model->mucStrFahrzeitFunktion = $berechnung->getMucStrFahrzeitFunktion();
            $model->mucStrFahrzeitAIst = $berechnung->getMucStrFahrzeitAIst();
            $model->mucStrFahrzeitANeu = $berechnung->getMucStrFahrzeitANeu();
            $model->mucFraFahrzeitFunktion = $berechnung->getMucFraFahrzeitFunktion();
            $model->mucFraFahrzeitAchseSIst = $berechnung->getMucFraFahrzeitAchseSIst();
            $model->mucFraFahrzeitAchseSNeu = $berechnung->getMucFraFahrzeitAchseSNeu();
            $model->mucFraFahrzeitAchseNIst = $berechnung->getMucFraFahrzeitAchseNIst();
            $model->mucFraFahrzeitAchseNNeu = $berechnung->getMucFraFahrzeitAchseNNeu();
            $model->mucVieFahrzeitFunktion = $berechnung->getMucVieFahrzeitFunktion();
            $model->mucVieFahrzeitSalIst = $berechnung->getMucVieFahrzeitSalIst();
            $model->mucVieFahrzeitSalNeu = $berechnung->getMucVieFahrzeitSalNeu();
            $model->mucMivFahrzeitFunktion = $berechnung->getMucMivFahrzeitFunktion();
            $model->mucMivFahrzeitA9Ist = $berechnung->getMucMivFahrzeitA9Ist();
            $model->mucMivFahrzeitA9Neu = $berechnung->getMucMivFahrzeitA9Neu();
            $model->mucMivFahrzeitAdIst = $berechnung->getMucMivFahrzeitAdIst();
            $model->mucMivFahrzeitAdNeu = $berechnung->getMucMivFahrzeitAdNeu();
            $model->mucMivFahrzeitAkSIst = $berechnung->getMucMivFahrzeitAkSIst();
            $model->mucMivFahrzeitAkSNeu = $berechnung->getMucMivFahrzeitAkSNeu();
            $model->mucMivFahrzeitAkWIst = $berechnung->getMucMivFahrzeitAkWIst();
            $model->mucMivFahrzeitAkWNeu = $berechnung->getMucTaktWNeu();
        }
        return $this->render("form", [
            'model' => $model,
            'page' => $page
        ]);
    }

    public function actionRestore()
    {
        if (Yii::$app->getUser()->can(Constants::ADMIN)) {

        }
    }

    public function actionHelp()
    {

    }
}