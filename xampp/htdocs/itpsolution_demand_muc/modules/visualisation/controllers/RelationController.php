<?php
/**
 * Created by PhpStorm.
 * User: Simon Hümmer
 * Date: 18.01.2019
 * Time: 13:41
 */

namespace app\modules\visualisation\controllers;

use app\components\BaseController;
use app\components\SessionUtil;
use app\modules\data\models\Antworten;
use app\modules\data\models\Fragen;
use app\modules\data\models\FragenEnum;
use app\modules\data\models\Knoten;
use app\modules\data\models\ValueType;
use DateTime;
use Yii;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\Response;

class RelationController extends FilterController
{
    public function actionIndex()
    {
        $values = $this->buildFilterValues();

        return $this->render('index', [
            'streckeId' => $values['streckeId'],
            'georef' => $values['georef'],
            'filters' => $values['filters'],
            'value' => $values['value'],
            'operations' => $values['operations'],
            'sessionParameters' => $values['sessionParameters'],
            'filterRows' => $values['filterRows'],
            'counter' => $values['counter']
        ]);
    }

    public function actionGetKnots($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];
        if (!is_null($q)) {
            $subStrings = explode(' ', $q);

            $nameQueries = ['and'];
            $nrQueries = ['and'];
            $names = [];

            foreach ($subStrings as $subString) {
                $subString = str_replace('"', "", $subString);
                if (is_numeric($subString)) {
                    $nrQueries[] = "nr like \"%$subString%\"";
                } else {
                    $nameQueries[] = "name like \"%$subString%\"";
                    $names[] = $subString;
                }
            }

            if (count($nrQueries) > 1) {
                $stringQueries = ['or'];
                $stringQueries[] = $nameQueries;

                $knotQuery = Knoten::find()
                    ->where($nrQueries)
                    ->andWhere($stringQueries);
            } else {
                $knotQuery = Knoten::find()
                    ->where($nrQueries)
                    ->orWhere($nameQueries);
            }

            $knots = $knotQuery
                ->limit(20)
                ->all();

            $out['results'] = [];
            /** @var Knoten $knot */
            foreach ($knots as $knot) {
                $out['results'][] = ['id' => $knot->getNr(), 'text' => $knot->getNr() . " - " . $knot->getName()];
            }
        } else {
            $knots = Knoten::find()->orderBy('nr ASC')->limit(100)->all();

            $out['results'] = [];
            /** @var Knoten $knot */
            foreach ($knots as $knot) {
                $out['results'][] = ['id' => $knot->getNr(), 'text' => $knot->getNr() . " - " . $knot->getName()];
            }
        }
        if ($id > 0) {
            $knot = Knoten::find()->where(['nr' => $id])->one();
            $out['results'] = ['id' => $knot->getNr(), 'text' => $knot->getNr() . " - " . $knot->getName()];
        }
        return $out;
    }
}