<?php
/**
 * Created by PhpStorm.
 * User: Simon Hümmer
 * Date: 18.01.2019
 * Time: 13:41
 */

namespace app\modules\visualisation\controllers;


class MatrixController extends FilterController
{

    public function actionIndex()
    {
        $values = $this->buildFilterValues();

        return $this->render('index', [
            'streckeId' => $values['streckeId'],
            'georef' => $values['georef'],
            'filters' => $values['filters'],
            'value' => $values['value'],
            'operations' => $values['operations'],
            'sessionParameters' => $values['sessionParameters'],
            'filterRows' => $values['filterRows'],
            'counter' => $values['counter']
        ]);
    }


    public function actionRenderMatrix($data)
    {
        $data = json_decode($data);

        $stationsList = [];

        foreach ($data as $relation) {
            $stationsList[$relation->startStation->nr] = $relation->startStation->name;
            $stationsList[$relation->endStation->nr] = $relation->endStation->name;
        }

        $max = 1;
        $matrixEntries = [];
        $rowTotals = [];
        $colTotals = [];


        foreach ($stationsList as $vonNr => $vonName) {
            foreach ($stationsList as $zuNr => $zunName) {

                $value = 0;
                foreach ($data as $rel) {
                    if ($rel->startStation->nr == $vonNr && $rel->endStation->nr == $zuNr) {
                        $value = $rel->value;
                    }
                }

                $matrixEntries[$vonNr][$zuNr] = $value;

                $rowTotals[$vonNr] = (sizeof($rowTotals) <= $vonNr ? 0 : $rowTotals[$vonNr]) + $value;
                $colTotals[$zuNr] = (sizeof($colTotals) <= $zuNr ? 0 : $colTotals[$zuNr]) + $value;

                if ($value > $max) {
                    $max = $value;
                }

            }
        };

        return $this->renderPartial("_matrix", [
            'data' => $data,
            'stationsList' => $stationsList,
            'rowTotals' => $rowTotals,
            'colTotals' => $colTotals,
            'matrixEntries' => $matrixEntries,
            'max' => $max
        ]);
    }

}