<?php

/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 02.03.2018
 * Time: 14:47
 */

namespace app\modules\visualisation;

use Yii;

/**
 * Class Module
 * @package app\modules\visualisation
 */
class Module extends \yii\base\Module
{
    /**
     * @inheritdoc
     * Translates a message to the specified language.
     *
     * @param string $category the message category.
     * @param string $message the message to be translated.
     * @param array $params the parameters that will be used to replace the corresponding placeholders in the message.
     * @param string $language the language code (e.g. `en-US`, `en`). If this is null, the current
     * [[\yii\base\Application::language|application language]] will be used.
     * @return string the translated message.
     */
    public static function t($category, $message, $params = [], $language = null)
    {
        return Yii::t('modules/visualisation/' . $category, $message, $params, $language);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->defaultRoute = 'visualisation/area/index';
        $this->registerTranslations();
    }

    /**
     * Registers the module translations
     */
    public function registerTranslations()
    {
        Yii::$app->i18n->translations['modules/visualisation/*'] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'sourceLanguage' => 'en-US',
            'basePath' => '@app/modules/visualisation/messages',
            'fileMap' => [
                'modules/visualisation/visualisation' => 'visualisation.php',
            ],
        ];
    }

}
