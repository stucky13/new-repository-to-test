/**
 * @param mapId - the html element id
 * @param {number[]|number[][]} bounds - single pair of latLon or multible [[lat,lon], ...] pairs
 * @constructor
 */
let LeafletMap = function (mapId, bounds) {
    let self = this;

    /**
     * Holding the initialized leaflet map instance, only one map supported.
     * @type {Map} - the map instance
     * @private
     */
    let _map = null;

    /**
     * @typedef {Marker} CustomMarker
     * @property {MarkerIdentifier} markerIdentifier
     * @property {L.Icon} icon
     * @property {L.Icon} highlightIcon
     */

    /**
     * Holds all Markers which are added to the map.
     * @type {[CustomMarker]}
     * @private
     */
    let _markers = [];

    /**
     * Holds all hidden Markers.
     * @type {[CustomMarker]}
     * @private
     */
    let _hiddenMarker = [];

    /**
     * Holds the current selected/highlighted marker.
     * Will be set through {@link highlightMarker}
     * @type {CustomMarker|null}
     * @private
     */
    let _selectedMarker = null;

    /**
     * Holds a listView of spots. Used for marking the list items if a marker was clicked throuth the map.
     * Will be set through {@link highlightMarker}
     * @type {CustomMarker|null}
     * @private
     */
    let _listView = null;

    /**
     * If letiable is 'undefined' or null this sets a default value.
     * @param {*} letiable - the letiable to check for a given value
     * @param defaultValue - the default value to set
     * @return {*}
     * @private
     */
    function _getDefaultOnEmpty(letiable, defaultValue) {
        return (letiable === undefined || letiable === null) ? defaultValue : letiable;
    }

    /**
     * Centers the map to the given point.
     * Expected to be used for highlighting purpose.
     * @param {number[]} point
     * @private
     */
    function _centerMapToPoint(point) {
        _map.setView(point);
    }

    /**
     * Searching for a marker in the _markers array by the specific markerIdentifier.
     * @param {MarkerIdentifier} markerIdentifier
     * @returns {CustomMarker|null}
     * @private
     */
    function _findMarkerByIdentifier(markerIdentifier) {
        //TODO check for markerIdenfitier.id
        for (let i = 0; i < _markers.length; i++) {
            let marker = _markers[i];
            if (marker.markerIdentifier.type === markerIdentifier.type &&
                marker.markerIdentifier.lat === markerIdentifier.lat &&
                marker.markerIdentifier.lon === markerIdentifier.lon)
                return marker;
        }
        return null;
    }

    /**
     * Searching for a marker in the _markers array by the specific markerIdentifier.
     * @param {MarkerIdentifier} markerIdentifier
     * @returns {CustomMarker|null}
     * @private
     */
    function _findSpotMarkerById(id) {
        for (let i = 0; i < _markers.length; i++) {
            let marker = _markers[i];
            if (marker.markerIdentifier.type === MarkerType.SPOT &&
                marker.markerIdentifier.nr == id)
                return marker;
        }
        return null;
    }

    /**
     * Initializes the map and centers it to the given latitude and longitude position.
     * Also adds the contributors to the botten right corner.
     * @param {string} mapID - the html dom id of the map
     * @param {number} latitude
     * @param {number} longitude
     * @param {number} [zoom] - only integer, decimal will be rounded by leaflet api
     */
    function _initmap(mapID, bounds, zoom) {
        zoom = _getDefaultOnEmpty(zoom, DEFAULT_ZOOM);
        _map = new L.Map(mapID, {zoom: zoom});

        //sets the items on the right bottem corner on the map
        let osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
        let osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
        let osm = new L.TileLayer(osmUrl, {attribution: osmAttrib});

        _map.addLayer(osm);

        if (isPoint(bounds)) {
            _map.setView(bounds);
            return;
        }

        // adding pad to create a little gap between map corners and spots to fit
        // so the icon can be displayed without getting cutted
        bounds = new L.latLngBounds(bounds).pad(0.5);
        _map.fitBounds(bounds);
    }

    let isPoint = function (bounds) {
        return bounds.length === 2 &&
            !isNaN(bounds[0]) &&
            !isNaN(bounds[1]);
    };


    /**
     * Draws an area on the initialized map with the given color or uses red.
     * @param {[{NR_PUNKT:number, LAT:number, LON:number}]} tarifGrenzePunkte - an array of arrays [[lat, lon], ...]
     * @param {string} [color] - default: 'red', e.g. '#ffffff'
     * @param {number} [weight] - default: 3, the width of the line
     * @param {string} [fillColor] - default: 'red', e.g. '#ffffff'
     * @param {number} [fillOpacity] - default: 0.2
     */
    this.drawArea = function (tarifGrenzePunkte, color, weight, fillColor, fillOpacity) {
        color = _getDefaultOnEmpty(color, DEFAULT_COLOR);
        weight = _getDefaultOnEmpty(weight, DEFAULT_LINE_WEIGHT);
        fillColor = _getDefaultOnEmpty(fillColor, DEFAULT_AREA_FILL_COLOR);
        fillOpacity = _getDefaultOnEmpty(fillOpacity, DEFAULT_FILL_OPACITY);

        return L.polygon(tarifGrenzePunkte, {
            color: color,
            weight: weight,
            fillColor: fillColor,
            fillOpacity: fillOpacity
        }).addTo(_map);
    };

    /**
     * Centers the map to the middle of the points and resize it automatically to fit the area.
     * @param {number[][]} points
     */
    this.fitMapToArea = function (points) {
        _map.fitBounds(points);
    };

    /**
     * Creates a popup for a marker on the map.
     * @param {CustomMarker} marker
     * @param {string} text - the text which will show up
     */
    function _bindPopupToMarker(marker, text) {
        marker.bindPopup(text);
    }

    function _isListViewSet() {
        return _listView !== undefined && _listView !== null;
    }

    function _addSpotOnClickToHighlightListView(markerIdentifier, marker) {
        if (_isListViewSet() && markerIdentifier.type === MarkerType.SPOT) {
            marker.on('click', function (event) {
                let currMarker = event.target;
                let currIdentity = currMarker.markerIdentifier;
                _listView.val(currIdentity.nr);
                //TODO bug, click the marker *on the map* doesn't show popup
                self.highlightMarker(currIdentity.nr, true, true);
            });
        }
    }

    function _setCustomPropertiesForHighlightingAndIdentifiingPurpose(marker, icon, highlightIcon, markerIdentifier) {
        marker.icon = icon;
        marker.highlightIcon = highlightIcon;
        marker.markerIdentifier = markerIdentifier;
    }

    /**
     * Creating a marker with Leaflet and adding custom properties and listView on click event.
     * @param {MarkerIdentifier} markerIdentifier
     * @param zIndex
     * @param icon
     * @param highlightIcon
     * @private
     */
    function _createMarker(markerIdentifier, zIndex, icon, highlightIcon) {
        let marker = {};

        //multiply zIndex by 10 because low values have no effect
        zIndex = zIndex * 10;

        if (icon === undefined || icon === null)
            marker = L.marker([markerIdentifier.lat, markerIdentifier.lon], {zIndexOffset: zIndex});
        else {
            marker = L.marker([markerIdentifier.lat, markerIdentifier.lon], {zIndexOffset: zIndex, icon: icon});
        }

        _setCustomPropertiesForHighlightingAndIdentifiingPurpose(marker, icon, highlightIcon, markerIdentifier);
        _addSpotOnClickToHighlightListView(markerIdentifier, marker);

        return marker;
    }

    /**
     * Adds a default marker with the default Leaflet icon to the map.
     * Holding the instance of the marker to the private data structure for manipulation.
     * If there is already a Marker with the given markerIdentifier an Error is thrown.
     * @param markerId
     * @param lat
     * @param lon
     * @param nr
     * @param icon
     * @param iconHighlight
     * @param text
     * @param {number} zIndex
     * @throws {Error} Marker already exists
     */
    this.addMarker = function (markerId, lat, lon, nr, icon, iconHighlight, text, zIndex, mayChange) {
        let identifier = new MarkerIdentifier(markerId, lat, lon, nr);
        let marker = _addMarkerWithCustomPic(identifier, zIndex, icon, iconHighlight);
        let popupText = text;

        if ($('#spot-map').length) {
            let button = mayChange ? "</br><a id='submit-ref-station-nr'>als Referenzhaltestelle setzen</a>" : "";
            let input = "</br><input type='hidden' id='ref-station-nr' value='" + nr + "'>";

            popupText = text + button + input;
        }

        if (marker.getPopup() !== undefined) {
            //popupText = text + ' &<br>' + marker.getPopup().getContent();
        }

        _bindPopupToMarker(marker, popupText);
        return marker;
    };

    /**
     * Adds a default marker with the default Leaflet icon to the map.
     * Holding the instance of the marker to the private data structure for manipulation.
     * On existence the marker gets returned.
     * @param {MarkerIdentifier} markerIdentifier
     * @param {number} zIndex
     * @param {Icon} icon - url to picture
     * @param highlightIcon
     * @return {CustomMarker}
     */
    function _addMarkerWithCustomPic(markerIdentifier, zIndex, icon, highlightIcon) {
        let marker = _findMarkerByIdentifier(markerIdentifier);

        if (marker === null) {
            marker = _createMarker(markerIdentifier, zIndex, icon, highlightIcon).addTo(_map);
            _markers.push(marker);
        }

        return marker;
    }

    /**
     * Highlights a marker found by it's markerIdentifier.
     * On the default settings it centers the map and opens the popup.
     * If the marker contains a highlightIcon set by the {@link _addMarkerWithCustomPic} it will also change to this icon.
     * @param {number} id
     * @param {boolean} [openPopup]
     * @param {boolean} [centerTo]
     */
    this.highlightMarker = function (id, openPopup, centerTo) {
        openPopup = _getDefaultOnEmpty(openPopup, true);
        centerTo = _getDefaultOnEmpty(centerTo, true);

        if (_selectedMarker !== null) {
            _selectedMarker.setIcon(_selectedMarker.icon);
        }

        _selectedMarker = _findSpotMarkerById(id);

        if (_selectedMarker === null)
            return;

        if (_selectedMarker.highlightIcon !== undefined)
            _selectedMarker.setIcon(_selectedMarker.highlightIcon);

        if (openPopup)
            _selectedMarker.openPopup();

        if (centerTo)
            _centerMapToPoint([_selectedMarker.markerIdentifier.lat, _selectedMarker.markerIdentifier.lon]);
    };

    /**
     * Simple function for drawing a line between the given points.
     * The fist point given will be the start and the last the end of the line.
     * @param {number[][]} points - as pairs [ [LAT, LON], ... ]
     * @param {string} [color] - default: 'red', e.g. '#ffffff'
     * @param {number} [weight] - default: 3, the width of the line
     * @param {string} [fillColor] - default: 'red', e.g. '#ffffff'
     * @param {number} [opacity] - default: 1
     * @param {string} [text] - popup text
     */
    this.drawLineBetweenPoints = function (points, color, weight, fillColor, opacity, text) {
        color = _getDefaultOnEmpty(color, DEFAULT_COLOR);
        weight = _getDefaultOnEmpty(weight, DEFAULT_LINE_WEIGHT);
        fillColor = _getDefaultOnEmpty(fillColor, DEFAULT_FILL_COLOR);
        opacity = _getDefaultOnEmpty(opacity, DEFAULT_OPACITY);

        let line = L.polyline(points, {
            color: color,
            weight: weight,
            fillColor: fillColor,
            opacity: opacity
        });
        if (text) {
            line.bindPopup(text);
        }
        line.addTo(_map);
    };

    /**
     * Simple function for drawing a line between to points.
     * @param {number[]} startPoint
     * @param {number[]} endPoint
     * @param {string} [color] - default: 'red', e.g. '#ffffff'
     * @param {number} [weight] - default: 3, the width of the line
     * @param {string} [fillColor] - default: 'red', e.g. '#ffffff'
     * @param {number} [opacity] - default: 1
     * * @param {string} [text] - popup text
     */
    this.drawLineBetweenStartEnd = function (startPoint, endPoint, color, weight, fillColor, opacity, text) {
        this.drawLineBetweenPoints([startPoint, endPoint], color, weight, fillColor, opacity, text)
    };

    /**
     * Sets the internal listView.
     * Used by on click event from the marker for highlighting the marker in the list on map click.
     * @param listId
     */
    this.setListView = function (listId) {
        _listView = $('#' + listId);
    };

    this.clearMap = function () {
        _map.remove();
    };

    /**
     * Hides all markers of a specific type.
     * @param {MarkerType} type
     */
    this.hideMarker = function (type) {
        _markers.forEach(function (marker) {
            if (marker.markerIdentifier.type === type) {
                _map.removeLayer(marker);
                _hiddenMarker.push(marker);
            }
        });
    };

    this.removeStrecke = function () {
        for (let i in _map._layers) {
            if (_map._layers[i]._path != undefined) {
                try {
                    _map.removeLayer(_map._layers[i]);
                } catch (e) {
                    console.log("problem with " + e + m._layers[i]);
                }
            }
        }
    };

    this.removeMarker = function () {
        for (let i = _markers.length - 1; i >= 0; i--) {
            let marker = _markers[i];
            _map.removeLayer(marker);
            _markers.pop();

        }
    };

    /**
     * Shows all markers of a specific type.
     * @param {MarkerType} type
     */
    this.showMarker = function (type) {
        for (let i = _hiddenMarker.length - 1; i >= 0; i--) {
            let marker = _hiddenMarker[i];
            if (marker.markerIdentifier.type === type) {
                _map.addLayer(marker);
                _hiddenMarker.pop();
            }
        }
    };

    //Constructor calls
    _initmap(mapId, bounds);
};

/**
 * Used for identify a marker in the internal {@link _markers} array
 * @param {MarkerType} type
 * @param {number} lat
 * @param {number} lon
 * @param {number} nr
 * @constructor
 */
let MarkerIdentifier = function (type, lat, lon, nr) {
    this.type = type;
    this.lat = lat;
    this.lon = lon;
    this.nr = nr;
};

/**
 * @typedef {Object} MarkerType
 */
let MarkerType = Object.freeze({
    SPOT: "SPOT",
    STOP: "STOP",
    STATION: "STATION",
});
