$(document).ready(function () {
    var index = $('#counter').val();
    $('#filter-select').change(function () {
            index++;
            getAndSetFilterAndValues($(this).val(), index);
        }
    );

    for (let i = 1; i <= index; i++) {
        addCloseListener(i);
    }
});

function getAndSetFilterAndValues(id, index) {
    $.ajax({
        url: baseUrl + "visualisation/relation/get-filter-values",
        data: {id: id, index: index},
        success: function (data) {
            $('#filter-table tbody').append(data);
            addCloseListener(index);
        },
        error: function (data) {
            //TODO: Errorhandling
        }
    });
}

function addCloseListener(index) {
    $('#delete-' + index).click(function () {
            $(this).closest('tr').remove();
        }
    );
}

function calculateData(filters, startId, startStations, endId, endStations, operation, valueId) {
    var result = "";
    $.ajax({
        async: false,
        url: baseUrl + "visualisation/filter/calculate-relation",
        data: {
            filters: JSON.stringify(filters),
            startId: startId,
            startStations: JSON.stringify(startStations),
            endId: endId,
            endStations: JSON.stringify(endStations),
            operation: operation,
            valueId: valueId
        },
        success: function (data) {
            result = data;
        }
    });
    return result;
}
