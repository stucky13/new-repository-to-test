<?php
/**
 * Created by PhpStorm.
 * User: Michi
 * Date: 07.03.2018
 * Time: 09:23
 */

namespace app\modules\visualisation\assets;

use yii\web\AssetBundle;

class RelationAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/visualisation/assets';
    public $js = [
        'relation.js'
    ];
    public $css = [
        'zaehldaten.css',
        'filter.css'
    ];
    public $depends = [
        VisualisationAsset::class
    ];

    public function init()
    {
        parent::init();
        $this->publishOptions['forceCopy'] = true;
    }
}