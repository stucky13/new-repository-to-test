/**
 * Basic ajax call for fast reuse.
 * @param {string} moduleURL
 * @param {Object} data - data structure as JSON
 * @param {function} callback - the callback to do operations on the returned values
 * @param {function} errorCallback - callback for error handling
 * @private
 */
function _basic_get_ajax_call(moduleURL, data, callback, errorCallback) {
    $.ajax({
        url: baseUrl + moduleURL,
        data: data,
        success: function (jsonData) {
            jsonData = JSON.parse(jsonData);
            if (jsonData.length > 0 || jsonData !== undefined || jsonData !== null)
                callback(jsonData);
        },
        error: function (error) {
            if (errorCallback !== undefined && errorCallback !== null)
                errorCallback(error);
            else
                console.log(error.responseText);
        }
    });
}