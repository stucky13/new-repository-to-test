const COLORS = [
    '#ff0000',
    '#ffff00',
    '#00ff00',
    '#00ffff',
    '#ff00ff',
    '#0000ff',
    '#ff8888',
    '#ffff88',
    '#88ff88',
    '#88ffff',
    '#ff88ff',
    '#8888ff'
];

const
    Z_INDEX_SPOT = 9,
    Z_INDEX_STOP = 0,
    Z_INDEX_REF_STOP = 1,
    Z_INDEX_STATION = 5;


const
    DEFAULT_ZOOM = 12,
    DEFAULT_OPACITY = 1,
    DEFAULT_LINE_WEIGHT = 2,
    DEFAULT_FILL_OPACITY = 0.2;

const
    DEFAULT_COLOR = 'black',
    DEFAULT_FILL_COLOR = 'red',
    DEFAULT_AREA_FILL_COLOR = 'red';