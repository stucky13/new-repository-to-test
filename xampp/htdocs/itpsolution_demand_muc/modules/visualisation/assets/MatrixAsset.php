<?php
/**
 * Created by PhpStorm.
 * User: Michi
 * Date: 07.03.2018
 * Time: 09:23
 */

namespace app\modules\visualisation\assets;

use yii\web\AssetBundle;

class MatrixAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/visualisation/assets';
    public $js = [
        'matrix.js'
    ];
    public $css = [
        'matrix.css',
        'zaehldaten.css',
        'filter.css'
    ];
    public $depends = [
        VisualisationAsset::class
    ];

    public function init()
    {
        parent::init();
        $this->publishOptions['forceCopy'] = true;
    }
}