<?php

namespace app\modules\visualisation\widgets;

use yii\base\Widget;

/**
 * Class FilterWidget
 * @package app\modules\viualisation\widgets
 */
class FilterWidget extends Widget
{
    /**
     * @var array mapping frage_id - frageestellung for georef questions
     */
    public $georef;

    /**
     * @var array mapping frage_id - frageestellung for filter questions
     */
    public $filters;

    /**
     * @var array mapping frage_id - frageestellung for value questions
     */
    public $value;

    /**
     * @var array the operations
     */
    public $operations;

    /**
     * @var array the parameters of the session
     */
    public $sessionParameters;

    /**
     * @var array the filter rows
     */
    public $filterRows;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     */
    public function run()
    {
        return $this->render('_filter_widget', [
            'georef' => $this->georef,
            'filters' => $this->filters,
            'value' => $this->value,
            'operations' => $this->operations,
            'sessionParameters' => $this->sessionParameters,
            'filterRows' => $this->filterRows
        ]);
    }
}
