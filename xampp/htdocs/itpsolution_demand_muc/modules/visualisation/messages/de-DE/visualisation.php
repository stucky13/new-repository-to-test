<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 05.03.2018
 * Time: 10:10
 */

return [
    'matrix name' => 'Quelle-Ziel-Verkehrsnachfrage',
    'from' => 'Von',
    'to' => 'Nach',
    'break in' => 'Einbrechend',
    'break out' => 'Ausbrechend',
    'total' => 'Gesamt',
    'count' => 'Zähldaten-Zeitreihe',
    'download' => 'Herunterladen',


    // map, matrix
    'choose value question' => 'Wähle die Frage zur Berechnung aus',
    'question id' => 'Frage Id',
    'question formulation' => 'Fragestellung',
    'options' => 'Optionen',
    'input field' => 'Eingabefeld',
    'choose filter criteria' => 'Wähle Filterkriterien aus',
    'choose end knot/s' => 'Wähle Endstationen aus',
    'choose start knot/s' => 'Wähle Startstationen aus',
    'choose start question' => 'Wähle eine Startfrage aus',
    'choose end question' => 'Wähle eine Endfrage aus',
    'calculate now' => 'Jetzt berechnen'
];