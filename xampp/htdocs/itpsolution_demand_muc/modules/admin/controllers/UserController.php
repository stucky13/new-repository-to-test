<?php

namespace app\modules\admin\controllers;

use app\components\BaseController;
use app\components\Constants;
use app\components\FormModel;
use app\components\SessionUtil;
use app\modules\admin\models\OrganisationSessionForm;
use app\modules\admin\models\RepUserOrganisationForm;
use app\modules\admin\models\UserForm;
use app\modules\admin\models\UserOrganisationForm;
use app\modules\admin\models\UserSearchForm;
use app\modules\admin\Module;
use app\modules\core\models\AuthAssignment;
use app\modules\core\models\Permission;
use app\modules\core\models\User;
use app\widgets\Alert;
use Throwable;
use Yii;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Response;

/**
 * Class UserController
 *
 * @package app\modules\admin\controllers
 */
class UserController extends BaseController
{

    /**
     * Displays the overview.
     * @param int $page
     * @return string the content
     * @return string|Response
     * @throws \Exception
     * @throws Throwable
     */
    public function actionIndex($page = 1)
    {
        if (Yii::$app->getUser()->can(Constants::VIEW_ALL_USERS) || Yii::$app->getUser()->can(Constants::ADMIN)) {
            $userSearch = new UserSearchForm();
            $userSearch->load(Yii::$app->request->post());
        } else if (Yii::$app->getUser()->can(Constants::VIEW_USER)) {
            $organisationId = SessionUtil::getSessionOrganisationId();
            $userSearch = new UserSearchForm(['organisation_id' => $organisationId]);
            $userSearch->load(Yii::$app->request->post());
        } else {
            $userSearch = new UserSearchForm(['user_id' => Yii::$app->getUser()->getId()]);
        }

        $allUsers = ArrayHelper::map(User::find()->all(), 'user_id', 'username');

        return $this->render("index", [
            'dataProvider' => $userSearch->search(Yii::$app->getRequest()->queryParams),
            'allUsers' => $allUsers,
            'searchModel' => $userSearch,
            'page' => $page
        ]);
    }

    /**
     * Displays the detail view for the specified user.
     *
     * @param int $id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid user id
     * @throws Throwable
     */
    public function actionView($id, $page = 1)
    {
        if (
            !Yii::$app->getUser()->can(Constants::ADMIN) &&
            (Yii::$app->getUser()->can(Constants::VIEW_USER) && SessionUtil::getSessionOrganisation()->getRepresentativeId() != Yii::$app->getUser()->getId()) &&
            !Yii::$app->getUser()->can(Constants::VIEW_ALL_USERS) &&
            Yii::$app->getUser()->getId() != $id) {
            return $this->goHome();
        }
        $user = User::findOne($id);
        if ($user == null) {
            throw new BadRequestHttpException(Module::t('admin', "Invalid user id ") . $id);
        }

        return $this->render('view', [
            'user' => $user,
            'page' => $page
        ]);
    }

    /**
     * Displays the user form.
     *
     * @param int $page
     * @return string the content
     * @throws Throwable
     * @throws \yii\base\Exception
     * @throws StaleObjectException
     */
    public function actionCreate($page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::EDIT_USER) && SessionUtil::getSessionOrganisation()->getRepresentativeId() != Yii::$app->getUser()->getId()) {
            return $this->goHome();
        }
        $model = new UserForm();
        $userOrganisations = [new UserOrganisationForm()];

        if ($model->load(Yii::$app->request->post())) {

            $userOrganisations = UserOrganisationForm::createMultiple(UserOrganisationForm::className());
            UserOrganisationForm::loadMultiple($userOrganisations, Yii::$app->request->post());
            $validOrganisations = UserOrganisationForm::validateMultiple($userOrganisations);

            $password = $this->generatePassword();
            $model->password = $password;
            $model->passwordRepeat = $password;

            if (!$model->isActive) {
                $model->isActive = false;
            }

            $valid = $model->validate() && $validOrganisations;
            $model->organisations = $userOrganisations;

            if ($valid && $model->save()) {
                Alert::addSuccess(Module::t('admin', 'user successfully created'));
                $page = Yii::$app->request->post('page');

                return $this->redirect(["index?page=$page"]);
            }
        }

        $assignableOrganisations = [];
        foreach (Yii::$app->getUser()->getIdentity()->getAssignableOrganisationsForUser()->all() as $org) {
            $assignableOrganisations[$org->getId()] = $org->getTitle();
        }

        $permissionArray = [];
        foreach (Constants::PERMISSION_GROUP_TITLES as $groupId => $title) {
            $permissionArray[$title] = Permission::find()->where(["permission_group_id" => $groupId])->andWhere(['is_assignable' => 1])->all();
        }

        return $this->render("form", [
            'model' => $model,
            'userOrganisations' => $userOrganisations,
            'repOrganisations' => null,
            'assignableOrganisations' => $assignableOrganisations,
            'allPermissions' => $permissionArray,
            'isAdmin' => false,
            'page' => $page,
            'editable' => true
        ]);
    }

    /**
     * @return string
     */
    private function generatePassword()
    {
        return str_shuffle(substr(str_shuffle('abcdefghjkmnpqrstuvwxyz'), 0, 4) . substr(str_shuffle('!$%&=?*-:;.,+~@_'), 0, 1) . substr(str_shuffle('123456789'), 0, 1));
    }

    /**
     * Displays the user edit form.
     *
     * @param int $id the user id
     *
     * @param int $page
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid user id
     * @throws Throwable
     * @throws \yii\base\Exception
     * @throws StaleObjectException
     */
    public function actionUpdate($id, $page = 1)
    {
        /** @var User $user */
        $user = User::findOne($id);
        if (!Yii::$app->getUser()->can(Constants::EDIT_USER) && !$user->hasUserAsRepresentative(Yii::$app->getUser()->getId()) && (Yii::$app->getUser()->getId() != $id)) {
            return $this->goHome();
        }
        if ($user == null) {
            throw new BadRequestHttpException(Module::t('admin', "Failed to find user with id ") . $id);
        }

        $userOrganisations = [new UserOrganisationForm()];
        $repOrganisations = null;
        $model = new UserForm(FormModel::SCENARIO_UPDATE, $id);

        if ($model->load(Yii::$app->request->post())) {

            $userOrganisations = UserOrganisationForm::createMultiple(UserOrganisationForm::className());
            UserOrganisationForm::loadMultiple($userOrganisations, Yii::$app->request->post());
            $validUserOrganisations = UserOrganisationForm::validateMultiple($userOrganisations);

            $repUserOrganisations = RepUserOrganisationForm::createMultiple(RepUserOrganisationForm::className());
            RepUserOrganisationForm::loadMultiple($repUserOrganisations, Yii::$app->request->post());
            $validRepUserOrganisations = RepUserOrganisationForm::validateMultiple($repUserOrganisations);

            $valid = $model->validate() && $validUserOrganisations && $validRepUserOrganisations;

            $model->organisations = $userOrganisations;
            $model->repOrganisations = $repUserOrganisations;

            if ($valid && $model->save()) {
                if (!$user->isActive() && $model->isActive) {
                    $password = $this->generatePassword();

                    $user->setPassword($password);
                    $user->is_password_change_required = 1;
                    $user->save();
                    Yii::$app->mailer->compose('passwordChange', ['user' => $user, 'password' => $password])
                        ->setFrom(Constants::EMAIL_FROM)
                        ->setTo($user->getEmail())
                        ->setSubject(Module::t('admin', 'Your BW account password was reset'))
                        ->send();
                }
                $page = Yii::$app->request->post('page');

                if (!empty($error)) {
                    Alert::addError($error);
                }

                Alert::addSuccess(Module::t('admin', 'user %s successfully updated'), $model->firstName . ' ' . $model->lastName);
                return $this->redirect(["index?page=$page"]);
            } else {
                Alert::addError('fail');
            }
        } else {
            $model->userId = $id;
            $model->username = $user->getUsername();
            $model->firstName = $user->getFirstName();
            $model->lastName = $user->getLastName();
            $model->email = $user->getEmail();
            $model->phone1 = $user->getPhone1();
            $model->phone2 = $user->getPhone2();
            $model->phone3 = $user->getPhone3();
            $model->company = $user->getCompany();
            $model->address1 = $user->getAddress1();
            $model->address2 = $user->getAddress2();
            $model->address3 = $user->getAddress3();
            $model->address4 = $user->getAddress4();
            $model->isActive = $user->isActive();

            $userOrganisations = [];
            foreach ($user->getUserOrganisations()->all() as $userOrg) {
                $organisationForm = new UserOrganisationForm();
                $organisationForm->user_id = $user->getId();
                $organisationForm->organisation_id = $userOrg->getId();
                $organisationForm->permissions = array_fill_keys($user->getPermissionTitlesByOrganisationId($userOrg->getId()), 1);
                $userOrganisations[] = $organisationForm;
            }
            if (sizeof($userOrganisations) == 0) {
                $userOrganisations = [new UserOrganisationForm()];
            }

            $repOrganisations = [];

            foreach ($user->getRepresentativeOrganisations()->all() as $repOrg) {
                $organisationForm = new RepUserOrganisationForm();
                $organisationForm->user_id = $user->getId();
                $organisationForm->organisation_id = $repOrg->getId();
                $organisationForm->permissions = array_fill_keys($user->getPermissionTitlesByOrganisationId($repOrg->getId()), 1);
                $repOrganisations[] = $organisationForm;
            }
            if (sizeof($repOrganisations) == 0) {
                $repOrganisations = null;
            }
        }

        $assignableOrganisations = [];
        foreach (Yii::$app->getUser()->getIdentity()->getAssignableOrganisationsForUser($id)->all() as $org) {
            $assignableOrganisations[$org->getId()] = $org->getTitle();
        }

        $isAdmin = AuthAssignment::find()->where(['item_name' => 'admin', 'user_id' => $user->getId()])->one();

        $permissionArray = [];
        foreach (Constants::PERMISSION_GROUP_TITLES as $groupId => $title) {
            $permissionArray[$title] = Permission::find()->where(["permission_group_id" => $groupId])->andWhere(['is_assignable' => 1])->all();
        }

        return $this->render("form", [
            'model' => $model,
            'userOrganisations' => $userOrganisations,
            'repOrganisations' => $repOrganisations,
            'assignableOrganisations' => $assignableOrganisations,
            'isAdmin' => $isAdmin,
            'page' => $page,
            'allPermissions' => $permissionArray,
            'editable' => Yii::$app->getUser()->can(Constants::EDIT_USER) || $user->hasUserAsRepresentative(Yii::$app->getUser()->getId())
        ]);
    }

    /**
     * Deletes the specified user.
     *
     * @param int $id the user id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid user id
     * @throws Exception
     * @throws \Exception
     * @throws Throwable
     */
    public function actionDelete($id)
    {
        /** @var User $user */
        $user = User::findOne($id);
        if ($user == null) {
            throw new BadRequestHttpException("Failed to find user with id " . $id);
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user->is_deleted = 1;
            $user->save();

            $authAssignments = AuthAssignment::find()->where(['user_id' => $user->getId()]);
            foreach ($authAssignments->all() as $authAssignment) {
                $authAssignment->delete();
            }

            $transaction->commit();
            Alert::addSuccess(Module::t("admin", "user %s successfully deleted"), $user->getFirstName() . ' ' . $user->getLastName());
        } catch (Exception $e) {
            print_r($e->getMessage());
            $transaction->rollBack();
            Alert::addError(Module::t("admin", "user %s couldn't be deleted"), $user->getFirstName() . ' ' . $user->getLastName());
        }

        return $this->redirect(['index']);
    }

    /**
     * Sets the organisation for the session
     */
    public function actionSetOrganisation()
    {
        $model = new OrganisationSessionForm();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                SessionUtil::setSessionOrganisationId($model->organisation);
            } else {
                Yii::$app->response->statusCode = 400;
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderPartial('@app/modules/admin/views/user/_organisation_dropdown', [
                'model' => new OrganisationSessionForm()
            ]);
        } else {
            return $this->goHome();
        }
    }

    /**
     * Changes is_new of the given user to false and is_active to true
     * @param $id User Id
     * @throws BadRequestHttpException
     * @throws BadRequestHttpException
     * @throws \yii\base\Exception
     */
    public function actionRelease($id)
    {
        /** @var User $user */
        $user = User::find()->where(['user_id' => $id])->one();
        if (!$user) {
            throw new BadRequestHttpException("Invalid user id $id");
        }
        $user->is_new = 0;
        $user->is_active = 1;
        $password = $this->generatePassword();
        $user->setPassword($password);
        $user->is_password_change_required = 1;

        $user->save();

        Yii::$app->mailer->compose('initialLogin', ['user' => $user, 'password' => $password])
            ->setFrom(Constants::EMAIL_FROM)
            ->setTo($user->getEmail())
            ->setSubject(Module::t('admin', 'Your BW account password was created'))
            ->send();

        $this->goHome();
    }

    /**
     * Changes is_new of the given user to false and is_active to false
     * @param $id
     * @throws BadRequestHttpException
     */
    public function actionReject($id)
    {
        /** @var User $user */
        $user = User::find()->where(['user_id' => $id])->one();
        if (!$user) {
            throw new BadRequestHttpException("Invalid user id $id");
        }
        $user->is_new = 0;
        $user->is_active = 0;
        $user->save();
        $this->goHome();
    }

    public function actionReactivate($id)
    {
        /** @var User $user */
        $user = User::find()->where(['user_id' => $id])->one();
        if ($user == null) {
            throw new BadRequestHttpException("Failed to find user with id " . $id);
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $user->is_deleted = 0;
            $user->save();
            $transaction->commit();
            Alert::addSuccess(Module::t("admin", "user %s successfully reactivated"), $user->getFullName());
        } catch (Exception $e) {
            print_r($e->getMessage());
            $transaction->rollBack();
            Alert::addError(Module::t("admin", "user %s couldn't be reactivated"), $user->getFullName());
        }
        return $this->redirect(['index']);
    }

    /** Resets the password */
    public function actionResetPassword($id)
    {
        /** @var User $user */
        $user = User::find()->where(['user_id' => $id])->one();
        if (!$user) {
            throw new BadRequestHttpException("Invalid user id $id");
        }

        $password = $this->generatePassword();

        $user->setPassword($password);
        $user->is_password_change_required = 1;

        if ($user->save()) {
            Yii::$app->mailer->compose('passwordChange', ['user' => $user, 'password' => $password])
                ->setFrom(Constants::EMAIL_FROM)
                ->setTo($user->getEmail())
                ->setSubject(Module::t('admin', 'Your BW account password was reset'))
                ->send();
        }

        return $this->redirect('index');
    }

    /**
     * Representatives can confirm if users should still be declared as active
     * @param $id
     * @throws BadRequestHttpException
     */
    public function actionConfirm($id)
    {
        /** @var User $user */
        $user = User::find()->where(['user_id' => $id])->one();
        if (!$user) {
            throw new BadRequestHttpException("Invalid user id $id");
        }
        $user->is_new = 0;
        $user->is_active = 1;
        $user->save();
        $this->goHome();
    }
}