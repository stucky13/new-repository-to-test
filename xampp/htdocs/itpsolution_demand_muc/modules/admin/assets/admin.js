/**
 * Created by Wenk on 22.02.2018.
 */

$(document).ready(function () {
    // Change assignable permissions for organisation by organisation type
    $('#organisationform-organisationtypeid').change(function () {
        getAndSetOrganisationPermissions($(this).val());
    });

    // Change assignable permissions for user by organisation
    $('select[id*=-organisation_id]').each(function (index, value) {
        getUserPermissionsByUserAndOrg($(this), false);
    });
    // Syncs users. Called when button is clicked
    $('#syncButton').on('click', function () {
        syncUsers();
    });
});

// Reregister the change listeners on all organisation dropdowns. Called when one dropdown is clicked
function registerListeners() {
    $('.user_org_dropdown').unbind();
    $('.user_org_dropdown').change(function () {
        getUserPermissionsByUserAndOrg($(this), true);
    });
}

// Change assignable permissions for organisation by organisation type
function getAndSetOrganisationPermissions(id) {
    $.ajax({
        url: baseUrl + "admin/organisation/get-assignable-permissions-by-type",
        data: {id: id},
        success: function (data) {
            showOrganisationPermissions(data);
        },
        error: function (data) {
            //TODO: Errorhandling
        }
    });
}

// Hide all organisation permission checkboxes, then show assignable
function showOrganisationPermissions(data) {
    $('[class*=field-organisationform-permissions-]').hide();
    $('[class*=field-organisationform-permissions-]').find("[type='checkbox']").prop('checked', false);
    let parsed = JSON.parse(data);
    let arr = [];
    for (let x in parsed) {
        arr.push(parsed[x]);
    }
    arr.forEach(function (element) {
        $('.field-organisationform-permissions-'.concat(element)).show();
        $('.field-organisationform-permissions-'.concat(element)).find("[type='checkbox']").prop('checked', true);
    });
}

// Change assignable permissions for user by organisation
function getUserPermissionsByUserAndOrg(dropdown, unCheck) {
    if (dropdown.val() == "") {
        showUserPermissionCheckboxes(dropdown, "[]", true);
    } else {
        $.ajax({
            url: baseUrl + "admin/organisation/get-assignable-permissions-by-user-and-org",
            data: {
                orgId: dropdown.val()
            },
            success: function (data) {
                showUserPermissionCheckboxes(dropdown, data, unCheck);
            },
            error: function (data) {
                //TODO: Errorhandling
            }
        });
    }

}

// Hide all user permission checkboxes, then show assignable for this organisation
function showUserPermissionCheckboxes(dropdown, data, unCheck) {
    let checkboxes = dropdown.parent().parent().next().children().children();
    checkboxes.hide();
    if (unCheck) {
        checkboxes.find("[type='checkbox']").prop('checked', false);
    }
    let parsed = JSON.parse(data);
    let arr = [];
    for (let x in parsed) {
        arr.push(parsed[x]);
    }

    dropdown.parent().parent().next().find('[id*=permission-container-]').hide();

    arr.forEach(function (element) {
        dropdown.parent().parent().next().find('[class*=-permissions-'.concat(element).concat(']')).show();
        dropdown.parent().parent().next().find('[class*=-permissions-'.concat(element).concat(']')).parent().parent().parent().show();

        if (unCheck) {
            dropdown.parent().parent().next().find('[class*=-permissions-'.concat(element).concat(']')).find("[type='checkbox']").prop('checked', true);
        }
    });
}

//calls synch-all action to sync all users to outlook
function syncUsers() {
    $.ajax({
        async: true,
        url: baseUrl + "admin/user/synch-all",
        success: function () {
            showSuccessMessage('Erfolg', 'Nutzer wurden erfolgreich synchronisiert');
        },
        error: function () {
            showErrorMessage('Fehler', 'Nutzer konnten nicht synchronisiert werden');
        }
    });
}
