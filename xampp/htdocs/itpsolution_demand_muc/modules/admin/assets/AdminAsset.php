<?php

namespace app\modules\admin\assets;

use yii\web\AssetBundle;

class AdminAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/admin/assets';

    public $css = [
        'intraplan_site.css'
    ];

    public $js = [
        'admin.js',
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];

}