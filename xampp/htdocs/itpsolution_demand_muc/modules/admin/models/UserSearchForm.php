<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 13.02.2018
 * Time: 12:57
 */

namespace app\modules\admin\models;


use app\modules\core\models\Organisation;
use app\modules\core\models\User;
use Yii;
use yii\data\ActiveDataProvider;

class UserSearchForm extends User
{
    public $user_id;
    public $username;
    public $user_created;
    public $first_name;
    public $last_name;
    public $email;
    public $company;
    public $is_active;
    public $is_new;
    public $organisation_id;

    /**
     *
     * @inheridoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'user id'),
            'username' => Yii::t('app', 'username'),
            'user_created' => Yii::t('app', 'user created'),
            'first_name' => Yii::t('app', 'first name'),
            'last_name' => Yii::t('app', 'last name'),
            'email' => Yii::t('app', 'email'),
            'company' => Yii::t('app', 'company'),
            'is_active' => Yii::t('app', 'is active'),
            'is_new' => Yii::t('app', 'is new'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'user_id',
                'integer',
            ],
            [
                'username',
                'string'
            ],
            [
                'user_created',
                'integer'
            ],
            [
                'first_name',
                'string'
            ],
            [
                'last_name',
                'string'
            ],
            [
                'email',
                'string'
            ],
            [
                'company',
                'string'
            ]
        ];
    }

    /**
     * @param $params the search params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->user_id != null) {
            $query->andWhere(['user_id' => $this->user_id]);
        }

        if ($this->username != null) {
            $query->andWhere(['like', 'username', $this->username]);
        }

        if ($this->user_created != null) {
            $query->andWhere(['user_created' => $this->user_created]);
        }

        if ($this->first_name != null) {
            $query->andWhere(['like', 'first_name', $this->first_name]);
        }

        if ($this->last_name != null) {
            $query->andWhere(['like', 'last_name', $this->last_name]);
        }

        if ($this->email != null) {
            $query->andWhere(['like', 'email', $this->email]);
        }

        if ($this->company != null) {
            $query->andWhere(['like', 'company', $this->company]);
        }

        if ($this->organisation_id != null) {
            $query->leftJoin("user_organisation", "user.user_id = user_organisation.user_id");
            $query->andWhere(["organisation_id" => $this->organisation_id]);
            $repId = Organisation::find()->where(['organisation_id' => $this->organisation_id])->one()->getRepresentativeId();
            $query->orWhere(["user.user_id" => $repId]);
        }
        $query->groupBy('user_id');

        return $dataProvider;
    }
}