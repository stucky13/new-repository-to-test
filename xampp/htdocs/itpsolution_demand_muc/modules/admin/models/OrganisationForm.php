<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 09.02.2018
 * Time: 15:39
 */

namespace app\modules\admin\models;


use app\components\FormModel;
use app\modules\admin\Module;
use app\modules\core\models\AuthAssignment;
use app\modules\core\models\Organisation;
use app\modules\core\models\OrganisationAuthAssignment;
use app\modules\core\models\OrganisationGroup;
use app\modules\core\models\OrganisationType;
use app\modules\core\models\User;
use Exception;
use Throwable;
use Yii;
use yii\db\StaleObjectException;

/**
 * Class OrganisationForm
 *
 * @package app\modules\admin\models
 */
class OrganisationForm extends FormModel
{
    public $organisationId;
    public $organisationTypeId;
    public $title;
    public $shortTitle;
    public $street;
    public $houseNumber;
    public $zip;
    public $city;
    public $representativeId;
    public $permissions;

    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => $this->attributes(),
            static::SCENARIO_UPDATE => $this->attributes(),
        ];
    }


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                ['organisationId', 'organisationTypeId', 'title', 'shortTitle', 'street', 'houseNumber', 'zip', 'city'],
                'required',
            ],
            [
                'organisationTypeId',
                'exist',
                'targetClass' => OrganisationType::className(),
                'targetAttribute' => 'organisation_type_id'
            ],
            [
                'representativeId',
                'exist',
                'targetClass' => User::className(),
                'targetAttribute' => 'user_id'
            ],
            [
                ['street', 'city'],
                'string'
            ],
            [
                'zip',
                'integer'

            ],
            [
                //TODO: Validierung obs das Recht überhaupt gibt
                'permissions',
                'each',
                'rule' => [
                    'boolean'
                ]
            ],
            [
                'organisationId',
                'unique',
                'targetClass' => Organisation::className(),
                'targetAttribute' => 'organisation_id',
                'on' => static::SCENARIO_DEFAULT
            ]
        ];
    }

    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'organisationId' => Yii::t('app', 'organisation id'),
            'organisationTypeId' => Module::t('admin', 'organisation type'),
            'title' => Yii::t('app', 'title'),
            'shortTitle' => Yii::t('app', 'short title'),
            'street' => Yii::t('app', 'street'),
            'houseNumber' => Yii::t('app', 'house number'),
            'zip' => Yii::t('app', 'zip'),
            'city' => Yii::t('app', 'city'),
            'representativeId' => Module::t('admin', 'representative'),
            'permissions' => Yii::t('app', 'permissions')
        ];
    }

    /**
     * Creates a new organisation type, or updates an existing one.
     *
     * @return boolean true, if the organisation_type was saved successfully
     * @throws StaleObjectException
     * @throws Exception
     * @throws Throwable
     */
    public function save()
    {
        if ($this->getScenario() == static::SCENARIO_DEFAULT) {
            $organisation = new Organisation();
            $organisation->user_created = Yii::$app->user->getId();
            if ($this->organisationTypeId != null)
                $organisation->organisation_id = $this->organisationId;
        } else {
            $organisation = Organisation::findOne($this->id);
            $organisation->user_updated = Yii::$app->user->getId();
        }

        $organisation->organisation_id = $this->organisationId;
        $organisation->organisation_type_id = $this->organisationTypeId;
        $organisation->title = $this->title;
        $organisation->short_title = $this->shortTitle;
        $organisation->street = $this->street;
        $organisation->house_number = $this->houseNumber;
        $organisation->zip = $this->zip;
        $organisation->city = $this->city;
        $organisation->representative_id = $this->representativeId;

        if (!$organisation->save()
            || !$this->saveAuthAssignments($organisation->getId(), $this->permissions)) {
            return false;
        }

        return true;
    }

    /**
     * @param $id
     * @param String[] $permissions
     *
     * @returns true if successfully saved
     * @return bool
     * @throws StaleObjectException if [[optimisticLock|optimistic locking]] is enabled and the data
     * being deleted is outdated.
     * @throws Exception in case delete failed.
     * @throws Throwable in case delete failed.
     */
    private function saveAuthAssignments($id, $permissions)
    {
        /** @var Organisation $organisation */
        $organisation = Organisation::find()->where(['organisation_id' => $id])->one();

        $representativeId = $organisation->getRepresentativeId();

        if ($representativeId) {
            $repAssignments = AuthAssignment::find()->where(['user_id' => $representativeId, 'data' => $id])->all();
            foreach ($repAssignments as $repAssignment) {
                $repAssignment->delete();
            }
        }

        $assignments = OrganisationAuthAssignment::find()->where(['organisation_id' => $id])->all();
        foreach ($assignments as $assignment) {
            $assignment->delete();
        }

        if ($permissions) {
            foreach ($permissions as $title => $permission) {
                if ($permission) {
                    $assignment = new OrganisationAuthAssignment();
                    $assignment->organisation_id = $id;
                    $assignment->item_name = $title;
                    if (!$assignment->save()) {
                        return false;
                    }

                    if ($representativeId) {
                        $repAssignment = new AuthAssignment();
                        $repAssignment->user_id = $representativeId;
                        $repAssignment->data = $id;
                        $repAssignment->item_name = $title;
                        if (!$repAssignment->save()) {
                            return false;
                        }
                    }
                }
            }
            $userAssignments = AuthAssignment::find()->where(['data' => $id])->all();
            /** @var AuthAssignment $userAssignment */
            foreach ($userAssignments as $userAssignment) {
                if (!$permissions[$userAssignment->getItemName()])
                    $userAssignment->delete();
            }
        }

        return true;
    }
}