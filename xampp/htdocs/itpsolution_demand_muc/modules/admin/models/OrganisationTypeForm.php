<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 09.02.2018
 * Time: 11:45
 */

namespace app\modules\admin\models;


use app\components\FormModel;
use app\modules\core\models\AuthAssignment;
use app\modules\core\models\Organisation;
use app\modules\core\models\OrganisationAuthAssignment;
use app\modules\core\models\OrganisationType;
use app\modules\core\models\OrganisationTypeAuthAssignment;
use Exception;
use Throwable;
use Yii;
use yii\db\StaleObjectException;

/**
 * Class OrganisationTypeForm
 *
 * @package app\modules\admin\models
 */
class OrganisationTypeForm extends FormModel
{
    public $organisationTypeId;
    public $title;
    public $shortTitle;
    public $permissions;

    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => $this->attributes(),
            static::SCENARIO_UPDATE => $this->attributes(),
        ];
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                'organisationTypeId',
                'integer'
            ],
            [
                ['title', 'shortTitle'],
                'required',
            ],
            [
                //TODO: Validierung obs das Recht überhaupt gibt
                'permissions',
                'each',
                'rule' => [
                    'boolean'
                ]
            ],
            [
                'organisationTypeId',
                'unique',
                'targetClass' => OrganisationType::className(),
                'targetAttribute' => 'organisation_type_id',
                'on' => static::SCENARIO_DEFAULT
            ]
        ];
    }

    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'title' => Yii::t('app', 'title'),
            'shortTitle' => Yii::t('app', 'short title'),
            'permissions' => Yii::t('app', 'permissions'),
            'organisationTypeId' => Yii::t('app', 'organisation type id'),
        ];
    }

    /**
     * Creates a new organisation type, or updates an existing one.
     *
     * @return boolean true, if the organisation_type was saved successfully
     * @throws StaleObjectException
     * @throws Exception
     * @throws Throwable
     */
    public function save()
    {
        if ($this->getScenario() == static::SCENARIO_DEFAULT) {
            $organisationType = new OrganisationType();
            $organisationType->user_created = Yii::$app->user->getId();
            if ($this->organisationTypeId != null)
                $organisationType->organisation_type_id = $this->organisationTypeId;
        } else {
            $organisationType = OrganisationType::findOne($this->id);
            $organisationType->user_updated = Yii::$app->user->getId();
        }

        $organisationType->organisation_type_id = $this->organisationTypeId;
        $organisationType->title = $this->title;
        $organisationType->short_title = $this->shortTitle;

        if (!$organisationType->save() || !$this->saveAuthAssignments($organisationType->getId(), $this->permissions)) {
            return false;
        }

        return true;
    }

    /**
     * @param int $typeId the saved type id
     * @param String[] $permissions
     *
     * @returns true if successfully saved
     * @throws StaleObjectException if [[optimisticLock|optimistic locking]] is enabled and the data
     * being deleted is outdated.
     * @throws Exception|Throwable in case delete failed.
     */
    private function saveAuthAssignments($typeId, $permissions)
    {
        $assignments = OrganisationTypeAuthAssignment::find()->where(['organisation_type_id' => $typeId])->all();
        foreach ($assignments as $assignment) {
            $assignment->delete();
        }

        foreach ($permissions as $title => $permission) {
            if ($permission) {
                $assignment = new OrganisationTypeAuthAssignment();
                $assignment->organisation_type_id = $typeId;
                $assignment->item_name = $title;
                if (!$assignment->save()) {
                    return false;
                }
            }
        }

        $organisations = Organisation::find()->where(['organisation_type_id' => $typeId])->all();
        /** @var Organisation $organisation */
        foreach ($organisations as $organisation) {
            $organisationAssignments = OrganisationAuthAssignment::find()->where(['organisation_id' => $organisation->getId()])->all();
            /** @var OrganisationAuthAssignment $organisationAssignment */
            foreach ($organisationAssignments as $organisationAssignment) {
                if (!$permissions[$organisationAssignment->getItemName()])
                    $organisationAssignment->delete();
            }

            $userAssignments = AuthAssignment::find()->where(['data' => $organisation->getId()])->all();
            /** @var AuthAssignment $userAssignment */
            foreach ($userAssignments as $userAssignment) {
                if (!$permissions[$userAssignment->getItemName()])
                    $userAssignment->delete();
            }
        }

        return true;
    }
}