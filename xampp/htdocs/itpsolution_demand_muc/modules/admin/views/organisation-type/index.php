<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 09.02.2018
 * Time: 11:32
 */

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var int $page
 */

use app\components\Constants;
use app\modules\admin\Module;
use app\modules\core\models\Organisation;
use app\modules\core\models\OrganisationType;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\web\View;

$this->title = Module::t('admin', 'organisation type');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'class' => 'customTable text-color'
    ],
    'columns' => [
        'organisation_type_id' => [

            'attribute' => 'organisation_type_id',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'title' => [
            'attribute' => 'title',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'short_title' => [
            'attribute' => 'short_title',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        [
            'format' => 'raw',
            'attribute' => 'organisations',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (OrganisationType $model) {
                $links = [];
                /** @var Organisation $organisation */
                foreach ($model->getOrganisations()->all() as $organisation) {
                    $links[] = Html::a(Html::encode($organisation->getShortTitle()), ['/admin/organisation/view?id=' . $organisation->getId()]);
                }
                return (implode(', ', $links));
            }
        ],
        'is_deleted' => [
            'attribute' => 'is_deleted',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (OrganisationType $type) {
                return $type->is_deleted ? "Ja" : "Nein";
            }
        ],
        [
            'class' => ActionColumn::className(),
            'header' => 'Optionen',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'template' => '{view} {update} {delete} {reactivate}',
            'buttons' => [
                'view' => function ($url) {
                    return Html::a('', $url, ['class' => 'glyphicon glyphicon-eye-open', 'title' => Module::t('admin', 'open')]);
                },
                'update' => function ($url) use ($page) {
                    if (Yii::$app->getUser()->can(Constants::EDIT_ORGANISATION_TYPE)) {
                        return Html::a('', $url . "&page=$page", ['class' => 'glyphicon glyphicon-pencil', 'title' => Module::t('admin', 'edit')]);
                    }
                    return "";
                },
                'delete' => function ($url, $model) {
                    if (Yii::$app->getUser()->can(Constants::EDIT_ORGANISATION_TYPE) && ($model->is_deleted == 0)) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-trash', 'title' => Module::t('admin', 'delete'), 'onclick' => 'return confirm("' . Module::t('admin', 'delete organisation type') . '");']);
                    }
                    return "";
                },
                'reactivate' => function ($url, $model) {
                    if (Yii::$app->getUser()->can(Constants::ADMIN) && ($model->is_deleted == 1)) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-check', 'title' => Module::t('admin', 'reactivate'), 'onclick' => 'return confirm("' . Module::t('admin', 'reactivate organisation type') . '");']);
                    }
                    return "";
                }
            ]
        ]
    ],
]); ?>

<div class="clearfix">
    <?php if (Yii::$app->getUser()->can(Constants::EDIT_ORGANISATION_TYPE)): ?>
        <?= Html::a(Module::t('admin', 'create organisation type'), ["create?page=$page"], [
            'class' => 'btn mediumButton pull-right'
        ]); ?>
    <?php endif; ?>
    <?= Html::a(Module::t('admin', 'back'), Yii::$app->getHomeUrl(), ['class' => 'btn mediumButton pull-left']) ?>
</div>