<?php
/**
 * Created by PhpStorm.
 * User: Wenk
 * Date: 22.02.2018
 * Time: 14:06
 */

use app\components\Constants;
use app\components\SessionUtil;
use yii\widgets\ActiveForm;

?>

<?php
$form = ActiveForm::begin([
    'options' => [
        'class' => 'navbar-form navbar-right',
    ],
    'fieldConfig' => [
        'template' => "{input}",
    ]

]) ?>

<?php
if (Yii::$app->getUser()->can(Constants::ADMIN)) {
    $organisations = Yii::$app->getUser()->getIdentity()->getAllOrganisationArray();
} else {
    $organisations = Yii::$app->getUser()->getIdentity()->getAssignedOrganisationArray();
}
$model->organisation = SessionUtil::getSessionOrganisationId();
echo $form->field($model, 'organisation')->dropDownList($organisations, ["class" => 'form-control form-control-color']);
?>

<?php ActiveForm::end() ?>
