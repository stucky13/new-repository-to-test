<?php
/**
 * Created by PhpStorm.
 * User: Pascal Müller
 * Date: 18.01.2019
 * Time: 15:38
 */

use app\modules\account\Module;
use yii\db\ActiveQuery;
use yii\helpers\Url;

/**
 * @var ActiveQuery $org
 * @var ActiveQuery $surveyData
 */

$this->title = Module::t('account', 'survey data');
?>

<div id="viewContent">
    <div class="panel panel-default panel-color">
        <div class="panel-title panel-heading panel-default panel-color">
            <a data-toggle="collapse"
               href="#collapse-subnet-detail" class="collapsed">Aktuelle Auswahl: <?= $org->getTitle(); ?></a>
        </div>
        <div class="panel-body panel-color collapse" id="collapse-subnet-detail">
            <h3>Teilnetz <?= $org->getTitle() ?>:</h3>
            <br>
            <!-- TODO: wichtige Merkmale -->
            <h3>Verfügbare Erhebungsdaten:</h3>
            <ul>
                <?php
                foreach ($surveyData as $data): ?>
                    <li><a style="font-size: 14pt; color:white"
                           href="<?= Url::to(['/account/site/subnet-detail', 'id' => $data->getId()]) ?>">
                            VE <?= $data->getYear() ?>
                        </a></li>
                <?php endforeach; ?>
            </ul>
            <br>
            <h3>Besonderheiten:</h3>
            <ul>
                <!-- TODO: liste von Besonderheiten -->
            </ul>
            <br>
            <h3>Erhebungsmerkmale:</h3>
            <table class="table panel-color">
                <thead>
                <tr>
                    <th class="mediumButton gridview-header">Merkmal</th>
                    <?php foreach ($surveyData as $data): ?>
                        <th class="mediumButton gridview-header">VE <?= $data->getYear() ?></th>
                    <?php endforeach; ?>
                </tr>
                </thead>
                <tbody>
                <!-- TODO: Tablebody -->
                </tbody>
            </table>
        </div>
    </div>
</div>
