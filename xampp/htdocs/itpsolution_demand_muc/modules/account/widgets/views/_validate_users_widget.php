<?php

use app\modules\account\Module;
use app\modules\core\models\User;
use yii\db\ActiveQuery;
use yii\helpers\Url;

/**
 * @var $users ActiveQuery
 */

if ($users->count() == 0) : ?>
    <?= Module::t('account', 'no new users'); ?>
<?php else : ?>
    <table class="table  panel-color">
        <thead>
        <style>
            table {
                border-collapse: collapse;
            }

            table, td, th {
                border: 1px solid white;
            }
        </style>
        <tr>
            <th class="mediumButton gridview-header"> <?= Yii::t('app', 'username'); ?></th>
            <th class="mediumButton gridview-header"> <?= Yii::t('app', 'name'); ?></th>
            <th class="mediumButton gridview-header"> <?= Yii::t('app', 'email'); ?></th>
            <th class="mediumButton gridview-header"> <?= Yii::t('app', 'company'); ?></th>
            <th class="mediumButton gridview-header"> <?= Yii::t('app', 'creator'); ?></th>
            <th class="mediumButton gridview-header"> <?= Yii::t('app', 'last change'); ?></th>
            <th class="mediumButton gridview-header"> <?= Yii::t('app', 'renew/block'); ?></th>
        </tr>
        </thead>
        <tbody class="userTable">
        <?php
        /** @var User $user */
        foreach ($users->all() as $user) : ?>
            <tr>
                <td><a href="<?= Url::to(['/admin/user/view', 'id' => $user->getId()]) ?>">
                        <?= $user->getUsername(); ?>
                    </a></td>
                <td><?= $user->getPrettyName(); ?></td>
                <td><?= $user->getEmail(); ?></td>
                <td><?= $user->getCompany(); ?></td>
                <td><?= $user->getCreator() ? $user->getCreator()->getUsername() : null; ?></td>
                <td><?= Yii::$app->getFormatter()->asDatetime($user->getDtCreated()); ?></td>
                <td><a class="glyphicon glyphicon-ok"
                       href="<?= Url::to(['/admin/user/release', 'id' => $user->getId()]) ?>">
                    </a>
                    <a class="glyphicon glyphicon-remove"
                       href="<?= Url::to(['/admin/user/confirm', 'id' => $user->getId()]) ?>">
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
<?php endif; ?>
