<?php


use app\modules\account\Module;
use app\modules\core\models\User;
use yii\db\ActiveQuery;
use yii\helpers\Url;

/**
 * @var $users ActiveQuery
 */

if ($users->count() == 0) : ?>
    <?= Module::t('account', 'no new users'); ?>
<?php else : ?>

    <table class="table panel-color">
        <thead>
        <tr>
            <th class="mediumButton gridview-header">
                <div class="cut-text"><?= Module::t('account', 'username'); ?></div>
            </th>
            <th class="mediumButton gridview-header"><?= Module::t('account', 'name'); ?></th>
            <th class="mediumButton gridview-header"><?= Module::t('account', 'email'); ?></th>
            <th class="mediumButton gridview-header"><?= Module::t('account', 'company'); ?></th>
            <th class="mediumButton gridview-header"><?= Module::t('account', 'creator'); ?></th>
            <th class="mediumButton gridview-header"><?= Module::t('account', 'creation date'); ?></th>
            <th class="mediumButton gridview-header">
                <div class="cut-text"><?= Module::t('account', 'accept/deny'); ?></div>
            </th>
        </tr>
        </thead>
        <tbody class="userTable">
        <?php
        /** @var User $user */
        foreach ($users->all() as $user) : ?>
            <tr>
                <td><a href="<?= Url::to(['/admin/user/view', 'id' => $user->getId()]) ?>">
                        <?= $user->getUsername(); ?>
                    </a></td>
                <td><?= $user->getPrettyName(); ?></td>
                <td>
                    <div class="cut-text"><?= $user->getEmail(); ?></div>
                </td>
                <td>
                    <div class="cut-text"><?= $user->getCompany(); ?></div>
                </td>
                <td>
                    <div class="cut-text"><?= $user->getCreator() ? $user->getCreator()->getUsername() : null; ?></div>
                </td>
                <td><?= Yii::$app->getFormatter()->asDatetime($user->getDtCreated()); ?></td>
                <td><a class="glyphicon glyphicon-ok"
                       href="<?= Url::to(['/admin/user/release', 'id' => $user->getId()]) ?>">
                    </a>
                    <a class="glyphicon glyphicon-remove"
                       href="<?= Url::to(['/admin/user/reject', 'id' => $user->getId()]) ?>">
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php endif; ?>
