<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 14.02.2018
 * Time: 16:19
 */

use app\modules\account\Module;
use app\modules\account\widgets\DashboardPanel;
use yii\db\ActiveQuery;

/**
 * @var ActiveQuery $catalogs the catalogs that are to be shown
 * @var ActiveQuery $organisations
 */

?>

<?php DashboardPanel::begin(['title' => Module::t('account', 'master data')]) ?>
<div class="col-md-12">
    <?php
    echo $this->render("_navigation_type", ['title' => Yii::t('app', 'user'),
        'path' => '/admin/user/index']);

    echo $this->render("_navigation_type", ['title' => Yii::t('app', 'organisation'),
        'path' => '/admin/organisation/index']);

    echo $this->render("_navigation_type", ['title' => Yii::t('app', 'organisation type'),
        'path' => '/admin/organisation-type/index']);
    ?>
</div>
<div class="col-md-12">
    <?php
    echo $this->render("_navigation_type", ['title' => Yii::t('app', 'questions'),
        'path' => '/data/fragen/index'
    ]);
    ?>
</div>
<?php DashboardPanel::end(); ?>
<?php DashboardPanel::begin(['title' => Module::t('account', 'base data')]) ?>
<div class="col-md-12">
    <?php
    echo $this->render("_navigation_type", ['title' => Yii::t('app', 'knots'),
        'path' => '/data/knoten/index']);

    echo $this->render("_navigation_type", ['title' => Yii::t('app', 'routes'),
        'path' => '/data/linien/index']);

    echo $this->render("_navigation_type", ['title' => Yii::t('app', 'tickets'),
        'path' => '/data/fahrschein/index']);

    echo $this->render("_navigation_type", ['title' => Yii::t('app', 'verkehrsmittel'),
        'path' => '/data/verkehrsmittel/index']);
    ?>
</div>

<?php DashboardPanel::end(); ?>


<?php DashboardPanel::begin(['title' => Module::t('account', 'survey data')]) ?>
<div class="col-md-12">
    <?php
    echo $this->render("_navigation_type", ['title' => Yii::t('app', 'survey'),
        'path' => '/data/erhebungsdaten/index']);

    echo $this->render("_navigation_type", ['title' => Yii::t('app', 'surveyimport'),
        'path' => '/data/erhebungsdaten/upload']);
    ?>
</div>
<?php DashboardPanel::end(); ?>

<?php DashboardPanel::begin(['title' => 'Visualisierung']) ?>
<div class="col-md-12">
    <?php
    echo $this->render("_navigation_type", [
        'title' => 'Relationsansicht',
        'path' => '/visualisation/relation/index'
    ]);

    echo $this->render("_navigation_type", [
        'title' => 'Matrix',
        'path' => '/visualisation/matrix/index'
    ]);

    echo $this->render("_navigation_type", [
        'title' => 'Zähldaten',
        'path' => '/visualisation/zaehldaten/index'
    ]);
    ?>
</div>
<?php DashboardPanel::end(); ?>

<?php DashboardPanel::begin(['title' => 'Veränderungsanalyse']) ?>
    <div class="col-md-12">
        <?php
        echo $this->render("_navigation_type", [
            'title' => 'Berechnung',
            'path' => '/changeanalysis/berechnung/index'
        ]);
        ?>
    </div>
<?php DashboardPanel::end(); ?>