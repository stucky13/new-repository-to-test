<?php

namespace app\modules\account\controllers;

use app\components\BaseController;
use app\components\SessionUtil;
use app\modules\account\models\TeilnetzSessionForm;
use app\modules\core\models\Organisation;
use app\modules\core\models\User;
use Exception;
use Throwable;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;

/**
 * Class SiteController
 *
 * @package app\modules\account\controllers
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ]
                ]
            ]
        ];
    }

    /**
     * Displays the user dash boards.
     *
     * @return Response|string
     * @throws Exception
     * @throws Throwable
     */
    public function actionIndex()
    {
        $newUsers = User::find()->where(['is_new' => 1]);
        $sessionOrg = SessionUtil::getSessionOrganisation();

        if ($sessionOrg) {
            $oldUsers = $sessionOrg->getUsers()->where(['<', 'dt_updated', date("Y-m-d", strtotime(Yii::$app->params['account.userValidateTime']))]);
        }


        return $this->render('dashboard', [
            'newUsers' => $newUsers,
            'oldUsers' => isset($oldUsers) ? $oldUsers : null
        ]);
    }

    /**
     * Sets the teilnetz for the session
     */
    public function actionSetTeilnetzIdInSession()
    {
        $model = new TeilnetzSessionForm();
        if ($model->load(Yii::$app->request->post()))
            if ($model->validate()) {
                SessionUtil::setSessionTeilnetzId($model->teilnetz);
            } else {
                Yii::$app->response->statusCode = 400;
            }
    }

    /**
     * Displays the organisation dash boards.
     *
     * @return Response|string
     * @throws Exception
     * @throws Throwable
     */
    public function actionSubnetIndex()
    {
        /** @var User $user */
        $user = Yii::$app->getUser()->getIdentity();
        $orgs = $user->getAllOrganisations();

        return $this->renderPartial('_subnet_index', [
            'orgs' => $orgs,
        ]);
    }

    /**
     * Displays the survey data from the organisation.
     * @return Response|string
     * @throws Exception
     * @throws Throwable
     */
    public function actionDataIndex()
    {
        $org = SessionUtil::getSessionTeilnetz();
        if ($org) {
            $surveyData = $org->getErhebungsdaten()->all();
            return $this->renderPartial('_data_index', [
                'org' => $org,
                'surveyData' => $surveyData,
            ]);
        }
        return "";
    }

    /**
     * @param $id
     * @return string
     */
    public function actionSubnetDetail($id)
    {
        /** @var Organisation $org */
        $org = SessionUtil::getSessionTeilnetz();
        $surveyData = $org->getErhebungsdaten()->andWhere(['erhebungsdaten_id' => $id])->one();

        return $this->render('_subnet_detail', [
            'org' => $org,
            'surveyData' => $surveyData
        ]);
    }

}
