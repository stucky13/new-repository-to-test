<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 13.03.2018
 * Time: 11:16
 */

namespace app\modules\core\components;

use app\components\Constants;
use Yii;
use yii\db\ActiveQuery;

class UserQuery extends ActiveQuery
{

    public function init()
    {
        $this->andWhere(['`user`.`is_deleted`' => 0]);
        if (is_a(Yii::$app, 'yii\web\Application') && Yii::$app->getUser()->can(Constants::ADMIN)) {
            $this->orWhere(['`user`.`is_deleted`' => 1]);
        }
        parent::init();
    }
}