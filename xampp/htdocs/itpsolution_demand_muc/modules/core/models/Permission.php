<?php

namespace app\modules\core\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Permission
 * @package app\modules\core\models
 * @property int $permission_id;
 * @property string $permission;
 * @property bool $is_assignable;
 * @property int $permission_group_id;
 */
class Permission extends ActiveRecord
{
    /**
     * @return array|ActiveRecord[]
     */
    public static function getAssignablePermissions()
    {
        return Permission::find()->where(['is_assignable' => 1])->all();
    }

    /**
     * @return int
     */
    public function getPermissionGroupId()
    {
        return $this->permission_group_id;
    }

    /**
     * @return int
     */
    public function getPermissionId()
    {
        return $this->permission_id;
    }

    /**
     * @return string
     */
    public function getPermission()
    {
        return $this->permission;
    }

    /**
     * @return bool
     */
    public function isAssignable()
    {
        return $this->is_assignable;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->getTranslation()->getTitle();
    }

    /**
     * @param int $languageId
     * @return array|null|PermissionI18n
     */
    public function getTranslation($languageId = 1)
    {
        if ($languageId === null) {
            $languageId = 1;
        }
        return $this->getTranslations()->where(['language_id' => $languageId])->one();
    }

    /**
     * @return ActiveQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(PermissionI18n::className(), ['permission_id' => 'permission_id']);
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->getTranslation()->getDescription();
    }

    /**
     * @return array|ActiveRecord[]
     */
    public function getAllPermissions()
    {
        return $this->find()->all();
    }
}