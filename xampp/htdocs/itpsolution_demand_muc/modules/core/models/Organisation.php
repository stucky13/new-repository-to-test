<?php

namespace app\modules\core\models;


use app\components\AbstractActiveRecord;
use app\modules\core\components\OrganisationQuery;
use app\modules\data\models\Erhebungsdaten;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveQuery;

/**
 * Class Organisation
 * @package app\modules\core\models
 *
 * @property int $organisation_id
 * @property int $organisation_type_id
 * @property string $dt_created
 * @property string $dt_updated
 * @property int $user_created
 * @property int $user_updated
 * @property string $title
 * @property string $short_title
 * @property string $street
 * @property int $house_number
 * @property int $zip
 * @property string $city
 * @property int $representative_id
 * @property int $is_deleted
 */
class Organisation extends AbstractActiveRecord
{
    /**
     * @return OrganisationQuery|ActiveQuery
     */
    public static function find($useQuery = true)
    {
        if ($useQuery) {
            return new OrganisationQuery(get_called_class());
        } else {
            return parent::find();
        }

    }

    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'organisation_id' => Yii::t('app', 'organisation id'),
            'organisation_type_id' => Yii::t('app', 'organisation type id'),
            'user_created' => Yii::t('app', 'user created'),
            'dt_created' => Yii::t('app', 'dt created'),
            'user_updated' => Yii::t('app', 'user updated'),
            'dt_updated' => Yii::t('app', 'dt updated'),
            'title' => Yii::t('app', 'title'),
            'short_title' => Yii::t('app', 'short title'),
            'street' => Yii::t('app', 'street'),
            'house_number' => Yii::t('app', 'house number'),
            'zip' => Yii::t('app', 'zip'),
            'city' => Yii::t('app', 'city'),
            'representative_id' => Yii::t('app', 'representative id'),
            'is_deleted' => Yii::t('app', 'is_deleted')
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->organisation_id;
    }


    /**
     * @return int
     */
    public function getOrganisationTypeId()
    {
        return $this->organisation_type_id;
    }

    /**
     * @return string
     */
    public function getDtCreated()
    {
        return $this->dt_created;
    }

    /**
     * @return string
     */
    public function getDtUpdated()
    {
        return $this->dt_updated;
    }

    /**
     * @return int
     */
    public function getUserCreated()
    {
        return $this->user_created;
    }

    /**
     * @return int
     */
    public function getUserUpdated()
    {
        return $this->user_updated;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getShortTitle()
    {
        return $this->short_title;
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * @return int
     */
    public function getHouseNumber()
    {
        return $this->house_number;
    }

    /**
     * @return int
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @return int
     */
    public function getRepresentativeId()
    {
        return $this->representative_id;
    }

    /**
     * @return int
     */
    public function getIsDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @return User
     */
    public function getRepresentative()
    {
        return User::find()->where(['user_id' => $this->representative_id])->one();
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getUserArray()
    {
        /**
         * @var $users User[]
         */
        $users = $this->getUsers()->all();
        $userArray = [];
        foreach ($users as $user) {
            $userArray[$user->getId()] = $user->getUsername();
        }
        return $userArray;
    }

    /**
     * @return ActiveQuery
     * @throws InvalidConfigException
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['user_id' => 'user_id'])
            ->viaTable('user_organisation', ['organisation_id' => 'organisation_id']);
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    public function getUserIdsArray()
    {
        /**
         * @var $users User[]
         */
        $users = $this->getUsers()->all();
        $userArray = [];
        foreach ($users as $user) {
            $userArray[] = $user->getId();
        }
        return $userArray;
    }

    /**
     * @return string[]
     */
    public function getPermissionTitles()
    {
        $permissions = [];
        foreach ($this->getPermissions()->all() as $permission) {
            $permissions[] = $permission->getItemName();
        }
        return $permissions;
    }

    /**
     * @return ActiveQuery
     */
    public function getPermissions()
    {
        return $this->hasMany(OrganisationAuthAssignment::className(), ['organisation_id' => 'organisation_id']);
    }

    /**
     * @return OrganisationType
     */
    public function getOrganisationType()
    {
        return $this->hasOne(OrganisationType::className(), ['organisation_type_id' => 'organisation_type_id'])->one();
    }

    /**
     * @return ActiveQuery
     */
    public function getErhebungsdaten()
    {
        return $this->hasMany(Erhebungsdaten::className(), ['organisation_id' => 'organisation_id']);
    }

    /**
     * @param array $userIds Array of user ids to find email addr to
     * @return array email addrs
     */
    private function getEmails($userIds)
    {
        $emails = [];
        foreach ($userIds as $userId) {
            $user = User::findOne($userId);
            if ($user) {
                $emails[] = $user->getEmail();
            }
        }
        return $emails;
    }
}