<?php

namespace app\modules\core\models;

use app\components\AbstractActiveRecord;

/**
 * Class OrganisationTypeAuthAssignment
 * @package app\modules\core\models
 *
 * @property int $organisation_type_id
 * @property string $item_name
 *
 */
class OrganisationTypeAuthAssignment extends AbstractActiveRecord
{
    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->organisation_type_id;
    }

    /**
     * @return string
     */
    public function getItemName()
    {
        return $this->item_name;
    }
}