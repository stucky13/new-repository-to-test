<?php

namespace app\modules\core\models;

use yii\db\ActiveRecord;

/**
 * Class Language
 * @package app\modules\core\models
 * @property int $language_id
 * @property string $title
 * @property string $locale
 */
class Language extends ActiveRecord
{
    /**
     * @return int the language id
     */
    public function getLanguageId()
    {
        return $this->language_id;
    }

    /**
     * @return string the language title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string the language locale
     */
    public function getLocale()
    {
        return $this->locale;
    }

}