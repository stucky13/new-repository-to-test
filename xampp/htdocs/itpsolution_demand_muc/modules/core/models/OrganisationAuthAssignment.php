<?php

namespace app\modules\core\models;

use app\components\AbstractActiveRecord;
use yii\db\ActiveQuery;

/**
 * Class OrganisationAuthAssignment
 * @package app\modules\core\models
 *
 * @property int $organisation_id
 * @property string $item_name
 *
 */
class OrganisationAuthAssignment extends AbstractActiveRecord
{
    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->organisation_id;
    }

    /**
     * @return string
     */
    public function getItemName()
    {
        return $this->item_name;
    }

    /**
     * @return ActiveQuery
     */
    public function getPermission()
    {
        return $this->hasOne(Permission::className(), ['permission' => 'item_name']);
    }
}