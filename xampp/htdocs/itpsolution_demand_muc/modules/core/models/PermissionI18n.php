<?php

namespace app\modules\core\models;

use yii\db\ActiveRecord;

/**
 * Class PermissionI18n
 * @package app\modules\core\models
 * @property int $permission_id;
 * @property string $title;
 * @property string $description;
 */
class PermissionI18n extends ActiveRecord
{
    /**
     * @return int
     */
    public function getPermissionId()
    {
        return $this->permission_id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

}