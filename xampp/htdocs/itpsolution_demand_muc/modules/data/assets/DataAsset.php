<?php
/**
 * Created by PhpStorm.
 * User: Pascal Müller
 * Date: 18.01.2019
 * Time: 11:51
 */

namespace app\modules\data\assets;

use yii\web\AssetBundle;

class DataAsset extends AssetBundle
{
    public $sourcePath = '@app/modules/data/assets';

    public $css = [
    ];

    public $js = [
        'fragen.js'
    ];

    public $depends = [
        'yii\web\JqueryAsset',
    ];

}