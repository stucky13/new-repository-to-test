/**
 * Created by Tobi on 28.02.2019.
 */

$(document).ready(function () {
    // Change assignable referenceFields
    $('#fragenform-reference').change(function () {
        getAndSetReferenceFields($(this).val());
    });
});

// Change reference fields
function getAndSetReferenceFields(reference) {
    $.getJSON(baseUrl + "data/fragen/get-reference-fields?reference=" + reference, function (data) {
        $("#fragenform-field option").remove();
        console.log(data);
        $.each(data, function (key, value) {
            $("#fragenform-field").append('<option value="' + key + '">' + value + '</option>')
        })
    });
}