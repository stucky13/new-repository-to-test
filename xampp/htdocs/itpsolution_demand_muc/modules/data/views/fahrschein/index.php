<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 15.01.2019
 * Time: 14:11
 */

/**
 * @var ActiveDataProvider $dataProvider
 * @var int $page
 */

use app\modules\data\assets\DataAsset;
use app\modules\data\models\Fahrschein;
use app\modules\data\Module;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;
use yii\helpers\Html;

DataAsset::register($this);
$this->title = Module::t('data', 'ticket');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'class' => 'customTable text-color'
    ],
    'columns' => [
        'id' => [
            'attribute' => 'id',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'name' => [
            'attribute' => 'name',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'kategorie1' => [
            'attribute' => 'kategorie1',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fahrschein $ticket) {
                return $ticket->getCategory(1)->one()->getTitle();
            }
        ],
        'kategorie2' => [
            'attribute' => 'kategorie2',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fahrschein $ticket) {
                return $ticket->getCategory(2)->one()->getTitle();
            }
        ],
        'kategorie3' => [
            'attribute' => 'kategorie3',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fahrschein $ticket) {
                return $ticket->getCategory(3)->one()->getTitle();
            }
        ],
        'kategorie4' => [
            'attribute' => 'kategorie4',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fahrschein $ticket) {
                return $ticket->getCategory(4)->one()->getTitle();
            }
        ]
    ],
]); ?>
<div class="clearfix">
    <?= Html::a(Yii::t('app', 'back'), Yii::$app->getHomeUrl(), ['class' => 'btn mediumButton pull-left']) ?>

    <?= Html::a(Yii::t('app', 'tickets import'), 'upload', ['class' => 'btn mediumButton pull-right']) ?>
</div>