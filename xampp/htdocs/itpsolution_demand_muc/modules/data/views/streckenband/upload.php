<?php

/**
 * @var yii\web\View $this
 * @var app\modules\data\models\LinienCsvUpload
 * @var int $page
 */

use app\modules\data\assets\DataAsset;
use app\modules\data\Module;
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;

$this->title = Module::t('data', 'import tickets');
DataAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'options' => ['multipart/form-data'],
]); ?>

    <div class="col-lg-4"></div>
    <div class="col-lg-4">
        <div class="panel panel-default panel-color" style="display: block">
            <div class="panel-body">
                <?= $form->field($model, 'csvFile')->fileInput(['accept' => '.csv']) ?>
            </div>
        </div>

        <?= Html::submitButton(Module::t('data', 'import'), ['class' => 'btn mediumButton pull-right']) ?>
        <?= Html::a(Yii::t('app', 'abort'), ['index', 'page' => $page], ['class' => 'btn mediumButton pull-left']); ?>
    </div>
    <div class="col-lg-4"></div>

<?php ActiveForm::end(); ?>