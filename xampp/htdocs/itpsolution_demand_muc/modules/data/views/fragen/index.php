<?php

use app\modules\data\models\Fragen;
use app\modules\data\Module;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Module::t('questionnaire', 'index title');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'class' => 'customTable text-color'
    ],
    'columns' => [
        'frage_id' => [
            'attribute' => 'frage_id',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'fragestellung' => [
            'attribute' => 'fragestellung',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'kurztext' => [
            'attribute' => 'kurztext',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'antworttyp' => [
            'attribute' => 'antworttyp',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fragen $question) {
                return $question->adjustQuestionType();
            }
        ],
        'fragenEnums' => [
            'attribute' => 'fragenEnums',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fragen $question) {
                return $question->getFragenEnumsAntworten();
            }
        ],
        'wertebereich_min' => [
            'attribute' => 'wertebereich_min',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'wertebereich_max' => [
            'attribute' => 'wertebereich_max',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'nachkommastellen' => [
            'attribute' => 'nachkommastellen',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'reference' => [
            'attribute' => 'reference',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fragen $question) {
                return $question->adjustReference();
            }
        ],
        'field' => [
            'attribute' => 'field',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fragen $question) {
                return $question->adjustReferenceField();
            }
        ],
        'category_id' => [
            'attribute' => 'category_id',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fragen $question) {
                return $question->getCategory()->one() ? $question->getCategory()->one()->getTitle() : '';
            }
        ],
        'is_georef' => [
            'attribute' => 'is_georef',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fragen $question) {
                return $question->isGeoref() ? 'Ja' : 'Nein';
            }
        ],
        'is_filter' => [
            'attribute' => 'is_filter',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fragen $question) {
                return $question->isFilter() ? 'Ja' : 'Nein';
            }
        ],
        'is_value' => [
            'attribute' => 'is_value',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fragen $question) {
                return $question->isValue() ? 'Ja' : 'Nein';
            }
        ],
        'value_type' => [
            'attribute' => 'value_type',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'value' => function (Fragen $question) {
                return $question->getValueType()->one() ? $question->getValueType()->one()->getName() : '';
            }
        ],
        [
            'class' => ActionColumn::className(),
            'header' => 'Optionen',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'template' => '{view} {update} {delete}',
            'buttons' => [
                'view' => function ($url, $model) use ($page) {
                    return Html::a('', $url . "&page=$page", ['class' => 'glyphicon glyphicon-eye-open', 'title' => Module::t('questionnaire', 'view')]);
                },
                'update' => function ($url, $model) use ($page) {
                    return Html::a('', $url . "&page=$page", ['class' => 'glyphicon glyphicon-pencil', 'title' => Module::t('questionnaire', 'update')]);
                },
                'delete' => function ($url, $model) use ($page) {
                    return Html::a('', $url . "&page=$page", ['class' => 'glyphicon glyphicon-trash', 'title' => Module::t('questionnaire', 'delete'), 'onclick' => 'return confirm("' . Module::t('admin', 'delete user') . '");']);
                }
            ]
        ]
    ],
]); ?>

<div class="clearfix">
    <?= Html::a(Module::t('questionnaire', 'create question'), ["create", 'page' => $page], [
        'class' => 'btn mediumButton pull-right'
    ]); ?>
    <?= Html::a(Module::t('questionnaire', 'back'), Yii::$app->getHomeUrl(), ['class' => 'btn mediumButton pull-left']) ?>
</div>