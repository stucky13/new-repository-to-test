<?php

use app\components\FormModel;
use app\modules\data\assets\DataAsset;
use app\modules\data\models\FragenEnumForm;
use app\modules\data\Module;
use app\modules\data\widgets\CategorySelectWidget;
use wbraganca\dynamicform\DynamicFormWidget;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var $this yii\web\View
 * @var $model app\modules\data\models\FragenForm
 * @var array $assignableTypes []
 * @var FragenEnumForm[] $questionEnums
 * @var $form yii\widgets\ActiveForm
 * @var array $referenceTables []
 * @var array $referenceFields []
 * @var array $ticketCategories []
 * @var array $valueTypes []
 * @var int $page
 * */

$this->title = $model->getScenario() == FormModel::SCENARIO_DEFAULT ? Module::t('questionnaire', 'create question') : Module::t('questionnaire', 'update question');

DataAsset::register($this);
?>

<?php $form = ActiveForm::begin([
    'id' => 'question-form',
    'options' => ['class' => 'form-vertical']
]); ?>

<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default panel-color">
            <div class="panel-body panel-color">

                <?= $form->field($model, 'fragestellung')->textInput(['maxlength' => 128]) ?>

                <?= $form->field($model, 'frage_id')->hiddenInput(['maxlength' => true])->label(false) ?>

                <?= $form->field($model, 'kurztext')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'antworttyp')->dropdownList($assignableTypes,
                    ['prompt' => Module::t('questionnaire', 'select question type'),
                        'onchange' => "
                               $('#wertebereich_min').toggle(this.value == 'integer'); 
                               $('#wertebereich_max').toggle(this.value == 'integer'); 
                               $('#nachkommastellen').toggle(this.value == 'decimal');
                               $('#fragen_wrapper').toggle(this.value == 'user_defined');
                               $('#reference_wrapper').toggle(this.value == 'reference');
                         "]) ?>

                <div class="col-md-12">
                    <?= $form->field($model, 'wertebereich_min', [
                        'options' => [
                            'id' => 'wertebereich_min',
                            'style' => $model->antworttyp == 'integer' ? 'display:block' : 'display:none'
                        ]])->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'wertebereich_max', [
                        'options' => [
                            'id' => 'wertebereich_max',
                            'style' => $model->antworttyp == 'integer' ? 'display:block' : 'display:none'
                        ]])->textInput(['maxlength' => true]) ?>



                    <?= $form->field($model, 'nachkommastellen', [
                        'options' => [
                            'id' => 'nachkommastellen',
                            'style' => $model->antworttyp == 'decimal' ? 'display:block' : 'display:none'
                        ]])->textInput(['maxlength' => true]) ?>



                    <?php DynamicFormWidget::begin([
                        'widgetContainer' => 'fragen_wrapper',
                        'widgetBody' => '.question-enum-body',
                        'widgetItem' => '.question-enum-item',
                        'insertButton' => '.add-item',
                        'deleteButton' => '.delete-item',
                        'model' => $questionEnums[0],
                        'formId' => 'question-form',
                        'formFields' => [
                            'value',
                            'enum_id',
                            'frage_id'
                        ],
                    ]); ?>

                    <table id="fragen_wrapper"
                           style="display:<?= $model->antworttyp == 'user_defined' ? 'block' : 'none' ?>"
                           class="table table-bordered   margin-b-none panel-color tableColorwhite ">
                        <thead>
                        <tr>
                            <th style="width:90%"><?= Module::t('questionnaire', 'question enum value') ?></th>
                            <th style="width:10%"><?= Module::t('questionnaire', 'question enum options') ?></th>
                        </tr>
                        </thead>
                        <tbody class="question-enum-body tableColorwhite">
                        <?php
                        /** @var FragenEnumForm $questionEnum */
                        foreach ($questionEnums as $index => $questionEnum): ?>
                            <tr class="question-enum-item tableColorwhite">
                                <td>
                                    <?= $form->field($questionEnum, "[{$index}]frage_id")->hiddenInput(['maxlength' => true])->label(false) ?>
                                    <?= $form->field($questionEnum, "[{$index}]enum_id")->hiddenInput(['maxlength' => true])->label(false) ?>
                                    <?= $form->field($questionEnum, "[{$index}]value")->textInput(['maxlength' => true])->label(false) ?>
                                </td>
                                <td style="vertical-align: middle;" class="text-center">
                                    <button type="button" class="delete-item btn btn-danger btn-xs"><i
                                                class="fa fa-minus"></i>
                                    </button>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="2" class="text-right vcenter">
                                <button type="button" class="add-item btn mediumButton btn-sm"><span
                                            class="fa fa-plus"></span>
                                    Neu
                                </button>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                    <?php DynamicFormWidget::end(); ?>

                    <div id="reference_wrapper"
                         style="display:<?= $model->antworttyp == 'reference' ? 'block' : 'none' ?>">
                        <?= $form->field($model, 'reference')->dropdownList($referenceTables,
                            ['prompt' => Module::t('questionnaire', 'select reference'),
                                'onchange' => "
                               $('#category_picker').toggle(this.value == 'fahrschein');
                               if (this.value) {
                                  $('#field_picker').show();
                               } else {
                                  $('#field_picker').hide();
                               }                        
                         "]) ?>
                        <div id="field_picker" style="display:<?= $model->reference ? 'block' : 'none' ?>">
                            <?= $form->field($model, 'field')->dropdownList($referenceFields,
                                ['prompt' => Module::t('questionnaire', 'select field')]) ?>
                        </div>

                        <div id="category_picker"
                             style="display:<?= $model->reference == 'fahrschein' ? 'block' : 'none' ?>">
                            <?= CategorySelectWidget::widget([
                                'form' => $form,
                                'model' => $model,
                                'attribute' => 'category_id',
                                'updateRoute' => ["/data/category/query"]
                            ]); ?>
                        </div>
                    </div>
                </div>
                <?= $form->field($model, 'is_filter')->checkbox() ?>

                <?= $form->field($model, 'is_georef')->checkbox() ?>

                <?= $form->field($model, 'is_value')->checkbox([
                    'onchange' => "
                               $('#value_type').toggle(); 
                         "]) ?>

                <?= $form->field($model, 'value_type', [
                    'options' => [
                        'id' => 'value_type',
                        'style' => $model->is_value ? 'display:block' : 'display:none'
                    ]])->dropDownList($valueTypes, ['prompt' => Module::t('questionnaire', 'select value type')]) ?>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <?= Html::a(Module::t('questionnaire', 'abort'), ['index', 'page' => $page], ['class' => 'btn mediumButton pull-left']) ?>

        <?= Html::submitButton(Module::t('questionnaire', 'save'), ['class' => 'btn mediumButton pull-right', 'name' => 'page', 'value' => $page]) ?>
    </div>
</div>
<?php ActiveForm::end(); ?>
