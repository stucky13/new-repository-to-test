<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 15.01.2019
 * Time: 14:11
 */

/**
 * @var ActiveDataProvider $dataProvider
 * @var int $page
 */

use app\components\Constants;
use app\modules\data\assets\DataAsset;
use app\modules\data\Module;
use yii\data\ActiveDataProvider;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;

DataAsset::register($this);
$this->title = Module::t('data', 'route');
?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'options' => [
        'class' => 'customTable text-color'
    ],
    'columns' => [
        'linien_id' => [
            'attribute' => 'linien_id',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        'linien_name' => [
            'attribute' => 'linien_name',
            'headerOptions' => ['class' => 'mediumButton gridview-header']
        ],
        [
            'class' => ActionColumn::className(),
            'header' => 'Optionen',
            'headerOptions' => ['class' => 'mediumButton gridview-header'],
            'template' => '{view}',
            'buttons' => [
                'delete' => function ($url) {
                    if (Yii::$app->getUser()->can(Constants::EDIT_ROUTES)) {
                        return Html::a('', $url, ['class' => 'glyphicon glyphicon-trash', 'title' => Yii::t('app', 'delete'), 'onclick' => 'return confirm("' . Module::t('data', 'delete route') . '");']);
                    }
                }
            ]
        ]
    ],
]); ?>
<div class="clearfix">
    <?= Html::a(Yii::t('app', 'back'), Yii::$app->getHomeUrl(), ['class' => 'btn mediumButton pull-left']) ?>

    <?= Html::a(Yii::t('app', 'routes import'), 'upload', ['class' => 'btn mediumButton pull-right']) ?>
</div>