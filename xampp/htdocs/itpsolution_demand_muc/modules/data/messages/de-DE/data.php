<?php
/**
 * Created by PhpStorm.
 * User: Pascal Müller
 * Date: 18.01.2019
 * Time: 13:02
 */

return [
    "import" => "Importieren",
    "delete" => "Löschen",
    "name" => "Name",

    "ticket" => "Fahrschein",
    "delete ticket" => "Fahrschein löschen",
    "ticket %s successfully deleted" => "Fahrschein %s konnte erfolgreich gelöscht werden",
    "ticket %s couldn't be deleted" => "Fahrschein %s konnte nicht gelöscht werden",
    "Failed to find ticket with id " => "Fahrschein konnte nicht gefunden werden. Id: ",
    "import tickets" => "Fahrscheine importieren",
    "id" => "Fahrscheinnummer",
    "category 1" => "Kategorie 1",
    "category 2" => "Kategorie 2",
    "category 3" => "Kategorie 3",
    "category 4" => "Kategorie 4",
    "ticket file id" => "Fahrscheindatei Id",

    "knot" => "Knoten",
    "delete knot" => "Knoten löschen",
    "knot %s successfully deleted" => "Knoten %s konnte erfolgreich gelöscht werden",
    "knot %s couldn't be deleted" => "Knoten %s konnte nicht gelöscht werden",
    "Failed to find knot with nr " => "Knoten konnte nicht gefunden werden. Nr: ",
    "import knots" => "Knoten importieren",
    "nr" => "Knoten Id",
    "longitude" => "Laenge",
    "latitude" => "Breite",
    "knot file id" => "Knotendatei Id",

    "route" => "Linien",
    "delete route" => "Linie löschen",
    "route %s successfully deleted" => "Linie %s konnte erfolgreich gelöscht werden",
    "route %s couldn't be deleted" => "Linie %s konnte nicht gelöscht werden",
    "Failed to find route with Id " => "Linie konnte nicht gefunden werden. Id: ",
    "import routes" => "Linien importieren",
    "route id" => "Linien Id",
    "route name" => "Linienname",
    "route file id" => "Liniendatei Id",
    "knot name" => "Knotenname",
    "hst sort" => "Haltestellensortierung",

    "import raised data" => "Befragungsdaten importieren",
    "raised data" => "Befragungsdaten",

    //Verkehrsmittel
    'verlehrsmittel' => 'Verkehrsmittel',
    'create verkehrsmittel' => 'Verkehrsmittel anlegen',
    'delete verkehrsmittel' => 'Verkehrsmittel löschen',
    'verkehrsmittel successfully created' => 'Verkehrsmittel erfolgreich angelegt',
    'verkehrsmittel %s successfully updated' => 'Verkehrsmittel %s erfolgreich aktualisiert',
    'verkehrsmittel %s successfully deleted' => 'Verkehrsmittel %s erfolgreich gelöscht',
    'verkehrsmittel %s couldn\'t be deleted' => 'Verkehrsmittel %s konnte nicht gelöscht werden'

];