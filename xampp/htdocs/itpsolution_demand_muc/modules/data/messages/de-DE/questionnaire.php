<?php
/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 15.02.2019
 * Time: 10:27
 */

return [
    /** fragen */
    'index title' => 'Fragen',
    'subpage title' => 'Frage',
    'create question' => 'Frage anlegen',
    'update question' => 'Frage bearbeiten',
    'question id' => 'ID',
    'question' => 'Fragestellung',
    'short text' => 'Kurztext',
    'type of answer' => 'Antworttyp',
    'range min' => 'Wertebereich Min',
    'range max' => 'Wertebereich Max',
    'number of decimal places' => 'Nachkommastellen',
    'Invalid question id ' => 'Ungültige Fragen-ID ',
    'question successfully created' => 'Fragen konnte erfolgreich angelegt werden',
    'question successfully updated' => 'Fragen konnte erfolgreich aktualisiert werden',
    'question successfully deleted' => 'Fragen erfolgreich gelöscht',
    "question couldn't be deleted" => 'Fragen konnte nicht gelöscht werden',
    "select question type" => 'Antworttypen auswählen',
    "user defined title" => "Benutzerdefinierte Antworten",
    "question enum value" => "Wert",
    "question enum options" => "Entfernen",
    'reference' => 'Referenz',
    'field' => 'Referenzfeld',
    'category_id' => 'Kategorie',
    'select reference' => 'Referenz auswählen',
    'select field' => 'Referenzwert auswählen',
    'select value type' => 'Berechnungstyp auswählen',
    'is filter' => 'Ist filterbar?',
    'is georef' => 'Ist Geolokation?',
    'is value' => 'Ist Rechenwert?',
    'value type' => 'Berechnungstyp',

    /** antworttyp */
    'integer' => 'Integer',
    'decimal' => 'Decimal',
    'user defined' => 'User defined',


    /** allgemein */
    'back' => 'Zurück',
    'abort' => 'Abbrechen',
    'save' => 'Speichern',
    'update' => 'Bearbeiten',
    'view' => 'Detailansicht',
    'delete' => 'Löschen'
];