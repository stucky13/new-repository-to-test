<?php
/**
 * Created by PhpStorm.
 * User: Tom
 * Date: 23.08.2018
 * Time: 15:45
 */


return [
    //EA PERIOD
    'ea period created' => 'EA Periode wurde erstellt',
    'could not save ea period' => 'EA Periode konnte nicht erstellt werden',
    'ea period updated' => 'EA Periode wurde aktualisiert',
    'could not update ea period' => 'EA Periode konnte nicht aktualisert werden',
    'organisation mismatch' => 'Organisation stimmt nicht überein',
    'file does not exist' => 'Datei existiert nicht auf dem Server',
    'ea splitted' => 'EA Periode wurde aufgeteilt',
    'could not split ea' => 'EA Periode konnte nicht geteilt werden',
    'file uploaded' => 'Datei wurde hochgeladen',
    'could not save file' => 'Datei konnte nicht gespeichert werden',
    'could not upload file' => 'Datei konnte nicht hochgeladen werden',
    'organisation mismatch or file does not exist' => 'Organisation stimmt nicht überein oder Datei existiert nicht auf dem Server',
    'update saved' => 'Aktualisiert',
    'update failed' => 'Aktualisierung fehlgeschlagen',
    'no errors' => 'Keine Fehler',
    'could not find file' => 'Datei konnte nicht gefunden werden',
    'feedback sent' => 'Rückmeldung gesendet',
    'cannot switch from %s' => 'Statusänderung von %s nicht möglich',
    'cannot switch to %s' => 'Statusänderung zu %s nicht möglich',
    'organisation with id %d does not exist' => 'Organisation mit Id %d existiert nicht',
    'cannot change organisations while ea status is %s' => 'Änderungen von Organisationen während EA Status %s nicht möglich',
    'dt from before dt to' => 'Bis Datum liegt vor Von Datum',
    'dt email in the past' => 'Email wird in der Vergangenheit gesendet',
    'previous ea period does not exist' => 'Vorgänger EA Periode existiert nicht',
    'previous ea has next already' => 'Vorgänger EA Periode hat bereits einen Nachfolger',
    'invalid overlapping' => 'Ungültige Überlappung',
    'next ea period does not exist' => 'Nachfolger EA Periode existiert nicht',
    'next ea has prev already' => 'Nachfolger EA Periode hat bereits einen Vorgänger',
    'ea period not open' => 'EA Periode noch nicht geöffnet',
    'organisation mismatch or ea not open' => 'Organisation stimmt nicht überein oder Ea Periode nicht geöffnet',
    'authentication error: partner id mismatch' => 'Autorisierungsfehler: Die Vertragspartner-ID in den Verkaufsdaten entspricht nicht der Organisations-ID des angemeldeten Benutzers.',
    'dates exceed one month' => 'Bis und Vor Datum müssen in dem selben Monat liegen',
    'calendar month already existing' => 'Kalendermonat existiert bereits'
];