<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 15.01.2019
 * Time: 14:09
 */

namespace app\modules\data\models;

use app\modules\data\Module;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class Fahrschein
 * @package app\modules\data\models
 * @property int $id
 * @property string $name
 * @property int $kategorie1
 * @property int $kategorie2
 * @property int $kategorie3
 * @property int $kategorie4
 * @property int $fahrschein_file_id
 */
class Fahrschein extends ActiveRecord
{
    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('data', 'id'),
            'name' => Module::t('data', 'name'),
            'kategorie1' => Module::t('data', 'category 1'),
            'kategorie2' => Module::t('data', 'category 2'),
            'kategorie3' => Module::t('data', 'category 3'),
            'kategorie4' => Module::t('data', 'category 4'),
            'fahrschein_file_id' => Module::t('data', 'ticket file id')
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getFahrscheinFileId()
    {
        return $this->fahrschein_file_id;
    }

    /**
     * @return int
     */
    public function getKategorie1()
    {
        return $this->kategorie1;
    }

    /**
     * @return int
     */
    public function getKategorie2()
    {
        return $this->kategorie2;
    }

    /**
     * @return int
     */
    public function getKategorie3()
    {
        return $this->kategorie3;
    }

    /**
     * @return int
     */
    public function getKategorie4()
    {
        return $this->kategorie4;
    }

    /**
     * Gets the Category for the given type.
     *
     * @param int $type the category type
     * @return ActiveQuery
     */
    public function getCategory($type)
    {
        return $this->hasOne(Category::className(), ['category_id' => 'kategorie' . $type]);
    }
}