<?php

namespace app\modules\data\models;

use app\components\FormModel;

/**
 * Class LinienKnotenForm
 * @package app\modules\data\models
 */
class LinienKnotenForm extends FormModel
{
    public $linien_id;
    public $nr;
    public $hst_sort;

    public function rules()
    {
        return [
            [
                ['linien_id', 'nr', 'hst_sort'],
                'required'
            ],
        ];
    }

    public function save()
    {
        $routeKnot = new LinienKnoten();

        $routeKnot->linien_id = $this->linien_id;
        $routeKnot->nr = $this->nr;
        $routeKnot->hst_sort = $this->hst_sort;

        $routeKnot->save();
    }
}