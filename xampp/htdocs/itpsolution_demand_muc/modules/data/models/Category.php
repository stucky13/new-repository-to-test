<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 15.01.2019
 * Time: 14:20
 */

namespace app\modules\data\models;


use yii\db\ActiveRecord;

/**
 * Class Category
 * @package app\modules\data\models
 * @property int $category_id
 * @property string $title
 */
class Category extends ActiveRecord
{
    /**
     * @return int
     */
    public function getCategoryId()
    {
        return $this->category_id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}