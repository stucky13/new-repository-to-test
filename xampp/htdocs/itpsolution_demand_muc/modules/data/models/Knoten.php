<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 15.01.2019
 * Time: 15:12
 */

namespace app\modules\data\models;

use app\modules\data\Module;
use yii\db\ActiveRecord;

/**
 * Class Knoten
 * @package app\modules\data\models
 *
 * @property int $nr
 * @property string $name
 * @property double $laenge
 * @property double $breite
 * @property int $knoten_file_id
 */
class Knoten extends ActiveRecord
{
    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'nr' => Module::t('data', 'nr'),
            'name' => Module::t('data', 'name'),
            'laenge' => Module::t('data', 'longitude'),
            'breite' => Module::t('data', 'latitude'),
            'knoten_file_id' => Module::t('data', 'knot file id')
        ];
    }

    /**
     * @return int
     */
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * @return int
     */
    public function getKnotenFileId()
    {
        return $this->knoten_file_id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getLaenge()
    {
        return $this->laenge;
    }

    /**
     * @return float
     */
    public function getBreite()
    {
        return $this->breite;
    }

}