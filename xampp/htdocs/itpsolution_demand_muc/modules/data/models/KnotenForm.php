<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 16.01.2019
 * Time: 14:28
 */

namespace app\modules\data\models;


use app\components\FormModel;

/**
 * Class KnotenForm
 * @package app\modules\data\models
 */
class KnotenForm extends FormModel
{
    public $nr;
    public $name;
    public $laenge;
    public $breite;
    public $knoten_file_id;

    public function rules()
    {
        return [
            [
                ['nr', 'name', 'laenge', 'breite', 'knoten_file_id'],
                'required'
            ],
        ];
    }

    public function save()
    {
        $knoten = Knoten::findOne($this->nr);
        if (!$knoten) {
            $knoten = new Knoten();
        }

        $knoten->nr = $this->nr;
        $knoten->name = $this->name;
        $knoten->laenge = is_numeric(str_replace(",", ".", $this->laenge)) ? str_replace(",", ".", $this->laenge) : null;
        $knoten->breite = is_numeric(str_replace(",", ".", $this->breite)) ? str_replace(",", ".", $this->breite) : null;
        $knoten->knoten_file_id = $this->knoten_file_id;

        return $knoten->save();
    }
}