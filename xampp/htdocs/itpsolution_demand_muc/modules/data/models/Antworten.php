<?php

namespace app\modules\questionnaire\models;

/**
 * Created by PhpStorm.
 * User: Isabella Neufeld
 * Date: 15.02.2019
 * Time: 10:40
 */

namespace app\modules\data\models;

use yii\db\ActiveRecord;

/**
 * Class Antworten
 * @package app\modules\data\models
 * @property int $antwort_id
 * @property int $interview_id
 * @property int $frage_id
 * @property int $fragebogen_id
 * @property string $value
 * @property int $antworten_file_id
 */
class Antworten extends ActiveRecord
{
    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [];
    }

    /**
     * @return int
     */
    public function getAntwortId()
    {
        return $this->antwort_id;
    }

    /**
     * @return int
     */
    public function getInterviewId()
    {
        return $this->interview_id;
    }

    /**
     * @return int
     */
    public function getFrageId()
    {
        return $this->frage_id;
    }

    /**
     * @return int
     */
    public function getFragebogenId()
    {
        return $this->fragebogen_id;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return int
     */
    public function getAntwortenFileId()
    {
        return $this->antworten_file_id;
    }

    public function getKnot()
    {
        return $this->hasOne(Knoten::className(), ['nr' => 'value']);
    }
}