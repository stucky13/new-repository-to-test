<?php

namespace app\modules\data\models;

use app\components\FormModel;

/**
 * Class AntwortenForm
 * @package app\modules\data\models
 */
class AntwortenForm extends FormModel
{
    public $interview_id;
    public $fragebogen_id;
    public $frage_id;
    public $value;
    public $antworten_file_id;

    public function rules()
    {
        return [
            [
                ['fragebogen_id', 'frage_id', 'antworten_file_id', 'interview_id'],
                'required'
            ],
        ];
    }

    public function save()
    {
        $answer = new Antworten();

        $answer->interview_id = $this->interview_id;
        $answer->fragebogen_id = $this->fragebogen_id;
        $answer->frage_id = $this->frage_id;
        $answer->value = $this->value;
        $answer->antworten_file_id = $this->antworten_file_id;

        return $answer->save();
    }
}