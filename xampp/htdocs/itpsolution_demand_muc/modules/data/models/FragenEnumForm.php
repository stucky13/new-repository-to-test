<?php

namespace app\modules\data\models;


use app\models\Model;
use app\modules\data\Module;
use Yii;


/**
 * Class FragenEnumForm
 *
 * @package app\modules\questionnaire\models
 */
class FragenEnumForm extends Model
{
    public $frage_id;
    public $enum_id;
    public $value;
    public $antworttyp;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                ['value'],
                'required',
                'when' => function () {
                    return $this->antworttyp == 'user_defined';
                },
                'whenClient' => "function(attribute, value) {
                    return $('#fragenform-antworttyp').val() == 'user_defined';
                }"
            ],
            [['frage_id', 'enum_id'], 'integer'],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'frage_id' => Module::t('questionnaire', 'question id'),
            'enum_id' => Module::t('questionnaire', 'question'),
            'value' => Module::t('questionnaire', 'short text'),
        ];
    }

    /**
     * Creates a new question, or updates an existing one.
     */
    public function save()
    {
        /** @var FragenEnum $question */
        $question = FragenEnum::find()->where(['and', ['frage_id' => $this->frage_id], ['enum_id' => $this->enum_id]])->one();
        if (!$question) {
            $question = new FragenEnum();
            $question->user_created = Yii::$app->user->getId();
        } else {
            $question->user_updated = Yii::$app->user->getId();
        }

        $question->frage_id = $this->frage_id;
        $question->enum_id = $this->enum_id;
        $question->value = $this->value;

        if (!$question->save()) {
            return false;
        }

        return true;
    }
}