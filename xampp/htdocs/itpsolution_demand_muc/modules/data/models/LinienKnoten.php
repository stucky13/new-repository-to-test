<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 28.01.2019
 * Time: 10:55
 */

namespace app\modules\data\models;

use app\modules\data\Module;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class LinienKnoten
 * @package app\modules\data\models
 *
 * @property int $linien_id
 * @property int $nr
 * @property int $hst_sort
 */
class LinienKnoten extends ActiveRecord
{
    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'linien_id' => Module::t('data', 'route id'),
            'hst_sort' => Module::t('data', 'hst sort'),
            'nr' => Module::t('data', 'knot name')
        ];
    }

    /**
     * @return int
     */
    public function getLinienId()
    {
        return $this->linien_id;
    }

    /**
     * @return int
     */
    public function getNr()
    {
        return $this->nr;
    }

    /**
     * @return int
     */
    public function getHstSort()
    {
        return $this->hst_sort;
    }

    /**
     * Returns the station.
     *
     * @return ActiveQuery
     */
    public function getKnot()
    {
        return $this->hasOne(Knoten::className(), ['nr' => 'nr']);
    }

    /**
     * Returns the route.
     *
     * @return ActiveQuery
     */
    public function getRoute()
    {
        return $this->hasOne(Linien::className(), ['linien_id' => 'linien_id']);
    }
}