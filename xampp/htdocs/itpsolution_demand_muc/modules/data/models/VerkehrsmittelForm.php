<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 09.02.2018
 * Time: 15:39
 */

namespace app\modules\data\models;

use app\components\FormModel;
use Exception;
use Throwable;
use Yii;
use yii\db\StaleObjectException;

/**
 * Class VerkehrsmittelForm
 *
 * @package app\modules\data\models
 */
class VerkehrsmittelForm extends FormModel
{
    public $nr;
    public $name;

    public function scenarios()
    {
        return [
            static::SCENARIO_DEFAULT => $this->attributes(),
            static::SCENARIO_UPDATE => $this->attributes(),
        ];
    }


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [
                ['nr', 'name'],
                'required',
            ]
        ];
    }

    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'nr' => Yii::t('app', 'nr'),
            'name' => Yii::t('app', 'name')
        ];
    }

    /**
     * Creates a new organisation type, or updates an existing one.
     *
     * @return boolean true, if the organisation_type was saved successfully
     * @throws StaleObjectException
     * @throws Exception
     * @throws Throwable
     */
    public function save()
    {
        if ($this->getScenario() == static::SCENARIO_DEFAULT) {
            $verkehrsmittel = new Verkehrsmittel();
        } else {
            $verkehrsmittel = Verkehrsmittel::findOne($this->id);
        }
        $verkehrsmittel->nr = $this->nr;
        $verkehrsmittel->name = $this->name;

        return $verkehrsmittel->save();
    }
}