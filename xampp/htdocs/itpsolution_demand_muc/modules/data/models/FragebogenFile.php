<?php

namespace app\modules\data\models;

use app\components\AbstractActiveRecord;
use app\modules\data\Module;

/**
 * Class FragebogenFile
 * @package app\modules\data\models
 *
 * @property int $fragebogen_file_id
 * @property int $organisation_id
 * @property boolean $has_errors
 * @property string $path
 * @property string $path_errors
 * @property string $name
 * @property string $upload_ip
 * @property string $dt_created
 * @property string $dt_updated
 * @property int $user_created
 * @property int $user_updated
 * @property boolean $is_deleted
 * @property boolean $is_valid
 * @property boolean $is_processed
 * @property boolean $is_imported
 */
class FragebogenFile extends AbstractActiveRecord
{
    public function attributeLabels()
    {
        return [
            'fragebogen_file_id' => Module::t('data', "route file id"),
            'has_errors' => Module::t('data', 'has errors'),
            'name' => Module::t('data', 'filename'),
            'user_created' => Module::t('data', "user created"),
            'dt_created' => Module::t('data', 'dt created'),
            'upload_ip' => Module::t('data', 'upload ip'),
            'path_errors' => Module::t('data', 'path errors'),
            'organisation_id' => Module::t('data', 'organisation id'),
            'is_imported' => Module::t('data', 'is imported')
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->fragebogen_file_id;
    }

    /**
     * @return bool
     */
    public function isHasErrors()
    {
        return $this->has_errors;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getPathErrors()
    {
        return $this->path_errors;
    }

    /**
     * @return bool
     */
    public function isValid()
    {
        return $this->is_valid;
    }

    /**
     * @return bool
     */
    public function isProcessed()
    {
        return $this->is_processed;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getUploadIp()
    {
        return $this->upload_ip;
    }

    /**
     * @return int
     */
    public function getUserCreated()
    {
        return $this->user_created;
    }

    /**
     * @return string
     */
    public function getDtCreated()
    {
        return $this->dt_created;
    }

    /**
     * @return int
     */
    public function getUserUpdated()
    {
        return $this->user_updated;
    }

    /**
     * @return string
     */
    public function getDtUpdated()
    {
        return $this->dt_updated;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @return int
     */
    public function getOrganisationId()
    {
        return $this->organisation_id;
    }

    /**
     * @return bool
     */
    public function isImported()
    {
        return $this->is_imported;
    }

    public function getFragebogenFileErrors()
    {
        return $this->hasMany(FragebogenFileError::className(), ['fragebogen_file_id' => 'fragebogen_file_id']);
    }
}