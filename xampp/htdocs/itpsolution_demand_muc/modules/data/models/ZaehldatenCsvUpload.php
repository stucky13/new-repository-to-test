<?php

namespace app\modules\data\models;

use app\components\AbstractCsvUpload;
use app\components\Constants;
use app\components\CsvValidator;
use Throwable;
use Yii;
use yii\db\Exception;
use yii\web\UploadedFile;

class ZaehldatenCsvUpload extends AbstractCsvUpload
{
    public $organisation_id;
    public $subFolder = "zaehldaten/";
    public $countFileId;

    /**
     * @var null | UploadedFile
     */
    public $csvFile = null;

    //api
    public $userCreated = null;

    //validation
    /** @var CsvValidator $ticketCsvValidator */
    public $csvValidator;

    function rules()
    {
        return array_merge(parent::rules(), [
            [
                'csvFile', 'validateContent'
            ],
            [
                'csvFile',
                'required'
            ]
        ]);
    }

    /**
     * Validate income report
     * @param $attribute
     * @throws \Exception
     */
    public function validateContent($attribute)
    {
        $this->csvValidator = new CsvValidator($this, $this->ticketCsvRules(), Constants::CSV_HEADER_ZAEHLDATEN_FILE);
        $this->csvValidator->loadCsvFromModel($this, $attribute);
        $this->csvValidator->validateCsv();

        if (!empty($this->csvValidator->getCsvCritical())) {
            foreach ($this->csvValidator->getCsvCritical() as $error) {
                $this->addError($attribute, $error);
            }
            //Critical errors occured abort for now
            return;
        }
    }

    function ticketCsvRules()
    {
        return [
            [
                'validateSingle',
                0,
                [
                    'validator' => 'integer'
                ],
                'level' => CsvValidator::ERROR_CRITICAL
            ]
        ];
    }

    /**
     * Save data
     * @return bool
     * @throws \Exception
     * @throws Throwable
     */
    public function save()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $countFile = new ZaehldatenFile();
            $countFile->organisation_id = $this->organisation_id;
            $countFile->path = $this->uploadedPath;
            $countFile->name = $this->name;
            // Cut if last 2 digits of IP
            $countFile->upload_ip = substr(Yii::$app->request->remoteIP, 0, -2);
            $countFile->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();

            if (!$countFile->save()) {
                throw new Exception("Could not save TicketFile to DB");
            }

            $this->countFileId = $countFile->getPrimaryKey();

            if (!empty($this->csvValidator->getCsvErrors())) {
                $errorFile = $this->uploadedPath . '_fehler.txt';
                file_put_contents(Yii::getAlias($errorFile), implode("\n", $this->csvValidator->getCsvErrors()));
                $countFile->path_errors = $errorFile;
                $countFile->has_errors = true;
            } else {
                $countFile->is_valid = true;
            }
            $countFile->save();

            foreach ($this->csvValidator->getCsvErrors() as $message) {
                $countFileError = new ZaehldatenFileError();
                $countFileError->zaehldaten_file_id = $this->countFileId;
                $countFileError->error_type = CsvValidator::ERROR_NORMAL;
                $countFileError->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();
                $countFileError->detail = $message;
                if (!$countFileError->save()) {
                    throw new Exception("Could not save ZaehldatenFileError to DB");
                }
            }

            foreach ($this->csvValidator->getCsvWarnings() as $message) {
                $countFileError = new ZaehldatenFileError();
                $countFileError->zaehldaten_file_id = $this->countFileId;
                $countFileError->error_type = CsvValidator::ERROR_WARNING;
                $countFileError->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();
                $countFileError->detail = $message;
                if (!$countFileError->save()) {
                    throw new Exception("Could not save ZaehldatenFileError to DB");
                }
            }

            if (!$this->import($countFile)) {
                throw new Exception("Could not save Zaehldatene to DB");
            }

            $transaction->commit();
            return true;
        } catch (yii\db\Exception $e) {
            //print_r($e);die();
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * @param ZaehldatenFile $countFile
     * @return bool
     * @throws Exception
     */
    private function import($countFile)
    {
        //TODO: Transaktion in einer Transaktion? gilt für alle CsvUploads
        $transaction = Yii::$app->getDb()->beginTransaction();
        $csvData = $this->getCsv($countFile->getPath());
        $this->countFileId = $countFile->getId();
        $countFileUserCreated = $countFile->getUserCreated();

        try {
            foreach ($csvData as $index => $row) {
                $lineValues = array_combine(array_map('strtolower', Constants::CSV_HEADER_ZAEHLDATEN_FILE), $row);

                $countData = new ZaehldatenForm();
                foreach ($lineValues as $key => $value) {
                    $countData->$key = $value;
                }
                $countData->zaehldaten_file_id = $this->countFileId;

                //TODO: Warum?
                /*if (ZaehldatenFile::findOne($countData->zaehldaten_file_id)) {
                    continue;
                }*/

                if ($countData->validate()) {
                    $countData->save();
                } else {
                    foreach ($countData->errors as $field => $fieldErrors) {
                        foreach ($fieldErrors as $error) {
                            $fileError = new ZaehldatenFileError();
                            $fileError->zaehldaten_file_id = $this->countFileId;
                            $fileError->user_created = $countFileUserCreated;
                            $fileError->detail = "Zeile " . $index . ": Feld " . $field . ": " . $error;
                            $fileError->save();
                        }
                    }
                }

                unset($lineValues);
                unset($fileError);
                unset($countData);
                gc_collect_cycles();
            }
            $transaction->commit();
        } catch (Exception $e) {
            print_r($e);
            $transaction->rollBack();
        }

        return true;
    }
}