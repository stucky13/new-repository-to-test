<?php

namespace app\modules\data\models;

use app\components\AbstractCsvUpload;
use app\components\Constants;
use app\components\CsvValidator;
use Throwable;
use Yii;
use yii\db\Exception;
use yii\web\UploadedFile;

class FragebogenCsvUpload extends AbstractCsvUpload
{
    public $organisation_id;
    public $subFolder = "fragebogen/";
    public $questionnaireFileId;
    public $questionnaireId;

    /**
     * @var null | UploadedFile
     */
    public $csvFile = null;

    //api
    public $userCreated = null;

    //validation
    /** @var CsvValidator $csvValidator */
    public $csvValidator;

    function rules()
    {
        return array_merge(parent::rules(), [
            [
                'csvFile', 'validateContent'
            ],
            [
                'csvFile',
                'required'
            ]
        ]);
    }

    /**
     * Validate questionnaire file
     * @param $attribute
     * @throws \Exception
     */
    public function validateContent($attribute)
    {
        $this->csvValidator = new CsvValidator($this, $this->ticketCsvRules(), Constants::CSV_HEADER_FRAGEBOGEN_FILE);
        $this->csvValidator->loadCsvFromModel($this, $attribute);
        $this->csvValidator->validateCsv();

        if (!empty($this->csvValidator->getCsvCritical())) {
            foreach ($this->csvValidator->getCsvCritical() as $error) {
                $this->addError($attribute, $error);
            }
            //Critical errors occured abort for now
            return;
        }
    }

    function ticketCsvRules()
    {
        return [
            [
                'validateSingle',
                0,
                [
                    'validator' => 'integer'
                ],
                'level' => CsvValidator::ERROR_CRITICAL
            ]
        ];
    }

    /**
     * Save data
     * @return bool
     * @throws \Exception
     * @throws Throwable
     */
    public function save()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $questionnaireFile = new FragebogenFile();
            $questionnaireFile->organisation_id = $this->organisation_id;
            $questionnaireFile->path = $this->uploadedPath;
            $questionnaireFile->name = $this->name;
            // Cut if last 2 digits of IP
            $questionnaireFile->upload_ip = substr(Yii::$app->request->remoteIP, 0, -2);
            $questionnaireFile->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();

            if (!$questionnaireFile->save()) {
                throw new Exception("Could not save TicketFile to DB");
            }

            $this->questionnaireFileId = $questionnaireFile->getPrimaryKey();

            if (!empty($this->csvValidator->getCsvErrors())) {
                $errorFile = $this->uploadedPath . '_fehler.txt';
                file_put_contents(Yii::getAlias($errorFile), implode("\n", $this->csvValidator->getCsvErrors()));
                $questionnaireFile->path_errors = $errorFile;
                $questionnaireFile->has_errors = true;
            } else {
                $questionnaireFile->is_valid = true;
            }
            $questionnaireFile->save();

            foreach ($this->csvValidator->getCsvErrors() as $message) {
                $questionnaireFileError = new FragebogenFileError();
                $questionnaireFileError->fragebogen_file_id = $this->questionnaireFileId;
                $questionnaireFileError->error_type = CsvValidator::ERROR_NORMAL;
                $questionnaireFileError->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();
                $questionnaireFileError->detail = $message;
                if (!$questionnaireFileError->save()) {
                    throw new Exception("Could not save FragebogenFileError to DB");
                }
            }

            foreach ($this->csvValidator->getCsvWarnings() as $message) {
                $questionnaireFileError = new FragebogenFileError();
                $questionnaireFileError->fragebogen_file_id = $this->questionnaireFileId;
                $questionnaireFileError->error_type = CsvValidator::ERROR_WARNING;
                $questionnaireFileError->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();
                $questionnaireFileError->detail = $message;
                if (!$questionnaireFileError->save()) {
                    throw new Exception("Could not save FragebogenFileError to DB");
                }
            }

            if (!$this->import($questionnaireFile)) {
                throw new Exception("Could not save Fragebogen to DB");
            }

            $transaction->commit();
            return true;
        } catch (yii\db\Exception $e) {
            //print_r($e);die();
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * @param $questionnaireFile FragebogenFile
     * @return bool
     * @throws Exception
     */
    private function import($questionnaireFile)
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        $csvData = $this->getCsv($questionnaireFile->getPath());
        $questionnaireFileUserCreated = $questionnaireFile->getUserCreated();

        try {
            $questionnaire = new FragebogenForm();
            $questionnaire->fragebogen_file_id = $this->questionnaireFileId;
            $questionnaire->save();

            $this->questionnaireId = Yii::$app->db->getLastInsertID();

            foreach ($csvData as $row) {
                $lineValues = array_combine(array_map('strtolower', Constants::CSV_HEADER_FRAGEBOGEN_FILE), $row);

                $questionQuestionnaire = new FrageFragebogenForm();
                $questionQuestionnaire->frage_id = $lineValues['frage_id'];
                $questionQuestionnaire->fragebogen_id = $this->questionnaireId;

                if ($questionQuestionnaire->validate()) {
                    $questionQuestionnaire->save();
                } else {
                    foreach ($questionQuestionnaire->errors as $field => $fieldErrors) {
                        foreach ($fieldErrors as $error) {
                            $fileError = new FragebogenFileError();
                            $fileError->fragebogen_file_id = $this->questionnaireFileId;
                            $fileError->user_created = $questionnaireFileUserCreated;
                            $fileError->detail = "Zeile " . $row . ": Feld " . $field . ": " . $error;
                            $fileError->save();
                        }
                    }
                }

                unset($lineValues);
                unset($fileError);
                unset($questionQuestionnaire);
                gc_collect_cycles();
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
        }

        return true;
    }
}