<?php

namespace app\modules\data\models;

use app\components\AbstractCsvUpload;
use app\components\CsvValidator;
use DateTime;
use Throwable;
use Yii;
use yii\db\Exception;
use yii\web\UploadedFile;

class AntwortenCsvUpload extends AbstractCsvUpload
{
    public $organisation_id;
    public $subFolder = "antworten/";
    public $surveyFileId;
    public $questionnaireId;
    /**
     * @var null | UploadedFile
     */
    public $csvFile = null;

    //api
    public $userCreated = null;

    //validation
    /** @var CsvValidator $csvValidator */
    public $csvValidator;


    function rules()
    {
        return array_merge(parent::rules(), [

            [
                'csvFile',
                'required'
            ],
            [
                'csvFile', 'validateContent'
            ],
            [
                'csvFile', 'validateAnswers'
            ]
        ]);
    }

    /**
     * Validate survey file content
     * @param $attribute
     * @throws \Exception
     */
    public function validateContent($attribute)
    {
        $this->csvValidator = new CsvValidator($this, $this->csvRules());
        $this->csvValidator->loadCsvFromModel($this, $attribute);
        $this->csvValidator->validateCsv();

        if (!empty($this->csvValidator->getCsvCritical())) {
            foreach ($this->csvValidator->getCsvCritical() as $error) {
                $this->addError($attribute, $error);
            }
            //Critical errors occured abort for now
            return;
        }
    }

    function csvRules()
    {
        return [
            [
                'validateSingle',
                0,
                [
                    'validator' => 'integer'
                ],
                'level' => CsvValidator::ERROR_NORMAL
            ]
        ];
    }

    /**
     * Validates the given answers.
     *
     * @param string $attribute the attribute name
     * @param array $params the validation params
     */
    public function validateAnswers($attribute, $params)
    {
        $csvData = $this->getCsv($this->csvFile->tempName);
        $csvHeader = $this->getCsvHeader($this->csvFile->tempName);

        foreach ($csvData as $rowIndex => $row) {
            foreach ($row as $columnIndex => $field) {
                // Skip empty fields
                if (!$field) {
                    continue;
                }
                /** @var Fragen $question */
                $question = Fragen::find()->where(['frage_id' => $csvHeader[$columnIndex]])->one();
                $answerType = $question->getAntworttyp();
                $reference = $question->getReference();
                $referenceField = $question->getField();
                $categoryId = $question->getCategoryId();

                switch ($answerType) {
                    case 'boolean':
                        if ($field != 'true' && $field != 'false' && $field != 'Ja' && $field != 'Nein' && $field != 'X') {
                            $this->addError($attribute, "Feld in Spalte $columnIndex Reihe $rowIndex ist kein Boolean.");
                        }
                        break;
                    case 'integer':
                        if (!preg_match('/^\d+$/', $field)) {
                            $this->addError($attribute, "Feld in Spalte $columnIndex Reihe $rowIndex ist keine Ganzzahl.");
                        } else if (!($field >= $question->getWertebereichMin() && $field <= $question->getWertebereichMax())) {
                            $this->addError($attribute, "Feld in Spalte $columnIndex Reihe $rowIndex liegt nicht im vorgesehenen Wertebereich.");
                        }
                        break;
                    case 'decimal':
                        $decimal = str_replace(',', '.', $field);
                        $decimalSplit = explode('.', $decimal);
                        if (!is_numeric($decimal)) {
                            $this->addError($attribute, "Feld in Spalte $columnIndex Reihe $rowIndex ist keine Dezimalzahl.");
                        } else if ((count($decimalSplit) > 1 ? strlen($decimalSplit[1]) : 0) > $question->getNachkommastellen()) {
                            $this->addError($attribute, "Feld in Spalte $columnIndex Reihe $rowIndex enthält zuviele Nachkommastellen.");
                        }
                        break;
                    case 'date':
                        $format = 'd.m.Y H:i:s';
                        $date = DateTime::createFromFormat($format, $field);
                        if ($date && $date->format($format) != $field) {
                            $this->addError($attribute, "Feld in Spalte $columnIndex Reihe $rowIndex besitzt nicht das Datumsformatm $format.");
                        }
                        break;
                    case 'reference':
                        $condition = " WHERE $referenceField = \"$field\"";
                        if ($reference == 'fahrschein') {
                            $condition .= $categoryId ? " AND (kategorie1 = $categoryId OR kategorie2 = $categoryId OR kategorie3 = $categoryId OR kategorie4 = $categoryId)" : "";
                        }
                        $query = "SELECT * FROM $reference" . $condition;
                        try {
                            $result = Yii::$app->db->createCommand($query)->execute();
                            if (!$result) {
                                $this->addError($attribute, "Feld in Spalte $columnIndex Reihe $rowIndex ist nicht in der Tabelle $reference" . ($categoryId ? "mit der Kategorie $categoryId" : "") . "enthalten.");
                            }
                        } catch (yii\db\Exception $e) {
                            $this->addError($attribute, $e->getName());
                        }
                        break;
                    case 'user_defined':
                        if (!FragenEnum::find()->where(['value' => $field])->one()) {
                            $this->addError($attribute, "Feld in Spalte $columnIndex Reihe $rowIndex ist kein vorgeschlagener Wert.");
                        }
                        break;
                }
            }
        }
    }


    /**
     * Save data
     * @return bool
     * @throws \Exception
     * @throws Throwable
     */
    public function save()
    {
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $surveyFile = new AntwortenFile();
            $surveyFile->organisation_id = $this->organisation_id;
            $surveyFile->path = $this->uploadedPath;
            $surveyFile->name = $this->name;
            // Cut if last 2 digits of IP
            $surveyFile->upload_ip = substr(Yii::$app->request->remoteIP, 0, -2);
            $surveyFile->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();

            if (!$surveyFile->save()) {
                throw new Exception("Could not save SurveyFile to DB");
            }

            $this->surveyFileId = $surveyFile->getPrimaryKey();

            if (!empty($this->csvValidator->getCsvErrors())) {
                $errorFile = $this->uploadedPath . '_fehler.txt';
                file_put_contents(Yii::getAlias($errorFile), implode("\n", $this->csvValidator->getCsvErrors()));
                $surveyFile->path_errors = $errorFile;
                $surveyFile->has_errors = true;
            } else {
                $surveyFile->is_valid = true;
            }
            $surveyFile->save();

            foreach ($this->csvValidator->getCsvErrors() as $message) {
                $surveyFileError = new AntwortenFileError();
                $surveyFileError->antworten_file_id = $this->surveyFileId;
                $surveyFileError->error_type = CsvValidator::ERROR_NORMAL;
                $surveyFileError->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();
                $surveyFileError->detail = $message;
                if (!$surveyFileError->save()) {
                    throw new Exception("Could not save BefragungsdatenFileError to DB");
                }
            }

            foreach ($this->csvValidator->getCsvWarnings() as $message) {
                $surveyFileError = new AntwortenFileError();
                $surveyFileError->antworten_file_id = $this->surveyFileId;
                $surveyFileError->error_type = CsvValidator::ERROR_WARNING;
                $surveyFileError->user_created = $this->userCreated ? $this->userCreated : Yii::$app->getUser()->getId();
                $surveyFileError->detail = $message;
                if (!$surveyFileError->save()) {
                    throw new Exception("Could not save BefragungsdatenFileError to DB");
                }
            }

            if (!$this->import($surveyFile)) {
                throw new Exception("Could not save Befragungsdatene to DB");
            }

            $transaction->commit();
            return true;
        } catch (yii\db\Exception $e) {
            //print_r($e);die();
            $transaction->rollBack();
            return false;
        }
    }

    /**
     * @param $surveyFile AntwortenFile
     * @return bool
     * @throws Exception
     */
    private function import($surveyFile)
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        $csvData = $this->getCsv($surveyFile->getPath());
        $csvHeader = $this->getCsvHeader($surveyFile->getPath());
        $this->surveyFileId = $surveyFile->getId();
        $surveyFileUserCreated = $surveyFile->getUserCreated();
        $fieldCount = count($csvHeader);

        try {
            foreach ($csvData as $rowIndex => $row) {
                if ($this->isEmptyRow($row, $fieldCount)) {
                    continue;
                };
                $interviewId = $rowIndex + 1;
                foreach ($row as $index => $field) {
                    $answer = new AntwortenForm();
                    $answer->frage_id = $csvHeader[$index];
                    $answer->interview_id = $interviewId;
                    $answer->fragebogen_id = $this->questionnaireId;
                    $answer->value = $field;
                    $answer->antworten_file_id = $this->surveyFileId;

                    if ($answer->validate()) {
                        $answer->save();
                    } else {
                        foreach ($answer->errors as $field => $fieldErrors) {
                            foreach ($fieldErrors as $error) {
                                $fileError = new FragebogenFileError();
                                $fileError->fragebogen_file_id = $this->surveyFileId;
                                $fileError->user_created = $surveyFileUserCreated;
                                $fileError->detail = "Zeile " . ($rowIndex + 1) . ": Feld " . $field . ": " . $error;
                                $fileError->save();
                            }
                        }
                    }

                    unset($lineValues);
                    unset($fileError);
                    unset($answer);
                    gc_collect_cycles();
                }
            }
            $transaction->commit();
        } catch (Exception $e) {
            $transaction->rollBack();
        }

        return true;
    }

    /**
     * Checks if all fields of the row are empty.
     *
     * @param $row
     * @param $fieldCount
     * @return boolean
     */
    private function isEmptyRow($row, $fieldCount)
    {
        $counter = 0;
        foreach ($row as $field) {
            if (strlen($field) == 0) {
                $counter++;
            }
        }

        return $counter === $fieldCount;
    }
}