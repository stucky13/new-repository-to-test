<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 15.01.2019
 * Time: 14:09
 */

namespace app\modules\data\models;

use app\modules\data\Module;
use yii\db\ActiveRecord;

/**
 * Class Zaehldaten
 * @package app\modules\data\models
 * @property int $zaehldaten_id
 * @property int $strecke
 * @property string $linie
 * @property int $zugnummer
 * @property int $zeitschicht
 * @property int $richtung
 * @property int $hst_sort
 * @property int $bhf_id
 * @property string $bhf_name
 * @property int $uhrzeit
 * @property int $zaehlung
 * @property int $ein
 * @property int $aus
 * @property int $bes
 * @property int $bes1
 * @property int $bes2
 * @property int $ein_f
 * @property int $aus_f
 * @property int $c_bes_f
 * @property string $abschnitt_km
 * @property string $f_tag
 * @property int $f_jahr
 * @property int $p_tag
 * @property int $r_tag
 * @property string $pkm_tag
 * @property string $rkm_tag
 * @property int $p_jahr
 * @property int $r_jahr
 * @property string $pkm_jahr
 * @property string $rkm_jahr
 * @property int $zaehldaten_file_id
 */
class Zaehldaten extends ActiveRecord
{
    /**
     * @param $value
     * @param $orgId
     * @return array|ActiveRecord[]
     */
    public static function getSumOfPassengers($value, $orgId)
    {
        return Zaehldaten::find()->select('sum(' . $value . ') AS ' . $value)
            ->leftJoin('erhebungsdaten', 'erhebungsdaten.count_data_id = zaehldaten.zaehldaten_file_id')
            ->where(['erhebungsdaten.organisation_id' => $orgId])
            ->groupBy('erhebungsdaten.count_data_id')
            ->orderBy('erhebungsdaten.year', SORT_ASC)->all();
    }

    /**
     * @param $value
     * @param $orgId
     * @return array|ActiveRecord[]
     */
    public static function getAvgOfPkm($value, $orgId)
    {
        return Zaehldaten::find()->select('avg(' . $value . ') AS ' . $value)
            ->leftJoin('erhebungsdaten', 'erhebungsdaten.count_data_id = zaehldaten.zaehldaten_file_id')
            ->where(['erhebungsdaten.organisation_id' => $orgId])
            ->groupBy('erhebungsdaten.count_data_id')
            ->orderBy('erhebungsdaten.year', SORT_ASC)->all();
    }

    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('data', 'id'),
            'zaehldaten_file_id' => Module::t('data', 'ticket file id')
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getStrecke()
    {
        return $this->strecke;
    }

    /**
     * @return string
     */
    public function getLinie()
    {
        return $this->linie;
    }

    /**
     * @return int
     */
    public function getZugnummer()
    {
        return $this->zugnummer;
    }

    /**
     * @return int
     */
    public function getZeitschicht()
    {
        return $this->zeitschicht;
    }

    /**
     * @return int
     */
    public function getRichtung()
    {
        return $this->richtung;
    }

    /**
     * @return int
     */
    public function getHstSort()
    {
        return $this->hst_sort;
    }

    /**
     * @return int
     */
    public function getBhfId()
    {
        return $this->bhf_id;
    }

    /**
     * @return string
     */
    public function getBhfName()
    {
        return $this->bhf_name;
    }

    /**
     * @return int
     */
    public function getUhrzeit()
    {
        return $this->uhrzeit;
    }

    /**
     * @return int
     */
    public function getZaehlung()
    {
        return $this->zaehlung;
    }

    /**
     * @return int
     */
    public function getEin()
    {
        return $this->ein;
    }

    /**
     * @return int
     */
    public function getAus()
    {
        return $this->aus;
    }

    /**
     * @return int
     */
    public function getBes()
    {
        return $this->bes;
    }

    /**
     * @return int
     */
    public function getBes1()
    {
        return $this->bes1;
    }

    /**
     * @return int
     */
    public function getBes2()
    {
        return $this->bes2;
    }

    /**
     * @return int
     */
    public function getEinF()
    {
        return $this->ein_f;
    }

    /**
     * @return int
     */
    public function getAusF()
    {
        return $this->aus_f;
    }

    /**
     * @return int
     */
    public function getCBesF()
    {
        return $this->c_bes_f;
    }

    /**
     * @return float
     */
    public function getAbschnittKm()
    {
        return $this->abschnitt_km;
    }

    /**
     * @return float
     */
    public function getFTag()
    {
        return $this->f_tag;
    }

    /**
     * @return int
     */
    public function getFJahr()
    {
        return $this->f_jahr;
    }

    /**
     * @return int
     */
    public function getPTag()
    {
        return $this->p_tag;
    }

    /**
     * @return int
     */
    public function getRTag()
    {
        return $this->r_tag;
    }

    /**
     * @return float
     */
    public function getPkmTag()
    {
        return $this->pkm_tag;
    }

    /**
     * @return float
     */
    public function getRkmTag()
    {
        return $this->rkm_tag;
    }

    /**
     * @return int
     */
    public function getPJahr()
    {
        return $this->p_jahr;
    }

    /**
     * @return int
     */
    public function getRJahr()
    {
        return $this->r_jahr;
    }

    /**
     * @return float
     */
    public function getPkmJahr()
    {
        return $this->pkm_jahr;
    }

    /**
     * @return float
     */
    public function getRkmJahr()
    {
        return $this->rkm_jahr;
    }

    /**
     * @return int
     */
    public function getZaehldatenFileId()
    {
        return $this->zaehldaten_file_id;
    }

}