<?php
/**
 * Created by PhpStorm.
 * User: Wenk
 * Date: 22.01.2019
 * Time: 14:09
 */

namespace app\modules\data\models;

use app\modules\data\Module;
use yii\db\ActiveRecord;

/**
 * Class Erhebungsdaten
 * @package app\modules\data\models
 * @property int $erhebungsdaten_id
 * @property int $questionnaire_id
 * @property int $survey_id
 * @property int $count_data_id
 * @property int $year
 * @property string $period
 * @property string $description
 * @property string $note
 * @property int organisation_id
 *
 */
class Erhebungsdaten extends ActiveRecord
{
    /**
     * @return array the attribute labels
     */
    public function attributeLabels()
    {
        return [
            'id' => Module::t('data', 'id')
        ];
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->erhebungsdaten_id;
    }

    /**
     * @return int
     */
    public function getQuestionnaireId()
    {
        return $this->questionnaire_id;
    }

    /**
     * @return int
     */
    public function getSurveyId()
    {
        return $this->survey_id;
    }

    /**
     * @return int
     */
    public function getCountDataId()
    {
        return $this->count_data_id;
    }

    /**
     * @return int
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * @return string
     */
    public function getPeriod()
    {
        return $this->period;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * @return int
     */
    public function getOrganisationId()
    {
        return $this->organisation_id;
    }


}