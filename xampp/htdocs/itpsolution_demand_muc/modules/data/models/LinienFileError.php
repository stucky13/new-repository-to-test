<?php

namespace app\modules\data\models;


use app\components\AbstractActiveRecord;

/**
 * Class LinienFileError
 * @package app\modules\data\models
 *
 * @property int $linien_file_id
 * @property int $error_type
 * @property boolean $is_deleted
 * @property string $detail
 * @property int $user_created
 * @property string $dt_created
 * @property int $user_updated
 * @property string $dt_updated
 */
class LinienFileError extends AbstractActiveRecord
{
    /**
     * @return int
     */
    public function getLinienFileId()
    {
        return $this->linien_file_id;
    }

    /**
     * @return int
     */
    public function getErrorType()
    {
        return $this->error_type;
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->is_deleted;
    }

    /**
     * @return string
     */
    public function getDetail()
    {
        return $this->detail;
    }

    /**
     * @return int
     */
    public function getUserCreated()
    {
        return $this->user_created;
    }

    /**
     * @return string
     */
    public function getDtCreated()
    {
        return $this->dt_created;
    }

    /**
     * @return int
     */
    public function getUserUpdated()
    {
        return $this->user_updated;
    }

    /**
     * @return string
     */
    public function getDtUpdated()
    {
        return $this->dt_updated;
    }

}