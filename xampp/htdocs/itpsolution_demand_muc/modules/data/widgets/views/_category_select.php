<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 12.02.2018
 * Time: 16:49
 */

/**
 * @var ActiveForm $form
 * @var string $attribute
 * @var Model $model
 * @var string $updateUrl
 * @var int $categoryId
 * @var string $initialText
 * @var boolean $readOnly
 */

use kartik\select2\Select2;
use yii\base\Model;
use yii\bootstrap\ActiveForm;
use yii\web\JsExpression;

?>
<div class="col-md-6">

    <?php
    echo $form->field($model, $attribute)->widget(Select2::className(), [
        'initValueText' => $initialText,
        'options' => [
            'placeholder' => Yii::t('app', 'Search for a category...'),
            'disabled' => $readOnly,

            'class' => 'form-control form-control-color'
        ],
        'pluginOptions' => [
            'allowClear' => true,
            'language' => [
                'errorLoading' => new JsExpression("function () { return 'Waiting for results...'; }"),
            ],
            'ajax' => [
                'url' => $updateUrl,
                'dataType' => 'json',
                'data' => new JsExpression('function(params) { return {q: params.term}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function (category) { return category.text; }'),
            'templateSelection' => new JsExpression('function (category) { return category.text; }'),
        ],
    ]);
    ?>

</div>
