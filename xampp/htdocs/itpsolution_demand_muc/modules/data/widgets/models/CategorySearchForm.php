<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 12.02.2018
 * Time: 16:56
 */

namespace app\modules\core\widgets\models;

use app\modules\data\models\Category;
use yii\base\Model;
use yii\db\ActiveQuery;

/**
 * Class CategorySearchForm
 *
 * @package app\modules\core\widgets\models
 */
class CategorySearchForm extends Model
{
    public $searchInput;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                'searchInput',
                'required'
            ],
            [
                'searchInput',
                'string',
                'min' => 3
            ]
        ];
    }

    /**
     * @return ActiveQuery the query that returns all users that match the search input
     */
    public function search()
    {
        return Category::find()
            ->where(['LIKE', 'title', $this->searchInput])
            ->orWhere(['=', 'category_id', $this->searchInput])
            ->limit(20);
    }
}