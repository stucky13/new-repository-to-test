<?php

namespace app\modules\data\controllers;

use app\components\BaseController;
use app\components\Constants;
use app\components\FormModel;
use app\modules\data\models\Verkehrsmittel;
use app\modules\data\models\VerkehrsmittelForm;
use app\modules\data\Module;
use app\widgets\Alert;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\web\BadRequestHttpException;


/**
 * Class VerkehrsmittelController
 *
 * @package app\modules\data\controllers
 */
class VerkehrsmittelController extends BaseController
{
    /**
     * Displays the overview.
     *
     * @return string the content
     */
    public function actionIndex($page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::VIEW_VERKEHRSMITTEL)) {
            return $this->goHome();
        }
        return $this->render("index", [
            'dataProvider' => new ActiveDataProvider([
                'query' => Verkehrsmittel::find()
            ]),
            'page' => $page
        ]);
    }

    /**
     * Displays the detail view for the verkehrsmittel.
     *
     * @param int $id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid verkehrsmittel id
     */
    public function actionView($id, $page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::VIEW_VERKEHRSMITTEL)) {
            return $this->goHome();
        }
        $verkehrsmittel = Verkehrsmittel::findOne($id);
        if ($verkehrsmittel == null) {
            throw new BadRequestHttpException(Module::t('data', "Invalid verkehrsmittel id ") . $id);
        }

        return $this->render('view', [
            'model' => $verkehrsmittel,
            'page' => $page
        ]);
    }

    /**
     * Displays the verkehrsmittel form.
     *
     * @return string the content
     * @throws \Exception
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionCreate($page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::EDIT_VERKEHRSMITTE)) {
            return $this->goHome();
        }
        $model = new VerkehrsmittelForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            $page = Yii::$app->request->post('page');
            Alert::addSuccess(Module::t('data', 'verkehrsmittel successfully created'));
            return $this->redirect(["index?page=$page"]);
        }

        return $this->render("form", [
            'model' => $model,
            'page' => $page
        ]);
    }

    /**
     * Displays the verkehrsmittel edit form.
     *
     * @param int $id the verkehrsmittel id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid verkehrsmittel id
     * @throws \Exception
     * @throws Throwable
     * @throws StaleObjectException
     */
    public function actionUpdate($id, $page = 1)
    {
        if (!Yii::$app->getUser()->can(Constants::EDIT_VERKEHRSMITTE)) {
            return $this->goHome();
        }
        /** @var Verkehrsmittel $verkehrsmittel */
        $verkehrsmittel = Verkehrsmittel::findOne($id);
        if ($verkehrsmittel == null) {
            throw new BadRequestHttpException(Module::t('data', "Failed to find verkehrsmittel with id ") . $id);
        }

        $model = new VerkehrsmittelForm(FormModel::SCENARIO_UPDATE, $id);

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            $page = Yii::$app->request->post('page');
            Alert::addSuccess(Module::t('data', 'verkehrsmittel %s successfully updated'), $model->name);
            return $this->redirect(["index?page=$page"]);
        } else {
            $model->nr = $verkehrsmittel->getNr();
            $model->name = $verkehrsmittel->getName();
        }

        return $this->render("form", [
            'model' => $model,
            'page' => $page,
        ]);
    }

    /**
     * Deletes the specified verkehrsmittel.
     *
     * @param int $id the verkehrsmittel id
     *
     * @return string the content
     * @throws BadRequestHttpException in case that $id is not a valid verkehrsmittel id
     * @throws Exception
     * @throws \Exception
     * @throws Throwable
     */
    public function actionDelete($id)
    {
        /** @var Verkehrsmittel $verkehrsmittel */
        $verkehrsmittel = Verkehrsmittel::findOne($id);
        if ($verkehrsmittel == null) {
            throw new BadRequestHttpException("Failed to find verkehrsmittel with id " . $id);
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $verkehrsmittel->delete();
            $transaction->commit();
            Alert::addSuccess(Module::t("admin", "verkehrsmittel %s successfully deleted"), $verkehrsmittel->getName());
        } catch (Exception $e) {
            print_r($e->getMessage());
            $transaction->rollBack();
            Alert::addError(Module::t("admin", "verkehrsmittel %s couldn't be deleted"), $verkehrsmittel->getName());
        }

        return $this->redirect(['index']);
    }
}