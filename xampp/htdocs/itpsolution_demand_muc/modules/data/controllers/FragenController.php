<?php

namespace app\modules\data\controllers;

use app\components\FormModel;
use app\modules\data\models\Category;
use app\modules\data\models\Fragen;
use app\modules\data\models\FragenEnum;
use app\modules\data\models\FragenEnumForm;
use app\modules\data\models\FragenForm;
use app\modules\data\models\ValueType;
use app\modules\data\Module;
use app\widgets\Alert;
use Throwable;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Response;


class FragenController extends Controller
{
    /**
     * Displays the overview.
     *
     * @param int $page
     * @return mixed
     */
    public function actionIndex($page = 1)
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Fragen::find(),
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'page' => $page
        ]);
    }

    /**
     * Displays the detail view for the specified question.
     * @param integer $id
     * @param integer $page
     * @return mixed
     * @throws BadRequestHttpException in case that $id is not a valid question id
     */
    public function actionView($id, $page = 1)
    {
        $question = Fragen::findOne($id);
        $questionEnums = $question->fragenEnums;

        if ($question == null) {
            throw new BadRequestHttpException(Module::t('questionnaire', "Invalid question id ") . $id);
        }

        $answerType = $question->getAntworttyp();

        $attributes = [
            'frage_id',
            'fragestellung',
            'kurztext',
            'antworttyp' => [
                'attribute' => 'antworttyp',
                'value' => function (Fragen $question) {
                    return $question->adjustQuestionType();
                }
            ]
        ];

        if ($answerType == 'integer') {
            $attributes[] = 'wertebereich_min';
            $attributes[] = 'wertebereich_max';
        } elseif ($answerType == 'decimal') {
            $attributes[] = 'nachkommastellen';
        } elseif ($answerType == 'reference') {
            $attributes['reference'] = [
                'attribute' => 'reference',
                'value' => function (Fragen $question) {
                    return $question->adjustReference();
                }
            ];
            $attributes['field'] = [
                'attribute' => 'field',
                'headerOptions' => ['class' => 'mediumButton gridview-header'],
                'value' => function (Fragen $question) {
                    return $question->adjustReferenceField();
                }
            ];

            if ($question->getReference() == 'fahrschein') {
                $attributes['category_id'] = [
                    'attribute' => 'category_id',
                    'value' => function (Fragen $question) {
                        return $question->getCategory()->one() ? $question->getCategory()->one()->getTitle() : '';
                    }
                ];
            }
        }
        $attributes['is_georef'] = [
            'attribute' => 'is_georef',
            'value' => function (Fragen $question) {
                return $question->isGeoref() ? 'Ja' : 'Nein';
            }
        ];
        $attributes['is_filter'] = [
            'attribute' => 'is_filter',
            'value' => function (Fragen $question) {
                return $question->isFilter() ? 'Ja' : 'Nein';
            }
        ];
        $attributes['is_value'] = [
            'attribute' => 'is_value',
            'value' => function (Fragen $question) {
                return $question->isGeoref() ? 'Ja' : 'Nein';
            }
        ];
        if ($question->isValue()) {
            $attributes['value_type'] = [
                'attribute' => 'value_type',
                'value' => function (Fragen $question) {
                    return $question->getValueType()->one() ? $question->getValueType()->one()->getName() : '';
                }
            ];
        }

        return $this->render('view', [
            'model' => $question,
            'questionEnums' => $questionEnums,
            'attributes' => $attributes,
            'page' => $page
        ]);
    }

    /**
     * Creates a new question.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @param int $page
     * @return mixed
     */
    public function actionCreate($page = 1)
    {
        /** @var FragenForm $model */
        $model = new FragenForm();
        $questionEnum = [new FragenEnumForm()];

        if ($model->load(Yii::$app->request->post())) {

            $questionEnum = FragenEnumForm::createMultiple(FragenEnumForm::className());
            FragenEnumForm::loadMultiple($questionEnum, Yii::$app->request->post());

            $validateQuestionEnum = FragenEnumForm::validateMultiple($questionEnum);

            $valid = $model->validate() && $validateQuestionEnum;
            $model->questionEnums = $questionEnum;

            if ($valid && $model->save()) {
                Alert::addSuccess(Module::t('questionnaire', 'question successfully created'));
                $page = Yii::$app->request->post('page');

                return $this->redirect(['index', 'page' => $page]);

            }
        }

        if (sizeof($questionEnum) == 0) {
            $questionEnum = [new FragenEnumForm()];
        }

        $assignableTypes = ['integer' => 'Ganzzahl', 'boolean' => 'Ja/Nein', 'decimal' => 'Zahl', 'date' => 'Datum', 'string' => 'Freitext', 'reference' => 'Referenzwerte', 'user_defined' => 'Benutzerdefiniert',];
        $referenceTables = ['fahrschein' => 'Fahrscheintabelle', 'linien' => 'Linientabelle', 'knoten' => 'Knotentabelle'];
        $referenceFields = [
            'fahrschein' => [
                'id' => 'Id',
                'name' => 'Name'
            ],
            'linien' => [
                'linien_id' => 'Id',
                'linien_name' => 'Name'
            ],
            'knoten' => [
                'nr' => 'Nr',
                'name' => 'Name'
            ]
        ];
        $ticketCategories = ArrayHelper::map(Category::find()->all(), 'category_id', 'title');
        $valueTypes = ArrayHelper::map(ValueType::find()->all(), 'value_type_id', 'name');

        return $this->render('form', [
            'model' => $model,
            'questionEnums' => $questionEnum,
            'assignableTypes' => $assignableTypes,
            'referenceTables' => $referenceTables,
            'referenceFields' => $referenceFields['fahrschein'],
            'ticketCategories' => $ticketCategories,
            'valueTypes' => $valueTypes,
            'page' => $page
        ]);
    }

    /**
     * Updates an existing question.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param $id
     * @param $page
     * @return string|Response
     * @throws BadRequestHttpException in case that $id is not a valid question id
     */
    public function actionUpdate($id, $page = 1)
    {
        /** @var Fragen $question */
        $question = Fragen::findOne($id);
        if ($question == null) {
            throw new BadRequestHttpException(Module::t('questionnaire', "Invalid question id ") . $id);
        }
        $questionEnums = $question->getFragenEnums()->asArray()->all();
        $model = new FragenForm(FormModel::SCENARIO_UPDATE, $id);

        $questionEnum = [new FragenEnumForm()];

        if ($model->load(Yii::$app->request->post())) {
            $questionEnum = FragenEnumForm::createMultiple(FragenEnumForm::className());
            FragenEnumForm::loadMultiple($questionEnum, Yii::$app->request->post());
            $validateQuestionEnum = FragenEnumForm::validateMultiple($questionEnum);

            $valid = $model->validate() && $validateQuestionEnum;
            $model->questionEnums = $questionEnum;
            $oldIDs = ArrayHelper::map($questionEnum, 'enum_id', 'enum_id');
            $deletedIDs = array_diff(array_filter(ArrayHelper::map($questionEnums, 'enum_id', 'enum_id')), $oldIDs);

            if (!empty($deletedIDs)) {
                FragenEnum::deleteAll(['enum_id' => $deletedIDs]);
            }

            if ($valid && $model->save()) {
                $page = Yii::$app->request->post('page');

                Alert::addSuccess(Module::t('questionnaire', 'question successfully updated'));
                return $this->redirect(['index', 'page' => $page]);
            }


        } else {
            $model->frage_id = $question->frage_id;
            $model->fragestellung = $question->fragestellung;
            $model->kurztext = $question->kurztext;
            $model->antworttyp = $question->antworttyp;
            $model->wertebereich_min = $question->wertebereich_min;
            $model->wertebereich_max = $question->wertebereich_max;
            $model->nachkommastellen = $question->nachkommastellen;
            $model->reference = $question->reference;
            $model->field = $question->field;
            $model->category_id = $question->category_id;
            $model->is_value = $question->is_value;
            $model->is_filter = $question->is_filter;
            $model->is_georef = $question->is_georef;
            $model->value_type = $question->value_type;

            $questionEnum = [];
            /** @var FragenEnum $enum */
            foreach ($question->getFragenEnums()->all() as $enum) {
                $questionEnumForm = new FragenEnumForm();
                $questionEnumForm->frage_id = $question->frage_id;
                $questionEnumForm->enum_id = $enum->getEnumId();
                $questionEnumForm->value = $enum->getValue();
                $questionEnum[] = $questionEnumForm;
            }

            if (sizeof($questionEnum) == 0) {
                $questionEnum = [new FragenEnumForm()];
            }
        }

        $assignableTypes = ['integer' => 'Ganzzahl', 'boolean' => 'Ja/Nein', 'decimal' => 'Zahl', 'date' => 'Datum', 'string' => 'Freitext', 'reference' => 'Referenzwerte', 'user_defined' => 'Benutzerdefiniert'];
        $referenceTables = ['fahrschein' => 'Fahrscheintabelle', 'linien' => 'Linientabelle', 'knoten' => 'Knotentabelle'];
        $referenceFields = [
            'fahrschein' => [
                'id' => 'Id',
                'name' => 'Name'
            ],
            'linien' => [
                'linien_id' => 'Id',
                'linien_name' => 'Name'
            ],
            'knoten' => [
                'nr' => 'Nr',
                'name' => 'Name'
            ]
        ];
        $ticketCategories = ArrayHelper::map(Category::find()->all(), 'category_id', 'title');
        $valueTypes = ArrayHelper::map(ValueType::find()->all(), 'value_type_id', 'name');

        return $this->render('form', [
            'model' => $model,
            'questionEnums' => $questionEnum,
            'assignableTypes' => $assignableTypes,
            'referenceTables' => $referenceTables,
            'referenceFields' => $referenceFields[$model->reference ? $model->reference : 'fahrschein'],
            'ticketCategories' => $ticketCategories,
            'valueTypes' => $valueTypes,
            'page' => $page
        ]);
    }

    /**
     * Deletes an existing question.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param $id
     * @param $page
     * @return Response
     * @throws BadRequestHttpException in case that $id is not a valid question id
     * @throws Throwable in case something is wrong with the db
     */
    public function actionDelete($id, $page = 1)
    {
        /** @var Fragen $question */
        $question = Fragen::findOne($id);
        if ($question == null) {
            throw new BadRequestHttpException(Module::t('questionnaire', "Invalid question id ") . $id);
        }
        $questionnaireQuestions = $question->getFrageFragebogen()->all();
        $answers = $question->getAntworten()->all();
        $questionName = $question->getFragestellung();
        /** @var FragenEnum[] $fragenEnums */
        $fragenEnums = $question->fragenEnums;

        $transaction = Yii::$app->db->beginTransaction();
        try {
            if (count($fragenEnums) > 0) {
                foreach ($fragenEnums as $enum) {
                    $enum->delete();
                }
            }
            if (count($questionnaireQuestions) > 0) {
                foreach ($questionnaireQuestions as $questionnaireQuestion) {
                    $questionnaireQuestion->delete();
                }
            }
            if (count($answers) > 0) {
                foreach ($answers as $answer) {
                    $answer->delete();
                }
            }
            $question->delete();

            $transaction->commit();
            Alert::addSuccess(Module::t("questionnaire", "question %s successfully deleted"), $questionName);
        } catch (Exception $e) {
            print_r($e->getMessage());
            $transaction->rollBack();
            Alert::addError(Module::t("questionnaire", "question %s couldn't be deleted"), $questionName);
        }

        return $this->redirect(['index', 'page' => $page]);
    }

    public function actionGetReferenceFields($reference)
    {
        /* set the prompt field */
        $options = [
            Module::t('questionnaire', 'select field')
        ];

        $referenceFields = [
            'fahrschein' => [
                'id' => 'Id',
                'name' => 'Name'
            ],
            'linien' => [
                'strecke_id' => 'Id',
                'strecke_name' => 'Name'
            ],
            'knoten' => [
                'nr' => 'Nr',
                'name' => 'Name'
            ]
        ];

        /* add options */
        foreach ($referenceFields[$reference] as $key => $referenceField) {
            $options[$key] = $referenceField;
        }

        return json_encode($options);
    }
}
