<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 12.02.2018
 * Time: 17:09
 */

namespace app\modules\data\controllers;

use app\components\BaseController;
use app\modules\data\models\Category;
use Yii;
use yii\web\Response;

/**
 * Class CategoryController
 * @package app\modules\core\controllers
 */
class CategoryController extends BaseController
{
    /**
     * This action handles the ajax call from the auto complete drop down list.
     * The first 20 results that match the given query will be returned.
     *
     * @param string $q the query string
     * @param integer $id the id of a given user
     *
     * @return array the data
     */
    public function actionQuery($q = null, $id = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $out = ['results' => ['id' => '', 'text' => '']];
        $categoryQuery = Category::find();

        if (!is_null($q)) {
            $categoryQuery->where(['like', 'title', $q])
                ->orWhere(['=', 'category_id', $q])
                ->limit(20)
                ->orderBy('title');
            $query = $categoryQuery->createCommand()->rawSql;
            $categories = $categoryQuery->all();

            $out['results'] = [];
            /** @var Category $category */
            foreach ($categories as $category) {
                $out['results'][] = ['id' => $category->getCategoryId(), 'text' => $category->getTitle()];
            }
        } else {
            $categories = $categoryQuery->all();
            $out['results'] = [];
            /** @var Category $category */
            foreach ($categories as $category) {
                $out['results'][] = ['id' => $category->getCategoryId(), 'text' => $category->getTitle()];
            }
        }

        if ($id > 0) {
            $out['results'] = ['id' => $id, 'text' => Category::findOne($id)->getTitle()];
        }
        return $out;
    }
}