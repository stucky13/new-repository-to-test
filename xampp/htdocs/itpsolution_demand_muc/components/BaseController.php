<?php

namespace app\components;

use Yii;
use yii\web\Controller;

class BaseController extends Controller
{

    /**
     * Checks if a password change is required. If yes, the user will get redirected to the password change site until he changes his password.
     *
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        /*if (Parameter::getPortalAktiv() == 0 && Yii::$app->controller->action->id !== 'inactive'){
            $session = Yii::$app->session;
            $session->destroy();
            return $this->redirect(['/site/inactive']);
        }*/

        if (!Yii::$app->user->isGuest && Yii::$app->user->getIdentity()->is_password_change_required == 1 && Yii::$app->controller->action->id !== 'password-change') {
            return $this->redirect(['/site/password-change']);
        }

        if (!parent::beforeAction($action)) {
            return false;
        }


        return true;
    }
}