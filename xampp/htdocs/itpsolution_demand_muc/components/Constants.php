<?php
/**
 * Created by PhpStorm.
 * User: Tobias
 * Date: 16.02.2018
 * Time: 13:09
 */

namespace app\components;

class Constants
{
    const EMAIL_FROM = 'Test@testmail.de';

    /* Permissions */
    const ADMIN = "admin";
    const EDIT_ORGANISATION_TYPE = "edit_org_type";
    const EDIT_ORGANISATION = "edit_org";
    const EDIT_VERKEHRSMITTE = "edit_verkehrsmittel";
    const EDIT_USER = "edit_user";
    const VIEW_ORGANISATION_TYPE = "view_org_type";
    const VIEW_ORGANISATION = "view_org";
    const VIEW_VERKEHRSMITTEL = "view_verkehrsmittel";
    const VIEW_USER = "view_user";
    const VIEW_ALL_USERS = "view_all_users";
    const VIEW_TICKETS = "view_tickets";
    const EDIT_TICKETS = "edit_tickets";
    const IMPORT_TICKETS = "import_tickets";
    const VIEW_STATIONS = "view_stations";
    const EDIT_STATIONS = "edit_stations";
    const IMPORT_STATIONS = "import_stations";
    const VIEW_ROUTES = "view_routes";
    const EDIT_ROUTES = "edit_routes";
    const IMPORT_ROUTES = "import_routes";
    const VIEW_CHANGE_ANALYSIS = "view_change_analysis";
    const EDIT_CHANGE_ANALYSIS = "edit_change_analysis";

    const ADMIN_PERMISSION = "admin_permission";
    const EDIT_PERMISSION = "edit_permission";
    const VIEW_PERMISSION = "view_permission";
    const MANAGE_PERMISSION = "manage_permission";
    const UPLOAD_DOWNLOAD = "upload_download";
    const GOD_PERMISSIONS = "god_permissions";

    const PERMISSION_GROUP_TITLES = [
        1 => Constants::ADMIN_PERMISSION,
        2 => Constants::EDIT_PERMISSION,
        3 => Constants::VIEW_PERMISSION,
        4 => Constants::MANAGE_PERMISSION,
        5 => Constants::UPLOAD_DOWNLOAD
    ];

    /* CSV Header */
    const CSV_HEADER_KNOTEN_FILE = [
        "NR",
        "NAME",
        "LAENGE",
        "BREITE"
    ];

    const CSV_HEADER_LINIEN_FILE = [
        "LINIEN_ID",
        "LINIEN_NAME",
        "HST_SORT",
        "NR",
        "NAME"
    ];

    const CSV_HEADER_FAHRSCHEIN_FILE = [
        "ID",
        "NAME",
        "KATEGORIE1",
        "KATEGORIE2",
        "KATEGORIE3",
        "KATEGORIE4"
    ];

    const CSV_HEADER_ZAEHLDATEN_FILE = [
        "STRECKE",
        "LINIE",
        "ZUGNUMMER",
        "ZEITSCHICHT",
        "RICHTUNG",
        "HST_SORT",
        "BHF_ID",
        "BHF_NAME",
        "UHRZEIT",
        "ZAEHLUNG",
        "EIN",
        "AUS",
        "BES",
        "BES1",
        "BES2",
        "EIN_F",
        "AUS_F",
        "C_BES_F",
        "ABSCHNITT_KM",
        "F_TAG",
        "F_JAHR",
        "P_TAG",
        "R_TAG",
        "PKM_TAG",
        "RKM_TAG",
        "P_JAHR",
        "R_JAHR",
        "PKM_JAHR",
        "RKM_JAHR"
    ];

    const CSV_HEADER_FRAGEBOGEN_FILE = [
        "FRAGE_ID"
    ];


    private function __construct()
    {
        // prevent instantiation
    }
}