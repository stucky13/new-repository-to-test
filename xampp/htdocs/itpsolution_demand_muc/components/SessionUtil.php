<?php
/**
 * Created by PhpStorm.
 * User: Wenk
 * Date: 22.02.2018
 * Time: 14:09
 */

namespace app\components;


use app\modules\core\models\Organisation;
use Yii;

class SessionUtil
{
    private static $SESSION_ORGANISATION = 'organisation';
    private static $SESSION_TEILNETZ = 'teilnetz';
    private static $SESSION_MAPVISUALISATION = 'mapvisualisation';

    /**
     * Sets the session paramaters for map visualisation of the user
     * @param array $parameters
     */
    public static function setSessionMapVisualisation($parameters)
    {
        Yii::$app->session->set(SessionUtil::$SESSION_MAPVISUALISATION, $parameters);
    }

    /**
     * Gets the session paramaters for map visualisation of the user
     *
     * @return mixed|\yii\web\Session
     */
    public static function getSessionMapVisualisation()
    {
        return Yii::$app->session->get(SessionUtil::$SESSION_MAPVISUALISATION);
    }

    /**
     * Resets the session paramaters for map visualisation of the user
     *
     * @return mixed|\yii\web\Session
     */
    public static function resetSessionMapVisualisation()
    {
        return Yii::$app->session->set(SessionUtil::$SESSION_MAPVISUALISATION, null);
    }

    /**
     * Sets the session organisation of the user
     * @param int $organisation
     */
    public static function setSessionOrganisationId($organisation)
    {
        Yii::$app->session->set(SessionUtil::$SESSION_ORGANISATION, $organisation);
    }

    /**
     * @param $teilnetz
     */
    public static function setSessionTeilnetzId($teilnetz)
    {
        Yii::$app->session->set(SessionUtil::$SESSION_TEILNETZ, $teilnetz);
        $test = Yii::$app->session->get(SessionUtil::$SESSION_TEILNETZ);
        $var = 0;
    }

    /**
     * Returns the id of the session organisation or the first organisation for the user
     * @return integer
     * @throws \Exception
     * @throws \Throwable
     */
    public static function getSessionOrganisationId()
    {
        $sessionId = Yii::$app->session->get(SessionUtil::$SESSION_ORGANISATION);
        if (!$sessionId) {
            $user = Yii::$app->getUser()->getIdentity();
            if ($user) {
                $organisations = $user->getAssignedOrganisationArray();
                if (sizeof($organisations) > 0) {
                    $firstId = array_keys($organisations)[0];
                    return $firstId;
                } else {
                    //TODO: Adminprüfung führt zu endloos Loop. Wenn Orga nicht gewechselt wurde, ist die erste Orga vorbelegt. Daher funktioniert find()->one().
                    return Organisation::find()->one()->getId();
                }
            }

        }
        return $sessionId;
    }

    /**
     * @return mixed|\yii\web\Session
     */
    public static function getSesssionTeilnetzId()
    {
        return Yii::$app->session->get(SessionUtil::$SESSION_TEILNETZ);
    }

    /**
     * @return Organisation
     * @throws \Exception
     * @throws \Throwable
     */
    public static function getSessionOrganisation()
    {
        return Organisation::find()->where(['organisation_id' => SessionUtil::getSessionOrganisationId()])->one();
    }

    /**
     * @return Organisation
     */
    public static function getSessionTeilnetz()
    {
        return Organisation::find()->where(['organisation_id' => SessionUtil::getSesssionTeilnetzId()])->one();
    }
}