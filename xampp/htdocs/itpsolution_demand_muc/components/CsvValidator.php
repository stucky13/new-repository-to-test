<?php

namespace app\components;

use Yii;

/**
 * Class CsvValidate
 *
 * Generally: $rules = [
 *                          [ single rule ],
 *                          ['validatorName', columns, args, errorLevel],
 *                          [
 *                          'validateSingle',
 *                          'columnName',
 *                              [
 *                                  'validator' => 'BuiltIn/custom/anonymousFunction',
 *                                  'message' => 'String: Custom error message outputted with sprintf'
 *                              ]
 *                           'level' => CsvValidator::ERROR_CRITICAL
 *                          ]
 *                     ]
 *
 *
 * For detailed information about $rules, see functions
 *
 * Usage:
 * 1) Create a new CsvValidator for every csv that should be validated. Recommended: Pass rules and header names during creation of CsvValidator
 *    Rules and header names can be set anytime before calling csvValidate() with the setters or by setting the attributes $contentRules and $csvHeader
 * 2) Load data from a file with loadCsvFromFile() or from a model with loadCsvFromModel
 * 3) Validate the content with validateCsv based on the rules given
 * 4) Get Errors with getters ( 3 different error levels available: Warning, Normal(default), Critical
 *
 *
 *
 *
 * FUTURE WORK:
 *  - Optimize validation: group validator by column and execute all validators at once column wise
 *  - Naming
 *
 * @package app\models
 */
class CsvValidator
{

    // ERROR LEVELS FOR LOGGING
    const ERROR_WARNING = 0;
    const ERROR_NORMAL = 1;
    const ERROR_CRITICAL = 2;

    /**
     * Holds the content of the csvFile as 2d array
     * [[row] [row]]
     * row = [col, col, col]
     * @var array $content
     */
    public $content = [];


    /**
     * Place to lookup for custom validation function
     *
     * @var object $calledClass
     */
    public $calledClass;
    /**
     * Errors on column level where validation is not aborted when wrong formatted data or invalid data is found
     * For errors that abort upload, add error to Model.errors via $model->addError()
     * @var array $csvErrors
     */
    public $csvErrors = [];
    public $csvWarnings = [];
    public $csvCritical = [];

    /** @var string[] $csvHeader Expected header names */
    public $csvHeader = [];


    /**
     *
     * Similar to rules in Model class. This array holds validation rules. Example
     * colRules = [
     *  ['validatorName', col1Int, [args]],
     * ['validatorName', [col1, col2, ...]],
     * ['validatorName', [col1Int, col2, ...], [args]]
     * ]
     * validatorName = function name in this class or child class / anonymous function
     *  Custom validators can be defined and called. Custom validator signature should be validatorName($col, $args) where $col will be the column number and $args the argument array defined
     *  in the $contentRules
     *
     * col* = column number or column name, for which the validation should be applied
     * param* = array with key values, additional parameters for validation functions
     *
     * @var array $contentRules
     */
    public $contentRules;

    /**
     * CsvValidator constructor
     * @param null $calledClass Class where custom functions are looked up if none specified CsvValidator class will be used
     * @param array $contentRules Recommended set content rules in constructor
     * @param array $csvHeader Recommended set header names in constructor
     */
    public function __construct($calledClass = null, $contentRules = [], $csvHeader = [])
    {
        $this->calledClass = $calledClass ?? $this;
        $this->contentRules = $contentRules;
        $this->csvHeader = $csvHeader;
    }

    public static function className()
    {
        return get_called_class();
    }


    /**
     * Validate columns based on rules given in colRules and header based on names given in csvHeader
     * @throws \Exception
     * @return boolean false if critical errors occured or header is incorrect
     */
    public function validateCsv()
    {
        if (empty($this->content)) {
            throw new \Exception("No content");
        }

        // Validate header first, if errors occure, abort validation
        if (!$this->validateCsvHeader()) {
            return false;
        }

        $validators = $this->getCsvValidators();
        foreach ($validators as $validator) {
            //array_merge only for easy restructuring of rules if anything should change
            if (method_exists($this->calledClass, $validator['validator'])) {
                $errors = call_user_func_array([$this->calledClass, $validator['validator']], [intval($validator['col']), $validator['args']]);
            } else {
                $errors = call_user_func_array([$this, $validator['validator']], [intval($validator['col']), $validator['args']]);
            }
            if (!empty($errors)) {
                switch ($validator['level']) {
                    case CsvValidator::ERROR_WARNING:
                        $this->csvWarnings = $this->csvWarnings + $errors;
                        break;
                    case CsvValidator::ERROR_NORMAL:
                        $this->csvErrors = $this->csvErrors + $errors;
                        break;
                    case CsvValidator::ERROR_CRITICAL:
                        $this->csvCritical = $this->csvCritical + $errors;
                        break;
                }
            }
        }
        return empty($this->errors);
    }

    /**
     * Checks the first line of the file and checks entries against provided names
     * Errors are logged in $model->errors not $this->csvErrors
     * @throws \Exception
     * @return boolean True if header is correct
     */
    public function validateCsvHeader()
    {
        if (empty($this->csvHeader)) {
            return true;
        }

        if (empty($this->content)) {
            throw new \Exception("No content");
        }

        $errors = [];
        $fileHeaders = array_map('strtoupper', $this->content[0]);
        // Check for unknown columns
        $diff = array_diff($fileHeaders, $this->csvHeader);
        if (!empty($diff)) {
            foreach ($diff as $error) {
                $errors[] = sprintf(Yii::t('app', 'unknown column %s'), $error);
            }
        }
        // Check for missing columns
        $diff = array_diff($this->csvHeader, $fileHeaders);
        if (!empty($diff)) {
            foreach ($diff as $error) {
                $errors[] = sprintf(Yii::t('app', 'unknown column %s'), $error);
            }
        }

        // Check order of columns
        $diff = array_diff_assoc($this->csvHeader, $fileHeaders);
        if (!empty($diff)) {
            $errors[] = Yii::t('app', 'column order not correct');
        }

        if(!empty($errors)){
            $this->csvCritical = $this->csvCritical + $errors;
            return false;
        }else{
            return true;
        }
    }

    /**
     * Builds array of names and args of validation functions to be applied
     * @return array
     * @throws \Exception
     */
    private function getCsvValidators()
    {

        if (!is_array($this->contentRules)) {
            throw new \Exception("contentRules must return an array.");
        }

        // Build validators
        $csvValidators = [];
        foreach ($this->contentRules as $rule) {
            if (!is_array($rule) || sizeof($rule) < 2) {
                throw new \Exception("Invalid config of csvRule");
            }
            // check if validator exist
            if (!method_exists($this, $rule[0]) && !method_exists($this->calledClass, $rule[0])) {
                throw new \Exception("Invalid validator name $rule[0]");
            }

            $args = array_key_exists(2, $rule) ? $rule[2] : [];
            $error_level = array_key_exists('level', $rule) ? $rule['level'] : CsvValidator::ERROR_NORMAL;
            // Check if col/s is array or single col
            if (is_array($rule[1])) {
                foreach ($rule[1] as $col) {
                    $csvValidators[] = ['validator' => $rule[0], 'args' => $args, 'col' => $this->nameToCol($col), 'level' => $error_level];
                }
            } else {
                $csvValidators[] = ['validator' => $rule[0], 'args' => $args, 'col' => $this->nameToCol($rule[1]), 'level' => $error_level];
            }
        }
        return $csvValidators;
    }

    /**
     * Header name to column conversion. If input is a column number already then return number.
     * Otherwise lookup index
     * @param int|string $colName Either a integer or string
     * @param array $content
     * @throws \Exception
     * @return int columnnumber
     */
    private function nameToCol($colName, $content = null)
    {
        if (is_int($colName)) {
            return $colName;
        }
        $header = $content ? $content[0] : $this->content[0];
        $colIndices = array_keys($header, $colName);
        if (sizeof($colIndices) != 1) {
            throw new \Exception("Invalid colName. Either no or multiple columns named $colName");
        }
        return $colIndices[0];
    }

    /**
     * Loads csv file from model
     * @param \yii\base\Model $model
     * @param string $attribute attribute name to load data from
     * @param bool $alias If Yii::getAlias should be applied if the attribute is a file path
     * @throws \Exception
     */
    public function loadCsvFromModel($model, $attribute, $alias = false)
    {
        if (!($model instanceof \yii\base\Model)) {
            throw new \Exception('Invalid Model');
        }

        if ($model->$attribute instanceof yii\web\UploadedFile) {
            $this->loadCsvFromFile($model->$attribute->tempName);
        } elseif ($model->$attribute) {
            $path = $model->$attribute->tempName;
            if ($alias) {
                $path = Yii::getAlias($path);
            }
            $this->loadCsvFromFile($path);
        } else {
            throw new \Exception('Invalid Attribute');
        }
    }

    /**
     * Loads the content of a file and parses it
     *
     * @param string $filename Path of file
     */
    public function loadCsvFromFile($filename)
    {
        if (empty($filename)) {
            $this->content = [];
        }

        $this->content = array_map(function ($value) {
            return str_getcsv($value, ";");
        }, file($filename));
    }

    /**
     * Checks if all rows have the same col number
     */
    public function checkUniform()
    {
        if (empty($this->content)) {
            return true;
        }
        return $this->checkDimension($this->content, sizeof($this->content), sizeof($this->content[0]));
    }

    /**
     * Check dimensions of CSV file
     *
     * @param $content array[] Csv file contents
     * @param $expectedRow int Expected row number
     * @param $expectedCol int  Expected col per row
     * @return bool True if dimensions are correct
     */
    public function checkDimension($content, $expectedRow, $expectedCol)
    {
        if (sizeof($content) != $expectedRow) {
            return false;
        }
        foreach ($content as $i => $row) {
            if (sizeof($row) != $expectedCol) {
                $this->csvErrors[] = sprintf(Yii::t('app', 'expected %d columns in row %d, got %d columns'), $expectedCol, ($i + 1), sizeof($row));
                return false;
            }
        }
        return true;
    }

    /**
     * Validates single column.
     *
     * validation function args['validator'] can be a function name(string), anonymous function or a predefined function(string)
     * validation functions should have following signature
     *
     * public function ($element, $args){return boolean;}
     *
     * Predefined functions: validator => predefinedString
     *
     * numeric: Checks if element is numeric
     * integer: Checks if element is an integer
     * in: Checks if element is in array given by args['range']
     * date: Checks if element is in a specific date format, format given by args['format']
     * regex: Checks if element is in a specific format, format given by args['format']
     * required: Check if element is empty string
     *
     * @param int $col the column
     * @param array $args , relevant args: validator => predefinedFunctions / custom function name / anonymous function.
     *        Function should have following signature: validationFunction($element){ return 'isType X';}
     * @return array errors found
     */
    public function validateSingle($col, $args)
    {
        $predefinedFunctions = [
            'numeric' => function ($element) {
                return is_numeric($element);
            },
            'integer' => function ($element) {
                return ctype_digit($element);
            },
            'in' => function ($element) use ($args) {
                return in_array($element, $args['range']);
            },
            'date' => function ($element) use ($args) {
                if ($args['format'] === 'Ym') {
                    $date = \DateTime::createFromFormat($args['format'] . 'd', $element . '01');
                } else {
                    $date = \DateTime::createFromFormat($args['format'], $element);
                }
                return $date && $date->format($args['format']) == $element;
            },
            'regex' => function ($element) use ($args) {
                return preg_match($args['format'], $element);
            },
            'required' => function ($element) {
                return $element != "";
            },
        ];

        if ($args['validator'] instanceof \Closure) {
            $valFun = $args['validator'];
        } elseif (method_exists($this->calledClass, $args['validator'])) {
            $fun = $args['validator'];
            $valFun = function ($element, $args) use ($fun) {
                return $this->calledClass->$fun($element, $args);
            };
        } else {
            $valFun = $predefinedFunctions[$args['validator']];
        }

        $errors = [];
        for ($rowNum = 1; $rowNum < sizeof($this->content); $rowNum++) {
            $element = $this->content[$rowNum][$col];
            if ($element != "" && !$valFun($element, $args)) {
                $msg = array_key_exists('message', $args) ? $args['message'] : Yii::t('app', 'wrong format in column %s row %d');
                $errors[] = sprintf($msg, $this->content[0][$col], $rowNum);
            }
        }
        return $errors;
    }

    /**
     * Call $callback on every row of csv.
     * Callback signature: function($row){$row['xyz'] = 1; return $row}
     *
     * Used to modify content or count things:
     *
     * function($row) use (&$buffer)
     * {
     *  $buffer += $row['xx'];
     *  return $row;
     * }
     *
     * @param int|string $col
     * @param callable $callback
     * @throws \Exception
     */
    public function mapRow($col, $callback)
    {
        $col = $this->nameToCol($col);
        for ($i = 1; $i < sizeof($this->content); $i++) {
            $this->content[$i][$col] = array_keys($callback(array_combine($this->csvHeader, $this->content[$i])));
        }
    }

    /**
     * Checks if columns are unique in a single file
     * @param int $col
     * @return array
     */
    public function validateColUnique($col)
    {
        $errors = [];
        $valueCountMap = [];

        for ($rowNum = 1; $rowNum < sizeof($this->content); $rowNum++) {
            $element = $this->content[$rowNum][$col];
            if ($element != "") {
                if (array_key_exists($element, $valueCountMap)) {
                    $valueCountMap[$element][] = $rowNum;
                } else {
                    $valueCountMap[$element] = [$rowNum];
                }
            }
        }

        foreach ($valueCountMap as $value => $rows) {
            if (sizeof($rows) > 1) {
                $errors[] = sprintf(Yii::t('app', 'unique value expected, got multiple copies of %s in columns %s'), $value, implode(', ', $rows));
            }
        }
        return $errors;
    }

    /**
     * Validates two columns in file
     *
     * Expected args:
     *  validator = Name of validator function or anonymous function with the signature
     *   validator($element1, $element2)
     *  {
     *  return $element1 == $element2;
     *  }
     *  validateWith = int column or name of column
     *
     *
     * @param $col1
     * @param $args
     * @return array
     * @throws \Exception
     */
    public function validateColDual($col1, $args)
    {
        /** @var callable $valFun comparison function */
        $valFun = null;

        if ($args['validator'] instanceof \Closure) {
            $valFun = $args['validator'];
        } elseif (method_exists($this->calledClass, $args['validator'])) {
            $fun = $args['validator'];
            $valFun = function ($element, $args) use ($fun) {
                return $this->calledClass->$fun($element, $args);
            };
        }

        $col2 = $this->nameToCol($args['validateWith']);
        $msg = array_key_exists('message', $args) ? $args['validator'] : Yii::t('app', 'comparison failed, %s and %s at row %d column %s and %s');
        $errors = [];
        for ($rowNum = 1; $rowNum < sizeof($this->content); $rowNum++) {
            $val1 = $this->content[$rowNum][$col1];
            $val2 = $this->content[$rowNum][$col2];
            if ($val1 != "" && !$valFun($val1, $val2)) {
                $errors[] = sprintf($msg, $val1, $val2, $rowNum, $this->content[0][$col1], $this->content[0][$col2]);
            }
        }
        return $errors;
    }


    /**
     * Validates row with a column as "key"
     *
     * Expected args:
     *  validator: anonymous function or function name with following signature:
     *              $row is header=>value pairs
     *              function($row, $args, $col){
     *                  return boolean;
     *              }
     *
     * @param $col
     * @param $args
     * @return array
     */
    public function validateRow($col, $args)
    {
        $valFun = null;
        if ($args['validator'] instanceof \Closure) {
            $valFun = $args['validator'];
        } elseif (method_exists($this->calledClass, $args['validator'])) {
            $fun = $args['validator'];
            $valFun = function ($row, $args, $col) use ($fun) {
                return $this->calledClass->$fun($row, $args, $col);
            };
        }

        $msg = array_key_exists('message', $args) ? $args['validator'] : Yii::t('app', 'row error');
        $errors = [];
        for ($rowNum = 1; $rowNum < sizeof($this->content); $rowNum++) {
            $val = $this->content[$rowNum][$col];
            if (($val != "" && !$valFun(array_combine($this->csvHeader, $this->content[$rowNum]), $args, $col))) {
                $errors[] = sprintf($msg, $val, $rowNum, $this->content[0][$col]);
            }
        }
        return $errors;

    }

    /**
     *  Checks if element is unique across different files. Does not check against itself ! Use validateColUnique
     *
     *  Arguments expected:
     *  files: path of files to check against
     *  targetCol: int column of target or name of column
     *
     *
     * @param $col
     * @param $args
     * @throws \Exception
     * @return array
     */
    public function validateCrossUnique($col, $args)
    {
        $files = $args['files'];
        $targetCol = $args['targetCol'];

        $msg = array_key_exists('message', $args) ? $args['message'] : Yii::t('app', '%s not unique copie found for %s in file %s row %d');
        $errors = [];
        for ($rowNumSource = 1; $rowNumSource < sizeof($this->content); $rowNumSource++) {
            $sourceElement = $this->content[$rowNumSource][$col];
            foreach ($files as $file) {
                $targetContent = array_map(function ($value) {
                    return str_getcsv($value, ";");
                }, file($file));
                $targetCol = $this->nameToCol($targetCol, $targetContent);

                for ($rowNumTarget = 1; $rowNumTarget < sizeof($targetContent); $rowNumTarget++) {
                    if ($sourceElement == $targetContent[$rowNumTarget][$targetCol]) {
                        $errors[] = sprintf($msg, $this->content[0][$col], $sourceElement, basename($file), $rowNumTarget);
                    }
                }
            }
        }
        return $errors;
    }

    /**
     *  Validates element against elements of different file. If target not found then add error
     *  Args expected :
     *  targetCol = column of the key in the targetFile
     *  files = Array of filepaths  .
     *  matching = function name or anonymous function which compares element <=> key in targetFile. Takes two arguments: elementSource, elementTarget
     *  e.g matching = function($elementSource, $elementTarget){ return $elementSource == $elementTarget+1}
     *
     * onMatch = function name or anonymous function which takes the target row, source row and args to perform additional checks
     * $rowSource/$rowTarget associative arrays with headerName => rowValue
     *  e.g. onMatch = function($rowSource, $rowTarget, $args) { return $rowSource['someColumn1'] == $rowTarget['someColumn2']; }
     *
     *
     *
     * @param $col
     * @param $args
     * @throws \Exception
     * @return array
     */
    public function validateCrossExists($col, $args)
    {
        $matchFun = null;
        $onMatchFun = null;
        $targetCol = $args['targetCol'];

        /** @var string[] $files Array of file paths */
        $files = $args['files'];


        $errors = [];
        //TODO right error message
        $msg = array_key_exists('message', $args) ? $args['message'] : Yii::t('app', 'target data does not exist');


        if ($args['matching'] instanceof \Closure) {
            $matchFun = $args['matching'];
        } elseif (method_exists($this->calledClass, $args['matching'])) {
            $fun = $args['matching'];
            $matchFun = function ($elementSource, $elementTarget) use ($fun) {
                return $this->calledClass->$fun($elementSource, $elementTarget);
            };
        }
        if ($args['onMatch'] instanceof \Closure) {
            $onMatchFun = $args['onMatch'];
        } elseif (method_exists($this->calledClass, $args['onMatch'])) {
            $fun = $args['onMatch'];
            $onMatchFun = function ($rowSource, $rowTarget, $args) use ($fun) {
                return $this->calledClass->$fun($rowSource, $rowTarget, $args);
            };
        }

        for ($rowNumSource = 1; $rowNumSource < sizeof($this->content); $rowNumSource++) {
            $element = $this->content[$rowNumSource][$col];
            if($element != "") {
                $targetExists = false;

                foreach ($files as $file) {
                    $fileContent = array_map(function ($value) {
                        return str_getcsv($value, ";");
                    }, file($file));

                    $targetCol = $this->nameToCol($targetCol, $fileContent);
                    for ($rowNumTarget = 1; $rowNumTarget < sizeof($fileContent); $rowNumTarget++) {
                        if ($matchFun($element, $fileContent[$rowNumTarget][$targetCol])) {
                            $rowSource = array_combine($this->content[0], $this->content[$rowNumSource]);
                            $rowTarget = array_combine($fileContent[0], $fileContent[$rowNumTarget]);
                            if ($onMatchFun($rowSource, $rowTarget, $args)) {
                                $targetExists = true;
                                break;
                            }
                        }
                    }
                    if ($targetExists) {
                        break;
                    }
                }
                if (!$targetExists) {
                    $errors[] = sprintf($msg);
                }
            }
        }
        return $errors;
    }

    /**
     * @return array
     */
    public function getCsvErrors()
    {
        return $this->csvErrors;
    }

    /**
     * @return array
     */
    public function getCsvWarnings()
    {
        return $this->csvWarnings;
    }

    /**
     * @return array
     */
    public function getCsvCritical()
    {
        return $this->csvCritical;
    }

    /**
     * @param string[] $csvHeader
     */
    public function setCsvHeader(array $csvHeader)
    {
        $this->csvHeader = $csvHeader;
    }

    /**
     * @param array $contentRules
     */
    public function setContentRules(array $contentRules)
    {
        $this->contentRules = $contentRules;
    }


}